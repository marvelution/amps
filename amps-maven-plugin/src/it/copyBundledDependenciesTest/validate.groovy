def metaInfLib = new File(basedir, 'target/classes/META-INF/lib')
assert metaInfLib.exists(), "The $metaInfLib directory should exist"

assert new File(metaInfLib, 'commons-io-2.8.0.jar').exists()
assert new File(metaInfLib, 'commons-logging-1.2.jar').exists()
assert !new File(metaInfLib, 'servlet-api-2.4.jar').exists()
assert !new File(metaInfLib, 'junit-4.5.jar').exists()
assert !new File(metaInfLib, 'atlassian-base-pom-26.pom').exists()
