package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.util.AmpsPluginVersionChecker;
import com.atlassian.maven.plugins.amps.util.MojoExecutorWrapper;
import com.atlassian.maven.plugins.amps.util.UpdateChecker;
import com.google.common.annotations.VisibleForTesting;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

import static com.atlassian.maven.plugins.amps.util.ProjectUtils.shouldDeployTestJar;

/**
 * The base class for all AMPS Mojos. Intended to contain only the most general configuration properties.
 */
public abstract class AbstractAmpsMojo extends AbstractMojo {

    // ------------------- Dependencies ----------------------

    @Component
    private AmpsPluginVersionChecker ampsPluginVersionChecker;

    @Component
    private BuildPluginManager buildPluginManager;

    @Component
    protected MojoExecutorWrapper mojoExecutorWrapper;

    @Component
    private UpdateChecker updateChecker;

    /**
     * The Maven context.
     */
    @VisibleForTesting
    MavenContext mavenContext;

    /**
     * Helper object that runs Maven goals.
     */
    private MavenGoals mavenGoals;

    // ------------------- User-configurable properties ----------------------

    /**
     * The current Maven project.
     */
    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;

    /**
     * The list of modules being built, the reactor.
     */
    @Parameter(property = "reactorProjects", required = true, readonly = true)
    private List<MavenProject> reactor;

    /**
     * The Maven session.
     */
    @Parameter(property = "session", required = true, readonly = true)
    private MavenSession session;

    /**
     * The current Maven plugin artifact id.
     */
    @Parameter(property = "plugin.artifactId", required = true, readonly = true)
    private String pluginArtifactId;

    /**
     * The current Maven plugin version.
     */
    @Parameter(property = "plugin.version", required = true, readonly = true)
    private String pluginVersion;

    /**
     * Whether to force a check for a new SDK regardless of the last time such a check was made.
     */
    @Parameter(property = "force.update.check", defaultValue = "false")
    private boolean forceUpdateCheck;

    /**
     * Whether to skip checking the AMPS version in the POM.
     */
    @Parameter(property = "skip.amps.pom.check", defaultValue = "false")
    private boolean skipAmpsPomCheck;

    /**
     * Whether to skip files ending in {@code -min.js} or {@code .min.js}.
     */
    @Parameter(property = "js.compiler.skip.minified", defaultValue = "false")
    protected boolean skipMinifiedJs;

    /**
     * Whether to skip all prompting, for example so that automated builds don't hang.
     */
    @Parameter(property = "skipAllPrompts", defaultValue = "false")
    private boolean skipAllPrompts;

    /**
     * Whether the test plugin should be built or not.  If not specified, it detects an atlassian-plugin.xml in the
     * test classes directory and builds if exists.
     */
    @Parameter(property = "buildTestPlugin", defaultValue = "false")
    private boolean buildTestPlugin;

    /**
     * Whether Maven is running offline.
     */
    @Parameter(property = "offline", defaultValue = "${settings.offline}")
    protected boolean offline;

    /**
     * The path to a properties file to override internal plugin versions.
     * This file should be in the following format:
     * artifactId=version
     * e.g. maven-deploy-plugin=2.5
     *
     * @deprecated doesn't work well with multimodule builds, use versionOverrides
     */
    @Deprecated
    @Parameter(property = "version.override.path")
    private String versionOverridesPath;

    /**
     * The plugin artifactIds (as used by AMPS internally) for which the internal hardcoded versions should be
     * overridden by the versions in the effective POM's pluginManagement section.
     *
     * @since 6.2.0
     */
    @Parameter(property = "version.override.set")
    private Set<String> versionOverrides;

    /**
     * Project source files encoding. Along with explicit definition in the plugin
     * configuration property inherits value from the build section (for maven 3.x)
     * <pre>
     * &#60;project&#62;
     *   ...
     *   &#60;build&#62;
     *      ...
     *      &#60;sourceEncoding&#62;UTF-8&#60;/sourceEncoding&#62;
     *      ....
     *   &#60;/build&#62;
     *   ...
     *  &#60;/project&#62;
     * </pre>
     * <p>
     * For maven 2.x value inherits from the global property 'project.build.sourceEncoding':
     * <pre>
     * &#60;project&#62;
     *  ...
     *   &#60;properties&#62;
     *       &#60;project.build.sourceEncoding&#62;UTF-8&#60;/project.build.sourceEncoding&#62;
     *       ...
     *   &#60;/properties&#62;
     *   ...
     *  &#60;/project&#62;
     * </pre>
     */
    @Parameter(property = "encoding", defaultValue = "${project.build.sourceEncoding}")
    protected String encoding;

    // ------------------- Methods ----------------------

    protected MavenContext getMavenContext() {
        if (mavenContext == null) {
            mavenContext = new MavenContext(project, reactor, session, buildPluginManager, getLog());
        }

        mavenContext.setVersionOverridesPath(this.versionOverridesPath);
        mavenContext.setVersionOverrides(this.versionOverrides);
        return mavenContext;
    }

    protected final MavenGoals getMavenGoals() {
        if (mavenGoals == null) {
            mavenGoals = new MavenGoals(getMavenContext(), mojoExecutorWrapper);
        }
        return mavenGoals;
    }

    @Nonnull
    protected PluginInformation getPluginInformation() {
        final String artifactId = pluginArtifactId == null ? "amps-maven-plugin" : pluginArtifactId;
        return PluginInformation.fromArtifactId(artifactId, pluginVersion);
    }

    protected final UpdateChecker getUpdateChecker() {
        updateChecker.setCurrentVersion(getSDKVersion());
        updateChecker.setForceCheck(forceUpdateCheck);

        boolean skipCheck = (shouldSkipPrompts() || offline);

        updateChecker.setSkipCheck(skipCheck);

        return updateChecker;
    }

    protected final AmpsPluginVersionChecker getAmpsPluginVersionChecker() {
        ampsPluginVersionChecker.skipPomCheck(skipAmpsPomCheck);

        if (shouldSkipPrompts()) {
            ampsPluginVersionChecker.skipPomCheck(true);
        }
        return ampsPluginVersionChecker;
    }

    protected final String getAmpsPluginVersion() {
        final String ampsPluginVersion = System.getenv("AMPS_PLUGIN_VERSION");
        return ampsPluginVersion == null ? getPluginInformation().getVersion() : ampsPluginVersion;
    }

    protected final String getSDKVersion() {
        final String sdkVersion = System.getenv("ATLAS_VERSION");
        return sdkVersion != null ? sdkVersion : getPluginInformation().getVersion();
    }

    protected final boolean shouldBuildTestPlugin() {
        return buildTestPlugin || shouldDeployTestJar(getMavenContext());
    }

    protected final boolean shouldSkipPrompts() {
        return skipAllPrompts;
    }
}
