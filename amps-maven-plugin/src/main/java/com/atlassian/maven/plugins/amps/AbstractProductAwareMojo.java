package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.analytics.AnalyticsService;
import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;
import com.atlassian.maven.plugins.amps.product.ProductHandler;
import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.repository.RepositorySystem;

import javax.annotation.Nonnull;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.prefs.Preferences;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.sdkFirstRun;
import static com.atlassian.maven.plugins.amps.analytics.impl.GoogleAnalyticsService.newGoogleAnalyticsService;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.REFAPP;

/**
 * Base class for Mojos that run a product.
 */
public abstract class AbstractProductAwareMojo extends AbstractAmpsMojo {

    private static final String PREF_FIRSTRUN_PREFIX = "sdk-firstrun";

    // ------------------- Dependencies ----------------------

    private AnalyticsService analyticsService;

    @Component
    protected ArtifactResolver artifactResolver;

    @Component
    private ProjectBuilder projectBuilder;

    @Component
    protected RepositorySystem repositorySystem;

    @Component
    private WebAppManager webAppManager;

    // ------------------- User-configurable properties ----------------------

    /**
     * The Atlassian product id, e.g. {@value com.atlassian.maven.plugins.amps.product.ProductHandlerFactory#REFAPP}.
     *
     * @see ProductHandlerFactory
     */
    @Parameter(property = "product")
    private String product;

    /**
     * Instance to run. If provided, used to determine the product to use, instead of
     * using the product ID.
     */
    @Parameter(property = "instanceId")
    protected String instanceId;

    /**
     * <p>Flag to enable Google tracking.</p>
     *
     * <p>AMPS sends basic usage events to Google analytics by default. To disable tracking, either:</p>
     * <ol>
     * <li>Add <code>&lt;allow.google.tracking>false&lt;/allow.google.tracking></code> to the
     *  <code>&lt;properties></code> section of your <code>.m2/settings.xml</code> file</li>
     * <li>Include <code>&lt;allowGoogleTracking>false&lt;/allowGoogleTracking></code> in
     * the amps plugin configuration in your <code>pom.xml</code></li>
     * <li>or pass <code>-Dallow.google.tracking=false</code> on the command line.
     * </ol>
     */
    @Parameter(property = "allow.google.tracking", defaultValue = "true")
    protected boolean allowGoogleTracking;

    protected String getDefaultProductId() throws MojoExecutionException {
        // If the [product]-maven-plugin didn't override this method, we fetch the name of the plugin
        String nameOfTheCurrentMavenPlugin = getPluginInformation().getId();
        if (ProductHandlerFactory.getIds().contains(nameOfTheCurrentMavenPlugin)) {
            return nameOfTheCurrentMavenPlugin;
        }
        return null;
    }

    @Nonnull
    protected final String getProductId() throws MojoExecutionException {
        if (product == null) {
            product = getDefaultProductId();
            if (product == null) {
                product = REFAPP;
            }
        }
        return product;
    }

    @Nonnull
    private AnalyticsService getAnalyticsService() throws MojoExecutionException {
        if (analyticsService == null) {
            analyticsService = newGoogleAnalyticsService(
                    getProductId(), getAmpsPluginVersion(), getPluginInformation().getVersion(), getLog());
            getLog().info("Google Analytics Tracking is enabled to collect AMPS usage statistics.");
            getLog().info("Although no personal information is sent, you may disable tracking by adding" +
                    " <allowGoogleTracking>false</allowGoogleTracking> to the AMPS plugin configuration in your pom.xml");
        }
        return analyticsService;
    }

    /**
     * Sends the given analytics event if {@link #allowGoogleTracking} is {@code true}.
     *
     * @param event the event to send
     * @throws MojoExecutionException if execution fails
     */
    protected final void sendAnalyticsEvent(@Nonnull final AnalyticsEvent event) throws MojoExecutionException {
        if (allowGoogleTracking) {
            getAnalyticsService().send(event);
        }
    }

    protected final void trackFirstRunIfNeeded() throws MojoExecutionException {
        final boolean runningShellScript = System.getenv("ATLAS_VERSION") != null;
        if (allowGoogleTracking && runningShellScript) {
            String firstRunKey = PREF_FIRSTRUN_PREFIX + "-" + getAmpsPluginVersion();
            Preferences prefs = Preferences.userNodeForPackage(getClass());
            String alreadyRan = prefs.get(firstRunKey, null);
            if (alreadyRan == null) {
                sendAnalyticsEvent(sdkFirstRun(getAmpsPluginVersion()));
                prefs.put(firstRunKey, "true");
            }
        }
    }

    private final ConcurrentMap<String, ProductHandler> productHandlerCache = new ConcurrentHashMap<>();

    /**
     * Creates a handler for the given product.
     *
     * @param productId the product nickname (not the instance id)
     * @return a product handler for this nickname
     */
    protected ProductHandler getProductHandler(final String productId) {
        return productHandlerCache.computeIfAbsent(productId, key ->
                ProductHandlerFactory.create(productId, getMavenContext(), getMavenGoals(), repositorySystem,
                    projectBuilder, artifactResolver, webAppManager));
    }
}
