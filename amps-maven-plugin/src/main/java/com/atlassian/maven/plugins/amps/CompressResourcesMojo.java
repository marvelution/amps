package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Map;

/**
 * Compresses JavaScript resources using the configured {@link com.atlassian.maven.plugins.amps.minifier.Minifier}
 * plugin. Creates compressed versions of all JavaScript resources by attaching the '-min' suffix.
 *
 * @since 3.2
 */
@Mojo(name = "compress-resources")
public class CompressResourcesMojo extends AbstractAmpsMojo {

    /**
     * Whether to use Google Closure instead of YUI for JavaScript compilation.
     */
    @Parameter(property = "closure.js.compiler", defaultValue = "true")
    private boolean closureJsCompiler;

    /**
     * Whether to compress the resources or not. Defaults to true.
     */
    @Parameter(defaultValue = "true")
    private boolean compressResources;

    @Parameter(defaultValue = "true")
    private boolean compressJs;

    @Parameter(defaultValue = "true")
    private boolean compressCss;

    /**
     * Whether the closure JSdoc warnings are enabled or not. Defaults to true.
     */
    @Parameter(property = "closureJsdocWarningsEnabled", defaultValue = "true")
    private boolean closureJsdocWarningsEnabled;

    /**
     * <a href="http://javadoc.closure-compiler.googlecode.com/git/com/google/javascript/jscomp/CompilerOptions.html">
     * Options</a> to pass to the closure compiler. AMPS currently supports configuration of:
     * <ul>
     *     <li>languageIn</li>
     *     <li>languageOut</li>
     *     <li>emitUseStrict</li>
     * </ul>
     */
    @Parameter
    private Map<String, String> closureOptions;

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (compressResources) {
            final Charset cs;
            if (encoding == null) {
                cs = Charset.defaultCharset();
                getLog().warn("File encoding has not been set, using platform encoding " + cs.name() + ", i.e. build is platform dependent!");
            } else {
                try {
                    cs = Charset.forName(encoding);
                } catch (IllegalCharsetNameException | UnsupportedCharsetException ex) {
                    throw new MojoExecutionException("Failed to resolve charset: " + encoding, ex);
                }
            }
            getMavenGoals().compressResources(compressJs, compressCss, closureJsCompiler, cs, closureOptions,closureJsdocWarningsEnabled);
        } else {
            getLog().debug("Compressing resources disabled");
        }
    }
}
