package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.util.ArrayList;
import java.util.List;

/**
 * Copies bundled dependencies into {@code META-INF/lib} for the test plugin.
 */
@Mojo(name = "copy-test-bundled-dependencies", requiresDependencyResolution = ResolutionScope.TEST)
public class CopyTestBundledDependenciesMojo extends AbstractProductAwareMojo {

    /**
     * Whether the test plugin should exclude test scoped dependencies.
     * This should always be false for running "wired" integration tests
     */
    @Parameter(property = "excludeAllTestDependencies", defaultValue = "false")
    private boolean excludeAllTestDependencies;

    @Parameter(property = "extractTestDependencies", defaultValue = "false")
    private Boolean extractTestDependencies;

    /**
     * Any artifacts to exclude when copying test bundle dependencies.
     */
    @Parameter
    private List<ProductArtifact> testBundleExcludes = new ArrayList<>();

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (shouldBuildTestPlugin()) {
            if (!extractTestDependencies) {
                if (excludeAllTestDependencies) {
                    getMavenGoals().copyTestBundledDependenciesExcludingTestScope(testBundleExcludes);
                } else {
                    getMavenGoals().copyTestBundledDependencies(testBundleExcludes);
                }

            } else {
                if (excludeAllTestDependencies) {
                    getMavenGoals().extractTestBundledDependenciesExcludingTestScope(testBundleExcludes);
                } else {
                    getMavenGoals().extractTestBundledDependencies(testBundleExcludes);
                }
            }
        }
    }
}
