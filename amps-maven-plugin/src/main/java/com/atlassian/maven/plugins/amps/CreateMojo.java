package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.product.ProductHandler;
import com.atlassian.maven.plugins.amps.util.AmpsCreatePluginPrompter;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.createPlugin;
import static javax.xml.stream.XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES;
import static javax.xml.stream.XMLStreamConstants.CHARACTERS;
import static javax.xml.stream.XMLStreamConstants.END_ELEMENT;
import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Creates a new plugin project.
 */
@Mojo(name = "create", requiresProject = false)
public class CreateMojo extends AbstractProductHandlerMojo {

    @Component
    private AmpsCreatePluginPrompter ampsCreatePluginPrompter;

    @Override
    protected void doExecute() throws MojoExecutionException {
        trackFirstRunIfNeeded();

        sendAnalyticsEvent(createPlugin());

        // this is the name of a product (refapp, jira, confluence, etc)
        String pid = getProductId();
        // first try to get manual version
        Application a = getManualVersion(pid);
        final Properties userProperties =
                getMavenContext().getExecutionEnvironment().getMavenSession().getUserProperties();
        if (a != null) {
            if (isNotBlank(a.latest)) {
                getLog().info("using stable product version: " + a.latest);
                userProperties.setProperty(pid + "Version", a.latest);
            }
            if (isNotBlank(a.data)) {
                getLog().info("using stable data version: " + a.data);
                userProperties.setProperty(pid + "DataVersion", a.data);
            }
        } else {
            // use the old way (grab version from artifact)
            final Product product = getProductContexts().get(pid);
            getLog().info("determining latest stable product version...");
            final String stableVersion = getStableProductVersion(product);
            if (isNotBlank(stableVersion)) {
                getLog().info("using latest stable product version: " + stableVersion);
                userProperties.setProperty(pid + "Version", stableVersion);
            }

            getLog().info("determining latest stable data version...");
            final String stableDataVersion = getStableDataVersion(product);
            if (isNotBlank(stableDataVersion)) {
                getLog().info("using latest stable data version: " + stableDataVersion);
                userProperties.setProperty(pid + "DataVersion", stableDataVersion);
            }
        }

        getMavenGoals().createPlugin(getProductId(), ampsCreatePluginPrompter);
    }

    /**
     * @param searchString The name of a product (refapp, jira, confluence, etc)
     * @return An Application object holding latest stable/data version strings
     * and some other bits of information.
     * Possibly null if we cannot find it in the xml file.
     */
    private Application getManualVersion(final String searchString) {
        final XMLInputFactory f = XMLInputFactory.newInstance();
        f.setProperty(IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        try (InputStream versionsStream = CreateMojo.class.getClassLoader().getResourceAsStream("application-versions.xml")) {
            final XMLEventReader r = f.createXMLEventReader(versionsStream);
            Application a = new Application();
            String name = "";
            while (r.hasNext()) {
                final XMLEvent e = r.nextEvent();
                switch (e.getEventType()) {
                    case START_ELEMENT:
                        name = e.asStartElement().getName().getLocalPart();
                        if (name.equalsIgnoreCase("application"))
                            a = new Application();
                        break;
                    case END_ELEMENT:
                        if (e.asEndElement().getName().getLocalPart().equalsIgnoreCase("application") &&
                                a.name.equals(searchString))
                            return a;
                        name = "";
                        break;
                    case CHARACTERS:
                        final String value = e.asCharacters().getData().trim();
                        if (name.equalsIgnoreCase("name"))
                            a.name = value;
                        else if (name.equalsIgnoreCase("latest"))
                            a.latest = value;
                        else if (name.equalsIgnoreCase("data"))
                            a.data = value;
                        else if (name.equalsIgnoreCase("mvn-artifact"))
                            a.mvnArtifact = value;
                        break;
                    default:
                        // Ignore
                }
            }
        } catch (final IOException | XMLStreamException e) {
            // this can either be from closing the stream of an invalid XML file; either way, we just ignore it
        }
        return null;
    }

    protected final String getStableProductVersion(final Product product) throws MojoExecutionException {
        final ProductHandler handler = getProductHandler(product.getId());
        final ProductArtifact artifact = handler.getArtifact();

        if (artifact == null) {
            return "";
        }

        Artifact warArtifact = repositorySystem.createProjectArtifact(artifact.getGroupId(), artifact.getArtifactId(), "LATEST");

        return product.getArtifactRetriever().getLatestStableVersion(warArtifact);
    }

    protected final String getStableDataVersion(final Product product) throws MojoExecutionException {
        final ProductHandler handler = getProductHandler(product.getId());
        final Optional<ProductArtifact> maybeTestResourcesArtifact = handler.getTestResourcesArtifact();
        if (maybeTestResourcesArtifact.isPresent()) { // no lambda because exception thrown
            final ProductArtifact testResourcesArtifact = maybeTestResourcesArtifact.get();
            final Artifact warArtifact = repositorySystem.createProjectArtifact(
                    testResourcesArtifact.getGroupId(), testResourcesArtifact.getArtifactId(), "LATEST");
            return product.getArtifactRetriever().getLatestStableVersion(warArtifact);
        }
        return "";
    }

    private static class Application {
        String name = "";
        String latest = "";
        String data = "";
        String mvnArtifact = "";

        public String toString() {
            return "name=" + name + " latest=" + latest + " data=" + data + " mvn-artifact=" + mvnArtifact;
        }
    }
}
