package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import javax.annotation.Nonnull;
import java.util.List;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.debug;
import static com.atlassian.maven.plugins.amps.util.DebugUtils.getDebugPortProperties;
import static com.atlassian.maven.plugins.amps.util.DebugUtils.setNodeDebugPorts;

/**
 * Starts the Atlassian product in remote debug mode.
 */
@Mojo(name = "debug", requiresDependencyResolution = ResolutionScope.TEST)
@Execute(phase = LifecyclePhase.PACKAGE)
public class DebugMojo extends RunMojo {

    /**
     * The remote debug port. To avoid conflicts, this will only be used for the first product (and if it has more than
     * one node, only for its first node). Other {@code <product>} and/or {@code <node>} elements will be assigned a
     * random free port if they do not have an explicitly configured debug port.
     */
    @Parameter(property = "jvm.debug.port", defaultValue = "5005")
    private int jvmDebugPort;

    /**
     * Whether to suspend the JVM at startup to wait for the user to attach a debugger. This applies to all products
     * being run.
     */
    @Parameter(property = "jvm.debug.suspend")
    private boolean jvmDebugSuspend;

    @Override
    protected void beforeStart(final List<Product> products) throws MojoExecutionException {
        setNodeDebugPorts(products, jvmDebugPort);
        for (final Product product : products) {
            product.defaultJvmArgs(jvmArgs);
            product.setNodeDebugArgs(jvmDebugSuspend, getLog());
        }
        if (writePropertiesToFile) {
            propertiesToWriteToFile.putAll(getDebugPortProperties(products));
        }
    }

    @Override
    @Nonnull
    protected AnalyticsEvent getAnalyticsEvent() {
        return debug();
    }
}
