package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * Starts the Atlassian product in remote debug mode, but without a plugin project; the debuggable version of the
 * {@code run-standalone} goal.
 */
@Mojo(name = "debug-standalone", requiresProject = false)
public class DebugStandaloneMojo extends RunStandaloneMojo {
    @Override
    protected String getAmpsGoal() {
        return "debug";
    }
}
