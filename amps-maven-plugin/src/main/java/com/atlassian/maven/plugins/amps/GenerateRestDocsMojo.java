package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.wadl.RestDocsGenerator;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Generates the files needed by Jersey at runtime to provide an extended WADL including docs.
 *
 * @since 3.6.1
 */
@Mojo(name = "generate-rest-docs", requiresDependencyResolution = ResolutionScope.TEST)
public class GenerateRestDocsMojo extends AbstractAmpsMojo {
    /**
     * Whether to skip the generation of the REST documentation.
     */
    @Parameter(property = "rest.doc.generation.skip", defaultValue = "false")
    protected boolean skipRestDocGeneration;

    /**
     * Any custom Jackson serializer modules to be used when generating the documentation. If not blank, this value is
     * expected to be a colon-separated list of fully-qualified class names, with each such class implementing
     * {@code org.codehaus.jackson.map.Module}; see REST-267.
     */
    @Parameter
    private String jacksonModules;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipRestDocGeneration) {
            getLog().info("Skipping generation of the REST docs");
        } else {
            new RestDocsGenerator(getMavenContext(), mojoExecutorWrapper).generateRestDocs(jacksonModules);
        }
    }
}
