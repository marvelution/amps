package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.File;

import static com.atlassian.maven.plugins.amps.util.FileUtils.file;

/**
 * JARs up the current Maven project, bundling {@code target/classes/META-INF/MANIFEST.MF}, if it exists.
 */
@Mojo(name = "jar")
public class JarWithManifestMojo extends AbstractAmpsMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        File mf = file(getMavenContext().getProject().getBuild().getOutputDirectory(), "META-INF", "MANIFEST.MF");
        getMavenGoals().jarWithOptionalManifest(mf.exists());
    }
}
