package com.atlassian.maven.plugins.amps;

import org.apache.maven.model.Dependency;

import javax.annotation.Nonnull;

/**
 * An artifact put into a product's {@code lib} directory.
 */
public class LibArtifact extends ProductArtifact {

    public LibArtifact() {}

    public LibArtifact(final String groupId, final String artifactId, final String versionId) {
        super(groupId, artifactId, versionId);
    }

    /**
     * Returns this artifact as a Maven dependency.
     *
     * @return see description
     * @since 8.3
     */
    @Nonnull
    public Dependency asDependency() {
        final Dependency dependency = new Dependency();
        dependency.setGroupId(getGroupId());
        dependency.setArtifactId(getArtifactId());
        dependency.setVersion(getVersion());
        return dependency;
    }
}
