package com.atlassian.maven.plugins.amps;

import com.google.common.collect.Sets;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.twdata.maven.mojoexecutor.MojoExecutor;
import org.twdata.maven.mojoexecutor.MojoExecutor.ExecutionEnvironment;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.openInputStream;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.twdata.maven.mojoexecutor.MojoExecutor.plugin;

/**
 * The current Maven session, project, etc.
 */
public class MavenContext {
    
    private static final Map<String, String> DEFAULT_PLUGIN_VERSIONS = new LinkedHashMap<>();

    static {
        // Keep these in alphabetical order to avoid accidental duplication
        DEFAULT_PLUGIN_VERSIONS.put("atlassian-pdk", "2.3.3");
        DEFAULT_PLUGIN_VERSIONS.put("build-helper-maven-plugin", "3.0.0");
        DEFAULT_PLUGIN_VERSIONS.put("cargo-maven2-plugin", "1.8.2");
        DEFAULT_PLUGIN_VERSIONS.put("exec-maven-plugin", "1.2.1");
        DEFAULT_PLUGIN_VERSIONS.put("maven-archetype-plugin", "3.2.0");
        DEFAULT_PLUGIN_VERSIONS.put("maven-bundle-plugin", "3.5.0");
        DEFAULT_PLUGIN_VERSIONS.put("maven-cli-plugin", "1.0.11");
        DEFAULT_PLUGIN_VERSIONS.put("maven-dependency-plugin", "3.2.0");
        DEFAULT_PLUGIN_VERSIONS.put("maven-deploy-plugin", "2.8.2");
        DEFAULT_PLUGIN_VERSIONS.put("maven-failsafe-plugin", "2.22.1");
        DEFAULT_PLUGIN_VERSIONS.put("maven-help-plugin", "3.2.0");
        DEFAULT_PLUGIN_VERSIONS.put("maven-install-plugin", "2.5.2");
        DEFAULT_PLUGIN_VERSIONS.put("maven-jar-plugin", "3.0.2");
        DEFAULT_PLUGIN_VERSIONS.put("maven-javadoc-plugin", "3.3.0");
        DEFAULT_PLUGIN_VERSIONS.put("maven-release-plugin", "2.5.3");
        DEFAULT_PLUGIN_VERSIONS.put("maven-resources-plugin", "2.6");
        DEFAULT_PLUGIN_VERSIONS.put("maven-surefire-plugin", "2.22.1");
        DEFAULT_PLUGIN_VERSIONS.put("sql-maven-plugin", "1.5");
        DEFAULT_PLUGIN_VERSIONS.put("yuicompressor-maven-plugin", "1.5.1");
    }

    private final BuildPluginManager buildPluginManager;
    private final List<MavenProject> reactor;
    private final Log log;
    private final MavenProject project;
    private final MavenSession session;
    private final Set<String> versionOverridesSet = new HashSet<>();

    private Properties versionOverrides;
    private String versionOverridesPath;

    public MavenContext(final MavenProject project, final List<MavenProject> reactor, final MavenSession session,
                        final BuildPluginManager buildPluginManager, final Log log) {
        this.buildPluginManager = buildPluginManager;
        this.log = log;
        this.project = project;
        this.reactor = reactor;
        this.session = session;
    }

    public MavenProject getProject() {
        return project;
    }

    MavenSession getSession() {
        return session;
    }

    public Log getLog() {
        return log;
    }

    /**
     * Returns the Maven plugin version overrides for AMPS to use when invoking those plugins.
     *
     * @return a property set where the keys are Maven plugin artifactIds and the values are the versions to use
     * @see #setVersionOverrides(Set)
     * @see #setVersionOverridesPath(String)
     */
    @Nonnull
    public Properties getVersionOverrides() {
        if (versionOverrides == null) {
            this.versionOverrides = new Properties();

            if (versionOverridesPath != null) {
                final File overridesFile = new File(versionOverridesPath);
                if (overridesFile.isFile() && overridesFile.canRead()) {
                    loadVersionOverridesFrom(overridesFile);
                }
            }

            if (project.getPluginManagement() != null) {
                loadVersionOverridesFromPluginManagement();
            }
        }

        return versionOverrides;
    }

    private void loadVersionOverridesFromPluginManagement() {
        final Set<String> found = new HashSet<>();
        for (Plugin plg : project.getPluginManagement().getPlugins()) {
            if (versionOverridesSet.contains(plg.getArtifactId())) {
                versionOverrides.setProperty(plg.getArtifactId(), plg.getVersion());
                found.add(plg.getArtifactId());
            }
        }
        final Sets.SetView<String> diff = Sets.difference(versionOverridesSet, found);
        if (!diff.isEmpty()) {
            getLog().warn("Plugin artifactId(s) defined in 'versionOverrides' parameter but no" +
                    " associated entry found in <pluginManagement> section for " + diff);
        }
    }

    private void loadVersionOverridesFrom(final File overridesFile) {
        try (final FileInputStream overridesIn = openInputStream(overridesFile)) {
            versionOverrides.load(overridesIn);
        } catch (IOException e) {
            log.error("unable to load version overrides file as Properties: " +
                    overridesFile.getAbsolutePath(), e);
        }
    }

    public List<MavenProject> getReactor() {
        return reactor;
    }

    /**
     * Returns a new context with the same {@link BuildPluginManager} and {@link Log} as this one.
     *
     * @param project the new project
     * @param reactor the new reactor
     * @param session the new session
     * @return a new context
     */
    public MavenContext with(final MavenProject project, List<MavenProject> reactor, final MavenSession session) {
        return new MavenContext(project, reactor, session,
                this.buildPluginManager,
                this.log);
    }

    public ExecutionEnvironment getExecutionEnvironment() {
        return MojoExecutor.executionEnvironment(project, session, buildPluginManager);
    }

    /**
     * Sets the path to a properties file that maps Maven plugin artifactIds to the plugin versions that the user
     * wants AMPS to invoke (overriding the default verisons used by AMPS).
     *
     * This file is quietly ignored if it's either not an existing file or it's not readable.
     *
     * @param versionOverridesPath the path to an existing file
     * @see File#File(String)
     */
    public void setVersionOverridesPath(final String versionOverridesPath) {
        this.versionOverridesPath = versionOverridesPath;
    }

    /**
     * Sets the artifactIds of the Maven plugins for which the versions should be obtained from the
     * {@code pluginManagement} section of the user's POM.
     *
     * @param versionOverrides see description
     */
    void setVersionOverrides(final Set<String> versionOverrides) {
        this.versionOverridesSet.clear();
        if (versionOverrides != null) {
            this.versionOverridesSet.addAll(versionOverrides);
        }
    }

    /**
     * Returns the Maven plugin with the given coordinates and the version specified by this context.
     *
     * @param groupId    the groupId
     * @param artifactId the artifactId, assumed to be that of a Maven plugin, not some other artifact type
     * @return see description
     * @since 8.2
     */
    @Nonnull
    public Plugin getPlugin(final String groupId, final String artifactId) {
        return plugin(groupId, artifactId, getPluginVersion(artifactId));
    }

    private String getPluginVersion(final String artifactId) {
        final String defaultPluginVersion = DEFAULT_PLUGIN_VERSIONS.get(artifactId);
        final String pluginVersion = getVersionOverrides().getProperty(artifactId, defaultPluginVersion);
        if (isBlank(pluginVersion)) {
            throw new IllegalArgumentException(format("No version available for '%s'", artifactId));
        }
        return pluginVersion;
    }

    public String getBuildDirectory() {
        return project.getBuild().getDirectory();
    }
}
