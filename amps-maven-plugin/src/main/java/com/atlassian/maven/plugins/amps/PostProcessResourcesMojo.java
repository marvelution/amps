package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.i18n.RecordI18nUsages;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Allows processing files after the `process-resources` phase and well after all files should have
 * been added to the build output directory. Useful for running reports and analysis over all artifacts
 * generated for the maven artifact.
 *
 * @since 8.3
 */
@Mojo(name="post-process-resources")
public class PostProcessResourcesMojo extends AbstractAmpsMojo {
    /**
     * Whether to scan for usage of I18n keys in front-end files. Defaults to true.
     */
    @Parameter(defaultValue = "true")
    private boolean processI18nUsage;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (processI18nUsage) {
            final Build build = getMavenGoals().getContextProject().getBuild();
            new RecordI18nUsages().processProjectBuild(build);
        }
    }
}
