package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;
import com.google.common.annotations.VisibleForTesting;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.run;
import static java.lang.Boolean.FALSE;
import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Starts the configured products and loads the current plugin into them as applicable.
 */
@Mojo(name = "run", requiresDependencyResolution = ResolutionScope.TEST)
@Execute(phase = LifecyclePhase.PACKAGE)
public class RunMojo extends AbstractTestGroupsHandlerMojo {

    /**
     * Whether Maven should wait for the user to shut down the product; if {@code false}, Maven will shut down the
     * product and exit as soon as the product has finished starting up.
     */
    @Parameter(property = "wait", defaultValue = "true")
    private boolean wait;

    /**
     * Whether or not to write properties used by the plugin to amps.properties.
     * This can be useful in a CI environment, for example, to find out the product's base URL.
     */
    @Parameter(property = "amps.properties", required = true, defaultValue = "false")
    protected boolean writePropertiesToFile;

    /**
     * When this property is set to {@code true}, this Mojo will be executed on the last project in the Reactor.
     * This would typically be used when that project contains a P2 plugin.
     */
    @Parameter(property = "runLastProject", required = true, defaultValue = "false")
    @VisibleForTesting
    boolean runLastProject;

    /**
     * Set this property to override the {@code artifactId} of the project to be run.
     * <p>Example:
     * <ul>
     *     <li><code>mvn amps:run -DrunProject=my-project</code></li>
     * </ul>
     * </p>
     */
    @Parameter(property = "runProject")
    @VisibleForTesting
    String runProject;

    /**
     * The properties written to the file when {@link #writePropertiesToFile} is set.
     */
    protected final Map<String, String> propertiesToWriteToFile = new HashMap<>();

    protected void doExecute() throws MojoExecutionException, MojoFailureException {
        if (shouldSkipCurrentProject()) {
            getLog().info("Skipping execution");
            return;
        }
        getUpdateChecker().check();
        getAmpsPluginVersionChecker().checkAmpsVersionInPom(getAmpsPluginVersion(), getMavenContext().getProject());
        trackFirstRunIfNeeded();
        sendAnalyticsEvent(getAnalyticsEvent());
        final List<Product> products = getProductsToExecute();
        beforeStart(products);
        startProducts(products);
    }

    /**
     * Hook for subclasses to perform any final steps before the products are started.
     * This implementation does nothing.
     *
     * @param productsToExecute the products being started
     */
    protected void beforeStart(final List<Product> productsToExecute) throws MojoExecutionException {
        // No-op
    }

    /**
     * Returns the analytics event to send when this mojo runs. This implementation returns the "run" event.
     *
     * @return see description
     */
    @Nonnull
    protected AnalyticsEvent getAnalyticsEvent() {
        return run();
    }

    protected final void startProducts(final List<Product> products) throws MojoExecutionException {
        if (wait) {
            addStopProductsShutdownHook(products);
        }
        final long globalStartTime = System.nanoTime();
        setParallelMode(products);
        final List<StartupInformation> successMessages = new ArrayList<>();
        final boolean onlyOneProduct = products.size() == 1;
        for (final Product product : products) {
            successMessages.add(startProduct(product, onlyOneProduct));
        }
        if (writePropertiesToFile) {
            writePropertiesFile();
        }
        if (parallel) {
            waitForProducts(products, true);
        }
        // Give the messages once all applications are started
        logSuccessMessages(globalStartTime, successMessages);
        if (wait) {
            awaitShutdownCommand();
            // We don't stop products when -Dwait=false, because some projects rely on the
            // application running after the end of the RunMojo goal. The Invoker tests
            // check this behaviour.
            stopProducts(products);
        }
    }

    private StartupInformation startProduct(final Product product, final boolean onlyProduct)
            throws MojoExecutionException {
        if (product.isInstallPlugin() == null) {
            product.setInstallPlugin(shouldInstallPlugin());
        }
        if (shouldBuildTestPlugin()) {
            // Add artifacts for test console
            product.addBundledArtifacts(getTestFrameworkPlugins());
        }
        logStartingMessage(product);

        // Actually start the product
        final long startTime = System.nanoTime();
        final List<Node> nodes = getProductHandler(product.getId()).start(product);
        final long durationSeconds = NANOSECONDS.toSeconds(System.nanoTime() - startTime);

        // Log the success message
        final StartupInformation message =
                new StartupInformation(product, "started successfully", nodes, durationSeconds);
        if (!parallel) {
            getLog().info(message.toString());
        }

        if (writePropertiesToFile) {
            setPropertiesToWriteToFile(onlyProduct, product);
        }

        return message;
    }

    private void logSuccessMessages(final long globalStartTime, final Collection<StartupInformation> successMessages) {
        final long globalDurationSeconds = NANOSECONDS.toSeconds(System.nanoTime() - globalStartTime);
        if (successMessages.size() > 1 || parallel) {
            getLog().info("");
            getLog().info("=== Summary (total time " + globalDurationSeconds + "s):");
            // First show the log files
            for (final StartupInformation message : successMessages) {
                if (isNotBlank(message.getOutput())) {
                    getLog().info("Log available at: " + message.getOutput());
                }
            }
            // Then show the applications
            for (final StartupInformation message : successMessages) {
                getLog().info(message.toString());
            }
        }
    }

    private void logStartingMessage(final Product product) {
        getLog().info("");
        if (isBlank(product.getOutput())) {
            getLog().info(format("Starting %s...", product.getInstanceId()));
        } else {
            getLog().info(format("Starting %s... (see log at %s)", product.getInstanceId(), product.getOutput()));
        }
    }

    private void setPropertiesToWriteToFile(final boolean onlyProduct, final Product product) {
        final List<Integer> nodeWebPorts = product.getWebPorts();
        if (nodeWebPorts.isEmpty()) {
            throw new IllegalArgumentException("Expected at least one node");
        }
        final int firstWebPort = nodeWebPorts.get(0);
        if (onlyProduct) {
            propertiesToWriteToFile.put("http.port", String.valueOf(firstWebPort));
            propertiesToWriteToFile.put("context.path", product.getContextPath());
        }
        propertiesToWriteToFile.put("context." + product.getInstanceId() + ".path", product.getContextPath());
        // Even though the web ports will differ between nodes, publish these properties for backward compatibility
        propertiesToWriteToFile.put("http." + product.getInstanceId() + ".port", String.valueOf(firstWebPort));
        propertiesToWriteToFile.put("baseurl." + product.getInstanceId(), product.getBaseUrlForPort(firstWebPort));
        for (int i = 0; i < nodeWebPorts.size(); i++) {
            // But also publish all the ports via node-indexed properties, for users running multiple nodes
            final int webPort = nodeWebPorts.get(i);
            propertiesToWriteToFile.put("http." + product.getInstanceId() + ".port." + i, String.valueOf(webPort));
            propertiesToWriteToFile.put("baseurl." + product.getInstanceId() + "." + i, product.getBaseUrlForPort(webPort));
        }
    }

    private void awaitShutdownCommand() {
        getLog().info("Type Ctrl-C to shutdown gracefully");
        try {
            // Wait for the user to press Ctrl-D
            //noinspection StatementWithEmptyBody
            while (System.in.read() != -1);
        } catch (final Exception e) {
            // ignore
        }
    }

    private void addStopProductsShutdownHook(final Collection<Product> products) {
        getLog().debug("=======> ADDING SHUTDOWN HOOK\n");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            getLog().info("Running Shutdown Hook");
            try {
                stopProducts(products);
            } catch (MojoExecutionException e) {
                throw new IllegalStateException("Unable to shut down products in shutdown hook", e);
            }
        }, "AMPS product shutdown"));
    }

    /**
     * Only install a plugin if the installPlugin flag is true and the project is a jar. If the test plugin was built,
     * it will be installed as well.
     */
    private boolean shouldInstallPlugin() {
        Artifact artifact = getMavenContext().getProject().getArtifact();
        return installPlugin &&
                (artifact != null && !"pom".equalsIgnoreCase(artifact.getType()));
    }

    private void writePropertiesFile() throws MojoExecutionException {
        final Properties props = new Properties();

        for (Map.Entry<String, String> entry : propertiesToWriteToFile.entrySet()) {
            props.setProperty(entry.getKey(), entry.getValue());
        }

        final File ampsProperties = new File(getMavenContext().getProject().getBuild().getDirectory(), "amps.properties");
        try (OutputStream out = new FileOutputStream(ampsProperties)) {
            props.store(out, "");
        } catch (IOException e) {
            throw new MojoExecutionException("Error writing " + ampsProperties.getAbsolutePath(), e);
        }
    }

    /**
     * Wraps information about the startup of a product
     */
    private static class StartupInformation {

        private final List<Node> nodes;
        private final long durationSeconds;
        private final String event;
        private final Product product;

        StartupInformation(
                final Product product, final String event, final List<Node> nodes, final long durationSeconds) {
            this.nodes = unmodifiableList(nodes);
            this.product = requireNonNull(product);
            this.event = requireNonNull(event);
            this.durationSeconds = durationSeconds;
        }

        @Override
        public String toString() {
            final String mode = FALSE.equals(product.getSynchronousStartup()) ? " (asynchronously)" : "";
            String message = format("%s %s in %ds", product.getInstanceId(), event + mode, durationSeconds);
            final int[] httpPorts = nodes.stream()
                    .mapToInt(Node::getWebPort)
                    .filter(p -> p > 0)
                    .toArray();
            if (httpPorts.length > 0) {
                final String contextPath = "ROOT".equals(product.getContextPath()) ? "" : product.getContextPath();
                message += " at ";
                message += stream(httpPorts)
                        .mapToObj(port -> product.getProtocol() + "://localhost:" + port + contextPath)
                        .collect(joining(" and "));
            }
            return message;
        }

        /**
         * Returns the name of the log file for the started product.
         *
         * @return see description
         */
        public String getOutput() {
            return product.getOutput();
        }
    }

    /**
     * Determines whether this Mojo should skip the current project (e.g. in the reactor). By default it won't affect
     * execution, but it can be influenced by the {@link #runLastProject} and {@link #runProject} properties.
     *
     * @return <code>true</code> when this execution not should be skipped, <code>false</code> otherwise
     * @see com.atlassian.maven.plugins.amps.RunMojo#runLastProject
     * @see com.atlassian.maven.plugins.amps.RunMojo#runProject
     */
    protected final boolean shouldSkipCurrentProject() {
        final MavenContext mavenContext = getMavenContext();
        final MavenProject currentProject = mavenContext.getProject();

        getLog().debug(format("Current project ID: %s, runLastProject=%b, runProject=%s",
                currentProject.getArtifactId(), runLastProject, runProject));

        // check explicit runProject artifact name
        if (isNotBlank(runProject)) {
            return !equalsIgnoreCase(runProject, currentProject.getArtifactId());
        }

        // otherwise check runLastProject flag
        if (runLastProject) {
            final List<MavenProject> reactor = mavenContext.getReactor();
            if (reactor == null || reactor.isEmpty()) {
                return false;
            }
            final MavenProject lastProject = reactor.get(reactor.size() - 1);
            return !currentProject.equals(lastProject);
        }

        return false;
    }
}
