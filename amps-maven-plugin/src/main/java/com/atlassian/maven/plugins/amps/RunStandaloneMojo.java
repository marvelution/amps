package com.atlassian.maven.plugins.amps;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginManagement;
import org.apache.maven.model.locator.DefaultModelLocator;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.project.ProjectBuildingResult;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.runStandalone;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.FECRU;
import static java.util.Collections.singletonList;
import static java.util.regex.Pattern.matches;

/**
 * Starts an Atlassian product without a plugin project.
 */
@Mojo(name = "run-standalone", requiresProject = false)
public class RunStandaloneMojo extends AbstractProductHandlerMojo {

    private static final String GROUP_ID = "com.atlassian.amps";
    private static final String ARTIFACT_ID = "standalone";

    private static final String AMPS_FECRU_URL =
            "https://packages.atlassian.com/content/repositories/atlassian-public/com/atlassian/fecru/amps-fecru/";

    @Component
    private ProjectBuilder projectBuilder;

    private Artifact getStandaloneArtifact() {
        final String version = getPluginInformation().getVersion();
        return repositorySystem.createProjectArtifact(GROUP_ID, ARTIFACT_ID, version);
    }

    protected String getAmpsGoal() {
        return "run";
    }

    protected void doExecute() throws MojoExecutionException {
        getUpdateChecker().check();
        trackFirstRunIfNeeded();
        sendAnalyticsEvent(runStandalone());
        try {
            final MavenGoals goals = createMavenGoals(projectBuilder);
            /*
             * When we run with Maven 3, the configuration from the POM isn't automatically picked up by the mojo
             * executor, so we grab it manually from pluginManagement.
             */
            final PluginManagement mgmt = goals.getContextProject().getBuild().getPluginManagement();
            final Plugin plugin = mgmt.getPluginsAsMap().get("com.atlassian.maven.plugins:amps-maven-plugin");
            final Xpp3Dom configuration = (Xpp3Dom) plugin.getConfiguration();
            goals.executeAmpsRecursively(getPluginInformation().getVersion(), getAmpsGoal(), configuration);
        } catch (Exception e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    protected MavenGoals createMavenGoals(final ProjectBuilder projectBuilder)
            throws ProjectBuildingException, IOException {
        // overall goal here is to create a new MavenContext / MavenGoals for the standalone project
        final MavenContext oldContext = getMavenContext();
        final MavenSession oldSession = oldContext.getSession();

        // Use a different dir per version.
        // (If we use the same directory, we get support pain when the old version from an existing directory gets
        // restarted instead of the version they specified on the command line.)
        final Properties systemProperties = oldSession.getSystemProperties();

        // Defaults to refapp if not set
        final String product = getPropertyOrDefault(systemProperties, "product", "refapp");

        // Only the inner execution in com.atlassian.amps:standalone has the actual latest version that will be used if
        // they didn't specify a version number. Too hard to restructure; 'LATEST' is better than nothing.
        final String productVersion = getPropertyOrDefault(systemProperties, "product.version", "LATEST");

        // If we're building FECRU, not using LATEST and have entered a short version name then get the full version name
        if (product.equals(FECRU) && !"LATEST".equals(productVersion) && !matches(".*[0-9]{14}$", productVersion)) {
            final String fullProductVersionMessage = getFullVersion(productVersion)
                    .map(version -> generateFullVersionMessage(productVersion, version))
                    .orElseGet(() -> generateNoVersionMessage(productVersion));
            getLog().error("=======================================================================");
            getLog().error(fullProductVersionMessage);
            getLog().error("=======================================================================");
            System.exit(1);
        }

        final File base = new File("amps-standalone-" + product + "-" + productVersion).getAbsoluteFile();

        final ProjectBuildingRequest pbr = oldSession.getProjectBuildingRequest();

        // hack #1 from before
        pbr.setRemoteRepositories(oldSession.getCurrentProject().getRemoteArtifactRepositories());
        pbr.setPluginArtifactRepositories(oldSession.getCurrentProject().getPluginArtifactRepositories());
        pbr.getSystemProperties().setProperty("project.basedir", base.getPath());

        final ProjectBuildingResult result = projectBuilder.build(getStandaloneArtifact(), false, pbr);

        final List<MavenProject> newReactor = singletonList(result.getProject());

        final MavenSession newSession = oldSession.clone();
        newSession.setProjects(newReactor);

        // Horrible hack #3 from before
        result.getProject().setFile(new DefaultModelLocator().locatePom(base));

        final MavenContext newContext = oldContext.with(
                result.getProject(),
                newReactor,
                newSession);

        return new MavenGoals(newContext, mojoExecutorWrapper);
    }

    private String generateNoVersionMessage(String productVersion) {
        return "There is no valid full version of " + productVersion + ". Please double check your input\n" +
                "\tThe full list of versions can be found at: " +
                "\n\thttps://packages.atlassian.com/content/repositories/atlassian-public/com/atlassian/fecru/amps-fecru/";
    }

    private String generateFullVersionMessage(String productVersion, String version) {
        return "You entered: " + productVersion + " as your version, this is not a version." +
                "\n\tDid you mean?: " + version + "\n\tPlease re-run with the correct version (atlas-run-standalone --product " +
                "fecru -v " + version + ")";
    }

    protected Optional<String> getFullVersion(final String versionInput) throws IOException {
        final Pattern p = Pattern.compile(".*>(" + versionInput + "-[0-9]{14}).*");
        Matcher m;
        String correctVersion = null;
        final URLConnection connection = new URL(AMPS_FECRU_URL).openConnection();
        try (final BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            // Read each line of the page and parse it to see the version number
            while ((inputLine = in.readLine()) != null) {
                m = p.matcher(inputLine);
                if (m.matches()) {
                    correctVersion = m.group(1);
                }
            }
        }
        return Optional.ofNullable(correctVersion);
    }

    private String getPropertyOrDefault(Properties systemProperties, String property, String defaultValue) {
        String value = systemProperties.getProperty(property);
        return value == null ? defaultValue : value;
    }
}
