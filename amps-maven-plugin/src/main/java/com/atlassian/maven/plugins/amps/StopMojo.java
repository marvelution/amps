package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

import java.util.List;

/**
 * Stop the webapps started by RunMojo. This may be useful when you set {@code -Dwait=false} for the RunMojo and you
 * want the products to make a clean shutdown.
 */
@Mojo(name = "stop")
public class StopMojo extends RunMojo {

    @Override
    protected void doExecute() throws MojoExecutionException, MojoFailureException {
        final List<Product> products = getProductsToExecute();
        setParallelMode(products);
        stopProducts(products);
    }
}
