package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.util.HashSet;
import java.util.Set;

/**
 * Checks that the current project has no platform modules in {@code compile} scope, even transitively, so that it does
 * not bundle any such artifacts and thereby cause classloading problems at runtime.
 */
@Mojo(name = "validate-banned-dependencies", requiresDependencyResolution = ResolutionScope.COMPILE)
public class ValidateBannedDependenciesMojo extends AbstractAmpsMojo {

    @Parameter(property = "skipBanningDependencies", defaultValue = "false")
    private boolean skipBanningDependencies;

    @Parameter(property = "banningExcludes")
    private Set<String> banningExcludes = new HashSet<>();

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipBanningDependencies) {
            getLog().info("dependencies validation skipped");
        } else {
            getMavenGoals().validateBannedDependencies(banningExcludes);
        }
    }
}
