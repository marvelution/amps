package com.atlassian.maven.plugins.amps.analytics.event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * An analytics event sent by AMPS.
 *
 * @since 8.2.4
 */
public interface AnalyticsEvent {

    /**
     * Returns the name of the user action.
     *
     * @return a non-blank name
     */
    @Nonnull
    String getAction();

    /**
     * Returns the event label, if any.
     *
     * @return see description
     */
    @Nullable
    String getLabel();
}
