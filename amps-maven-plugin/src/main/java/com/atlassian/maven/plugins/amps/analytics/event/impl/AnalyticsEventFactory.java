package com.atlassian.maven.plugins.amps.analytics.event.impl;

import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.DefaultAnalyticsEvent.labelledEvent;
import static com.atlassian.maven.plugins.amps.analytics.event.impl.DefaultAnalyticsEvent.unlabelledEvent;

/**
 * Static factory for {@link AnalyticsEvent}s.
 *
 * @since 8.2.4 was previously a group of Strings in {@code GoogleAmpsTracker}.
 */
public final class AnalyticsEventFactory {

    /**
     * Returns an analytics event for the user creating a new P2 plugin.
     *
     * @return see description
     */
    public static AnalyticsEvent createPlugin() {
        return unlabelledEvent("Create Plugin");
    }

    /**
     * Returns an analytics event for the user creating a new P2 plugin module.
     *
     * @param productId the product ID
     * @param moduleType the type of module being created
     * @return see description
     */
    public static AnalyticsEvent createPluginModule(final String productId, final String moduleType) {
        return labelledEvent("Create Plugin Module", productId + ":" + moduleType);
    }

    /**
     * Returns an analytics event for the user running a product in debug mode.
     *
     * @return see description
     * @see #run()
     */
    public static AnalyticsEvent debug() {
        return unlabelledEvent("Debug");
    }

    /**
     * Returns an analytics event for the user releasing their plugin.
     *
     * @param productId the product ID
     * @param ampsPluginVersion the AMPS plugin version
     * @return see description
     * @see #run()
     */
    public static AnalyticsEvent release(final String productId, final String ampsPluginVersion) {
        return labelledEvent("Release", productId + ":" + ampsPluginVersion);
    }

    /**
     * Returns an analytics event for the user running a product, not in debug mode.
     *
     * @return see description
     * @see #debug()
     */
    public static AnalyticsEvent run() {
        return unlabelledEvent("Run");
    }

    /**
     * Returns an analytics event for the user running a product in standalone mode.
     *
     * @return see description
     */
    public static AnalyticsEvent runStandalone() {
        return unlabelledEvent("Run Standalone");
    }

    /**
     * Returns an analytics event for the user running the SDK for the first time.
     *
     * @param ampsPluginVersion the AMPS plugin version
     * @return see description
     */
    public static AnalyticsEvent sdkFirstRun(final String ampsPluginVersion) {
        return labelledEvent("SDK First Run", ampsPluginVersion);
    }

    private AnalyticsEventFactory() {
        throw new UnsupportedOperationException("Not for instantiation");
    }
}
