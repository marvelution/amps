package com.atlassian.maven.plugins.amps.analytics.event.impl;

import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;

/**
 * The default {@link AnalyticsEvent} implementation.
 *
 * @since 8.2.4
 */
@ParametersAreNonnullByDefault
class DefaultAnalyticsEvent implements AnalyticsEvent {

    /**
     * Factory method for a labelled event.
     *
     * @param action the action
     * @param label the label
     * @return the event
     */
    static AnalyticsEvent labelledEvent(final String action, final String label) {
        return new DefaultAnalyticsEvent(action, label);
    }

    /**
     * Factory method for an unlabelled event.
     *
     * @param action the action
     * @return the event
     */
    static AnalyticsEvent unlabelledEvent(final String action) {
        return new DefaultAnalyticsEvent(action, null);
    }

    private final String action;
    private final String label;

    private DefaultAnalyticsEvent(final String action, @Nullable final String label) {
        this.action = requireNonNull(action);
        this.label = label;
    }

    @Override
    @Nonnull
    public String getAction() {
        return action;
    }

    @Override
    @Nullable
    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        if (label == null) {
            return action;
        }
        return action + " - " + label;
    }
}
