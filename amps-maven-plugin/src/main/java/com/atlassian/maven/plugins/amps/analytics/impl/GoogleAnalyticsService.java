package com.atlassian.maven.plugins.amps.analytics.impl;

import com.atlassian.maven.plugins.amps.analytics.AnalyticsService;
import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;
import com.atlassian.maven.plugins.amps.analytics.visitordata.PreferencesVisitorDataDao;
import com.atlassian.maven.plugins.amps.analytics.visitordata.VisitorDataDao;
import com.atlassian.maven.plugins.amps.analytics.visitordata.VisitorDataMarshaller;
import com.dmurph.tracking.AnalyticsConfigData;
import com.dmurph.tracking.JGoogleAnalyticsTracker;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.dmurph.tracking.JGoogleAnalyticsTracker.DispatchMode.MULTI_THREAD;
import static com.dmurph.tracking.JGoogleAnalyticsTracker.GoogleAnalyticsVersion.V_4_7_2;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * An {@link AnalyticsService} that sends events to GoogleAnalytics.
 *
 * @since version 8.2.4 was {@code GoogleAmpsTracker}.
 */
@ParametersAreNonnullByDefault
public class GoogleAnalyticsService implements AnalyticsService {

    private static final String CATEGORY_PREFIX = "AMPS";
    private static final String TRACKING_CODE = "UA-6032469-43";

    public static AnalyticsService newGoogleAnalyticsService(
            final String productId, final String sdkVersion, final String ampsVersion, final Log log) {
        final VisitorDataDao visitorDataDao = new PreferencesVisitorDataDao(log, new VisitorDataMarshaller());
        final AnalyticsConfigData config = new AnalyticsConfigData(TRACKING_CODE, visitorDataDao.load());
        config.setFlashVersion(ampsVersion);
        final JGoogleAnalyticsTracker tracker = new JGoogleAnalyticsTracker(config, V_4_7_2);
        tracker.setDispatchMode(MULTI_THREAD);
        final UserAgent userAgent = UserAgent.getInstance(ampsVersion, sdkVersion);
        final String eventCategory = isBlank(productId) ? CATEGORY_PREFIX : CATEGORY_PREFIX + ":" + productId;
        return new GoogleAnalyticsService(config, tracker, log, eventCategory, userAgent, visitorDataDao);
    }

    private final AnalyticsConfigData config;
    private final JGoogleAnalyticsTracker tracker;
    private final Log log;
    private final String eventCategory;
    private final UserAgent userAgent;
    private final VisitorDataDao visitorDataDao;

    GoogleAnalyticsService(final AnalyticsConfigData config, final JGoogleAnalyticsTracker tracker, final Log log,
                           final String eventCategory, final UserAgent userAgent, final VisitorDataDao visitorDataDao) {
        this.config = requireNonNull(config);
        this.tracker = requireNonNull(tracker);
        this.log = requireNonNull(log);
        this.eventCategory = requireNonNull(eventCategory);
        this.userAgent = requireNonNull(userAgent);
        this.visitorDataDao = requireNonNull(visitorDataDao);
    }

    @Override
    public void send(final AnalyticsEvent event) {
        try {
            System.setProperty("http.agent", userAgent.getHeaderValue());
            log.info("Sending event to Google Analytics: " + eventCategory + " - " + event);
            tracker.trackEvent(eventCategory, event.getAction(), event.getLabel());
            visitorDataDao.save(config.getVisitorData());
        } catch (final Exception ignored) {
            // Ignore
        }
    }
}
