package com.atlassian.maven.plugins.amps.analytics.impl;

import java.util.Locale;

import static java.util.Objects.requireNonNull;

/**
 * The user agent for sending analytics.
 *
 * @since 8.2.4 was code in {@code GoogleAmpsTracker}
 */
public class UserAgent {

    /**
     * Factory method whose result depends on the environment and system properties.
     *
     * @param ampsVersion the AMPS version
     * @param sdkVersion the SDK version
     * @return a new instance
     */
    public static UserAgent getInstance(final String ampsVersion, final String sdkVersion) {
        final boolean usingSdk = System.getenv("ATLAS_VERSION") != null;
        final String product = usingSdk ? "Atlassian-SDK" : "Atlassian-AMPS-Plugin";
        final String productVersion = usingSdk ? sdkVersion : ampsVersion;
        return new UserAgent(
                Locale.getDefault(),
                System.getProperty("os.name"),
                System.getProperty("os.version"),
                product,
                productVersion
        );
    }

    private static final String HEADER_VALUE_FORMAT = "%s/%s(compatible; %s %s; %s %s; %s-%s)";

    private final Locale locale;
    private final String operatingSystemName;
    private final String operatingSystemVersion;
    private final String product;
    private final String productVersion;

    UserAgent(final Locale locale, final String operatingSystemName, final String operatingSystemVersion,
              final String product, final String productVersion) {
        this.locale = requireNonNull(locale);
        this.operatingSystemName = requireNonNull(operatingSystemName);
        this.operatingSystemVersion = requireNonNull(operatingSystemVersion);
        this.product = requireNonNull(product);
        this.productVersion = requireNonNull(productVersion);
    }

    /**
     * Returns the value of the HTTP {@code User-Agent} header for this user agent.
     *
     * @return a valid header value
     */
    public String getHeaderValue() {
        return String.format(
                HEADER_VALUE_FORMAT,
                product,
                productVersion,
                product,
                productVersion,
                operatingSystemName,
                operatingSystemVersion,
                locale.getLanguage().toLowerCase(),
                locale.getCountry().toLowerCase()
        );
    }
}
