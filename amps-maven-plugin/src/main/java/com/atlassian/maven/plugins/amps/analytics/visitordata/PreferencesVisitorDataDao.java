package com.atlassian.maven.plugins.amps.analytics.visitordata;

import com.atlassian.maven.plugins.amps.util.VisitorDataDaoPreferencesNode;
import com.dmurph.tracking.VisitorData;
import com.google.common.annotations.VisibleForTesting;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.Consumer;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static com.dmurph.tracking.VisitorData.newVisitor;
import static java.util.Objects.requireNonNull;
import static java.util.prefs.Preferences.userNodeForPackage;

/**
 * A {@link VisitorDataDao} that uses {@link java.util.prefs.Preferences} for its persistence layer.
 *
 * @since 8.2.4 was in {@code GoogleAmpsTracker}
 */
@ParametersAreNonnullByDefault
public class PreferencesVisitorDataDao implements VisitorDataDao {

    // The preferences key under which this DAO persists visitor data.
    // Changing this will invalidate all of this DAO's data stored on ecosystem users' machines.
    @VisibleForTesting
    static final String PREF_NAME = "ga_visitor_data";

    private final Log log;
    private final VisitorDataMarshaller marshaller;

    public PreferencesVisitorDataDao(final Log log, final VisitorDataMarshaller marshaller) {
        this.log = requireNonNull(log);
        this.marshaller = requireNonNull(marshaller);
    }

    @Nonnull
    public VisitorData load() {
        final String persistedVisitorData = getPreferencesPersistenceNode().get(PREF_NAME, null);
        VisitorData visitorData;
        if (persistedVisitorData == null) {
            visitorData = newVisitor();
            save(visitorData);
        } else {
            try {
                visitorData = marshaller.unmarshalAndUpdate(persistedVisitorData);
            } catch (Exception e) {
                log.warn("Couldn't parse visitor data from prefs; deleting");
                updatePrefs(prefs -> prefs.remove(PREF_NAME));
                visitorData = newVisitor();
                save(visitorData);
            }
        }
        return visitorData;
    }

    public void save(final VisitorData visitor) {
        updatePrefs(prefs -> prefs.put(PREF_NAME, marshaller.marshal(visitor)));
    }

    private static void updatePrefs(final Consumer<Preferences> updater) {
        final Preferences prefs = getPreferencesPersistenceNode();
        try {
            updater.accept(prefs);
            prefs.flush();
        } catch (final BackingStoreException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Returns the node in the Java {@link Preferences} tree where this DAO stores {@link VisitorData}.
     *
     * @return see description
     */
    @Nonnull
    @VisibleForTesting
    static Preferences getPreferencesPersistenceNode() {
        return userNodeForPackage(VisitorDataDaoPreferencesNode.class);
    }
}
