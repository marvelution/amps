package com.atlassian.maven.plugins.amps.analytics.visitordata;

import com.dmurph.tracking.VisitorData;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A data access object for {@link VisitorData}.
 *
 * @since 8.2.4 was previously mixed into {@code GoogleAmpsTracker}
 */
@ParametersAreNonnullByDefault
public interface VisitorDataDao {

    /**
     * Returns the persisted {@link VisitorData}.
     *
     * @return see description
     */
    @Nonnull
    VisitorData load();

    /**
     * Persists the given {@link VisitorData}.
     *
     * @param visitorData the data to persist
     */
    void save(VisitorData visitorData);
}
