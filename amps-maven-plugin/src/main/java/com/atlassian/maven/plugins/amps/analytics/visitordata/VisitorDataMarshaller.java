package com.atlassian.maven.plugins.amps.analytics.visitordata;

import com.dmurph.tracking.VisitorData;

import static com.dmurph.tracking.VisitorData.newSession;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * Marhsals and unmarshals {@link com.dmurph.tracking.VisitorData} to/from Strings suitable for persistence via the
 * Java {@link java.util.prefs.Preferences} framework.
 *
 * @since 8.2.4 was in {@code GoogleAmpsTracker}
 */
public class VisitorDataMarshaller {

    /**
     * Marshals the given object into a String.
     *
     * @param visitorData the object to marshal
     * @return see description
     */
    public String marshal(final VisitorData visitorData) {
        final Object[] fields = {
                visitorData.getVisitorId(),        // index 0 in unmarshal below
                visitorData.getTimestampFirst(),   // index 1 in unmarshal below
                visitorData.getTimestampCurrent(), // index 2 in unmarshal below
                visitorData.getVisits()            // index 3 in unmarshal below
        };
        return join(fields, ':');
    }

    /**
     * Unmarshals the given string back into an object, mutating it to contain:
     * <ul>
     *     <li>the stored visitor ID</li>
     *     <li>the stored "first" timestamp</li>
     *     <li>the stored "current" timestamp as the "last" timestamp</li>
     *     <li>the current system time as the "current" timestamp</li>
     *     <li>the "visits" counter incremented by one</li>
     * </ul>
     *
     * @param visitorDataAsString the string to unmarshal
     * @return see description
     */
    public VisitorData unmarshalAndUpdate(final String visitorDataAsString) {
        final String[] parts = visitorDataAsString.split(":");
        final int visitorId = Integer.parseInt(parts[0]);
        final long timestampFirst = Long.parseLong(parts[1]);
        final long timestampLast = Long.parseLong(parts[2]);
        final int visits = Integer.parseInt(parts[3]);
        // We deliberately store the previous "current" time as the new "last" time
        return newSession(visitorId, timestampFirst, timestampLast, visits);
    }
}
