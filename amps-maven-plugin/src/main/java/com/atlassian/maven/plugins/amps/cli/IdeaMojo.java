package com.atlassian.maven.plugins.amps.cli;

import com.atlassian.maven.plugins.amps.AbstractAmpsMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Installs an IDEA plugin that sends commands to a listening CLI port. This most probably relates to the CLI that used
 * to be startable via the {@code atlas-cli} SDK command. Given that this CLI has been removed, this Mojo probably does
 * nothing useful.
 */
@Mojo(name = "idea", requiresProject = false)
public class IdeaMojo extends AbstractAmpsMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        getMavenGoals().installIdeaPlugin();
    }
}