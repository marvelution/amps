package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.LibArtifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor.Element;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;

/**
 * Base class for concrete {@link DatabaseType} implementations.
 */
@ParametersAreNonnullByDefault
public abstract class AbstractDatabase implements DatabaseType {

    protected final Log log;

    private final boolean hasSchema;
    private final String driverClass;
    private final String ofBizType;
    private final String urlPrefix;

    protected LibArtifact lib;

    /**
     * Constructor.
     *
     * @param log the Maven logger
     * @param hasSchema whether this type of database uses a schema
     * @param ofBizType the name used by OfBiz for this type of database
     * @param driverClass the name of the Java class that provides the JDBC driver for this type of database
     * @param urlPrefix the JDBC URL prefix used by this type of database
     */
    protected AbstractDatabase(final Log log, final boolean hasSchema, final String ofBizType, final String driverClass,
                               final String urlPrefix) {
        this.log = requireNonNull(log);
        this.hasSchema = hasSchema;
        this.ofBizType = requireNonBlank(ofBizType, "dbType");
        this.driverClass = requireNonBlank(driverClass, "driverClass");
        this.urlPrefix = requireNonBlank(urlPrefix, "urlPrefix");
    }

    private static String requireNonBlank(final String string, final String name) {
        if (isBlank(string)) {
            throw new IllegalArgumentException(format("Invalid %s '%s'", name, string));
        }
        return string;
    }

    protected abstract String dropDatabase(DataSource dataSource) throws MojoExecutionException;

    protected abstract String createDatabase(DataSource dataSource) throws MojoExecutionException;

    protected abstract String dropUser(DataSource dataSource);

    protected abstract String createUser(DataSource dataSource);

    protected abstract String grantPermissionForUser(DataSource dataSource) throws MojoExecutionException;

    protected Xpp3Dom systemDatabaseConfiguration(final DataSource dataSource) {
        return configuration(
                nonBlankElement("driver", dataSource.getDriver()),
                nonBlankElement("url", dataSource.getSystemUrl()),
                nonBlankElement("username", dataSource.getSystemUsername()),
                element(name("password"), dataSource.getSystemPassword()),
                element(name("autocommit"), "true")
        );
    }

    private static Element nonBlankElement(final String name, final String value) {
        return element(name(name), requireNonBlank(value, name));
    }

    protected Xpp3Dom productDatabaseConfiguration(final DataSource dataSource) {
        return configuration(
                nonBlankElement("driver", dataSource.getDriver()),
                nonBlankElement("url", dataSource.getUrl()),
                nonBlankElement("username", dataSource.getUsername()),
                element(name("password"), dataSource.getPassword()),
                // we need commit transaction for drop database and then create them again
                element(name("autocommit"), "true")
        );
    }

    /**
     * Returns the name of the database to which the given {@link DataSource} points.
     *
     * @param dataSource the datasource
     * @return see description
     * @throws MojoExecutionException if something goes wrong
     */
    @Nullable
    protected abstract String getDatabaseName(DataSource dataSource) throws MojoExecutionException;

    @Override
    @Nonnull
    public List<Dependency> getSqlMavenDependencies(final DataSource dataSource) {
        if (dataSource.getLibArtifacts() == null) {
            return emptyList();
        }
        return dataSource.getLibArtifacts().stream()
                .map(LibArtifact::asDependency)
                .collect(toList());
    }

    @Override
    @Nonnull
    public Xpp3Dom getSqlMavenFileImportConfiguration(final DataSource dataSource) {
        Xpp3Dom pluginConfiguration = productDatabaseConfiguration(dataSource);
        pluginConfiguration.addChild(
                element(name("srcFiles"),
                        element(name("srcFile"), dataSource.getDumpFilePath())).toDom()
        );
        return pluginConfiguration;
    }

    @Override
    public Xpp3Dom getExecMavenToolImportConfiguration(final DataSource dataSource)
            throws MojoExecutionException {
        return null;
    }

    @Override
    public final boolean hasSchema() {
        return hasSchema;
    }

    @Nonnull
    @Override
    public String getOfBizName() {
        return ofBizType;
    }

    @Override
    public boolean isTypeOf(final DataSource dataSource) {
        return startsWithExpectedPrefix(dataSource.getUrl()) && driverClass.equals(dataSource.getDriver());
    }

    private boolean startsWithExpectedPrefix(@Nullable final String jdbcUrl) {
        return jdbcUrl != null && jdbcUrl.trim().startsWith(urlPrefix);
    }
}
