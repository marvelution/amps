package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.product.ImportMethod;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.File;
import java.util.function.Function;

import static com.atlassian.maven.plugins.amps.product.ImportMethod.IMPDP;
import static java.lang.Boolean.TRUE;
import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;

/**
 * Base class for Oracle-related {@link DatabaseType} implementations.
 */
@ParametersAreNonnullByDefault
public abstract class AbstractOracleDatabase extends AbstractDatabase {

    protected static final String DATA_PUMP_DIR = "DATA_PUMP_DIR";

    private static final String ARGUMENT = "argument";

    protected AbstractOracleDatabase(final Log log, final String dbType) {
        super(log, false, dbType, "oracle.jdbc.OracleDriver", "jdbc:oracle");
    }

    @Override
    protected String dropDatabase(final DataSource dataSource) {
        return null;
    }

    @Override
    protected String dropUser(final DataSource dataSource) {
        return null;
    }

    @Override
    protected String createDatabase(final DataSource dataSource) {
        return null;
    }

    @Override
    protected String createUser(final DataSource dataSource) {
        return null;
    }

    @Override
    protected String grantPermissionForUser(final DataSource dataSource) {
        return null;
    }

    @Override
    protected String getDatabaseName(final DataSource dataSource) {
        return dataSource.getSchema();
    }

    @Override
    public Xpp3Dom getExecMavenToolImportConfiguration(final DataSource dataSource) throws MojoExecutionException {
        Xpp3Dom configDatabaseTool = null;
        if (IMPDP.equals(ImportMethod.getValueOf(dataSource.getImportMethod()))) {
            final File dumpFile = new File(dataSource.getDumpFilePath());
            final File dumpFileDirectory = dumpFile.getParentFile();
            final String dumpFileName = dumpFile.getName();
            // grant read, write and executable on dump file and parent directory for Oracle to execute import - impdp
            setExecutableReadWrite(dumpFile);
            setExecutableReadWrite(dumpFileDirectory);
            configDatabaseTool = configuration(
                    element(name("executable"), "impdp"),
                    element(name("arguments"),
                            element(name(ARGUMENT), dataSource.getUsername() + "/" + dataSource.getPassword()),
                            element(name(ARGUMENT), "DUMPFILE=" + dumpFileName),
                            element(name(ARGUMENT), "DIRECTORY=" + DATA_PUMP_DIR)
                    )
            );
        }
        return configDatabaseTool;
    }

    private static void setExecutableReadWrite(final File file) throws MojoExecutionException {
        ensure(file, f -> f.setExecutable(true, false));
        ensure(file, f -> f.setReadable(true, false));
        ensure(file, f -> f.setWritable(true, false));
    }

    private static void ensure(final File file, final Function<File, Boolean> action) throws MojoExecutionException {
        if (!TRUE.equals(action.apply(file))) {
            throw new MojoExecutionException("Could not modify the file " + file.getAbsolutePath());
        }
    }

    @Override
    @Nonnull
    public Xpp3Dom getSqlMavenCreateConfiguration(final DataSource dataSource) {
        // In Oracle, "user" and "schema" are almost the same concept; create/drop user also creates/drops schema.
        final String sql = getSqlToDropAndCreateUser(dataSource);
        log.info("Oracle initialization database SQL: " + sql);
        final Xpp3Dom sqlPluginConfiguration = systemDatabaseConfiguration(dataSource);
        addChild(sqlPluginConfiguration, "sqlCommand", sql);
        addChild(sqlPluginConfiguration, "delimiter", "/");
        addChild(sqlPluginConfiguration, "delimiterType", "row");
        addChild(sqlPluginConfiguration, "driverProperties", dataSource.getSqlPluginJdbcDriverProperties());
        return sqlPluginConfiguration;
    }

    /**
     * Subclasses should return the SQL (or PL/SQL) command(s) necessary to
     * drop and recreate the product user (i.e. schema). It can be assumed that
     * there is a SYSDBA session in progress.
     *
     * @param dataSource the datasource
     * @return a valid SQL string
     */
    @Nonnull
    protected abstract String getSqlToDropAndCreateUser(DataSource dataSource);

    /**
     * Adds a child node with the given name and value to the given DOM node.
     *
     * @param parentNode the node to receive a new child
     * @param childName  the name of the new child node
     * @param childValue the value of the new child node
     */
    private static void addChild(final Xpp3Dom parentNode, final String childName, final String childValue) {
        parentNode.addChild(element(name(childName), childValue).toDom());
    }
}
