package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import org.apache.maven.model.Dependency;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

/**
 * The Hsqldb database type.
 *
 * @since 8.3
 */
@ParametersAreNonnullByDefault
public class Hsql implements DatabaseType {

    @Nonnull
    @Override
    public Xpp3Dom getSqlMavenCreateConfiguration(final DataSource dataSource) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public Xpp3Dom getSqlMavenFileImportConfiguration(final DataSource dataSource) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public List<Dependency> getSqlMavenDependencies(final DataSource dataSource) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nullable
    @Override
    public Xpp3Dom getExecMavenToolImportConfiguration(final DataSource dataSource) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public String getOfBizName() {
        return "hsql";
    }

    @Override
    public boolean isTypeOf(final DataSource dataSource) {
        return dataSource.getUrl() != null && dataSource.getUrl().startsWith("jdbc:hsqldb");
    }

    @Override
    public boolean hasSchema() {
        return true;
    }
}
