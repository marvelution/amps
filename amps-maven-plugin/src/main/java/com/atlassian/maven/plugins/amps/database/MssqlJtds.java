package com.atlassian.maven.plugins.amps.database;

import org.apache.maven.plugin.logging.Log;

/**
 * An MSSQL database that uses the SourceForge JTDS JDBC driver.
 */
public class MssqlJtds extends AbstractMssqlDatabase {

    public MssqlJtds(final Log log) {
        super(log, "net.sourceforge.jtds.jdbc.Driver", "jdbc:jtds");
    }
}
