package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import org.apache.maven.plugin.logging.Log;

import java.io.File;

public class Oracle10g extends AbstractOracleDatabase {

    private static final String DROP_AND_CREATE_USER =
            "DECLARE\n"
                    + "    v_count INTEGER := 0;\n"
                    + "BEGIN\n"
                    + "    SELECT COUNT (1) INTO v_count FROM dba_users WHERE username = UPPER ('%s'); \n"
                    + "    IF v_count != 0\n"
                    + "    THEN\n"
                    + "        EXECUTE IMMEDIATE('DROP USER %s CASCADE');\n"
                    + "    END IF;\n"
                    + "    v_count := 0; \n"
                    + "    SELECT COUNT (1) INTO v_count FROM dba_tablespaces WHERE tablespace_name = UPPER('product'); \n"
                    + "    IF v_count != 0\n"
                    + "    THEN\n"
                    + "        EXECUTE IMMEDIATE('DROP TABLESPACE product INCLUDING CONTENTS AND DATAFILES');\n"
                    + "    END IF;\n"

                    + "    EXECUTE IMMEDIATE(q'{CREATE TABLESPACE product DATAFILE '/tmp/product.dbf' SIZE 32m AUTOEXTEND ON NEXT 32m MAXSIZE 4096m EXTENT MANAGEMENT LOCAL}');\n"
                    + "    EXECUTE IMMEDIATE('CREATE USER %s IDENTIFIED BY %s DEFAULT TABLESPACE product QUOTA UNLIMITED ON product');\n"
                    + "    EXECUTE IMMEDIATE('GRANT CONNECT, RESOURCE, IMP_FULL_DATABASE TO %s');\n"
                    + "    EXECUTE IMMEDIATE(q'{CREATE OR REPLACE DIRECTORY %s AS '%s'}');\n"
                    + "    EXECUTE IMMEDIATE('GRANT READ, WRITE ON DIRECTORY %s TO %s');\n"
                    + "END;\n"
                    + "/";

    public Oracle10g(final Log log) {
        super(log, "oracle10g");
    }

    protected String getSqlToDropAndCreateUser(final DataSource dataSource) {
        final String dumpFileDirectoryPath = (new File(dataSource.getDumpFilePath())).getParent();
        final String username = dataSource.getUsername();
        return String.format(DROP_AND_CREATE_USER,
                // drop user if exists
                username, username,
                // create user with default tablespace
                username,
                dataSource.getPassword(),
                username,
                DATA_PUMP_DIR,
                dumpFileDirectoryPath,
                DATA_PUMP_DIR,
                username
        );
    }
}
