package com.atlassian.maven.plugins.amps.i18n;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.maven.plugins.amps.util.FileUtils.readFileToString;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;

/**
 * Detects usages of i18n keys in soy files.
 *
 * @since 8.3
 */
class FindI18nUsageInSoy implements I18nScanner {
    private static final String I18N_KEY_GROUP = "i18nKey";

    private static final Pattern SOY_PATTERN = Pattern.compile(
            "\\{[^\\}]*?getText\\s*" + // opening of closure template command, with start of getText function
                    "\\(\\s*" + // start paren
                    "(['\"])(?<" + I18N_KEY_GROUP + ">[\\w.-]+)\\1" + // single or double quoted word
                    "\\s*[\\),]" // end paren, or start-of-args
    );

    @Override
    @Nonnull
    public List<String> findI18nUsages(@Nonnull final File file) {
        return readFileToString(file, UTF_8).map(input -> {
            final List<String> foundInstances = new ArrayList<>();
            final Matcher matcher = SOY_PATTERN.matcher(input);
            while (matcher.find()) {
                foundInstances.add(matcher.group(I18N_KEY_GROUP));
            }
            return foundInstances;
        }).orElse(emptyList());
    }

}
