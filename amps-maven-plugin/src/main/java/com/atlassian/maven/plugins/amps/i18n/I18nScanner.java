package com.atlassian.maven.plugins.amps.i18n;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.List;

/**
 * Scan files for i18n usages and return metadata about them.
 *
 * @since 8.3
 */
interface I18nScanner {
    /**
     * Given a file, return all discoverable usages of i18n keys in the file.
     * @param file The file to check for i18n keys in
     * @return the i18n keys that could be detected in the file.
     */
    @Nonnull
    List<String> findI18nUsages(@Nonnull File file);
}
