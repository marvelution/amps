package com.atlassian.maven.plugins.amps.i18n;

import com.atlassian.maven.plugins.amps.PostProcessor;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.util.DirectoryScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static com.atlassian.maven.plugins.amps.util.PropertyUtils.storeProperties;
import static com.atlassian.maven.plugins.amps.util.VersionUtils.getVersion;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;
import static org.codehaus.plexus.util.FileUtils.filename;
import static org.codehaus.plexus.util.FileUtils.getExtension;

/**
 * <p>
 *     Scans front-end files in the build output directory for usage of i18n functions and keys,
 *     then writes the found usages to a metadata file.
 * </p>
 * <p>
 *     By discovering usages at compile time, the Web Resource Manager can perform faster
 *     transformations at product runtime, concerning itself only with the substitution of known
 *     used translation values.
 * </p>
 * @since 8.3
 */
public class RecordI18nUsages implements PostProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordI18nUsages.class);

    /**
     * A registry of all the finder strategies utilised to scan for i18n usages.
     * The keys are filetype extensions, the values are the concrete strategies to use for that filetype
     */
    private static final Map<String, I18nScanner> SCANNERS = new HashMap<>();
    static {
        SCANNERS.put("js", new FindI18nUsageInJavascript());
        SCANNERS.put("soy", new FindI18nUsageInSoy());
    }

    /**
     * Find the source filename for a given filepath, accounting for minified variants of a resource file.
     * @param filepath the relative filepath of the file being scanned.
     * @return a filename without `-min` or `.min` suffixes
     */
    private static String getSourceFilename(final String filepath) {
        return filename(filepath)
                .replaceAll("\\-min\\.(\\w+)$", ".$1")
                .replaceAll("\\.min\\.(\\w+)$", ".$1");
    }

    /**
     * Given a filepath, returns the filename to write the i18n usage output to.
     * @param filepath the relative filepath of the file being scanned.
     */
    private static String getOutputFilename(final String filepath) {
        return getSourceFilename(filepath) + ".i18n.properties";
    }

    private static File getOutputFile(final File file) {
        final String outputDir = file.getParent();
        final String outputBasename = getOutputFilename(file.getName());
        return new File(outputDir, outputBasename);
    }

    @Override
    public void processProjectBuild(@Nonnull final Build build) {
        recordUsageForFilesInDirectory(build.getOutputDirectory());
    }

    @VisibleForTesting
    Collection<File> recordUsageForFilesInDirectory(@Nonnull final String outputDirectory) {
        return recordUsageForFilesInDirectory(Paths.get(outputDirectory).toFile());
    }

    @VisibleForTesting
    Collection<File> recordUsageForFilesInDirectory(@Nonnull final File outputDirectory) {
        if (!outputDirectory.isDirectory()) {
            LOGGER.warn("I18n output directory '{}' does not exist, skipping scan", outputDirectory);
            return emptyList();
        }

        DirectoryScanner scanner = new DirectoryScanner();
        scanner.setBasedir(outputDirectory);
        scanner.addDefaultExcludes();
        // Convert the extensions our finders can handle in to recursive globs
        scanner.setIncludes(SCANNERS.keySet().stream().map(ext -> "**/*." + ext).toArray(String[]::new));

        // Find the files we can process
        scanner.scan();

        // For every found file, pick the appropriate i18n finder, invoke them on the file,
        // then store all found usages. Return a list of files that house the results of the scans.
        return stream(scanner.getIncludedFiles())
                .map(s -> Paths.get(outputDirectory.getAbsolutePath(), s))
                .map(Path::toFile)
                .filter(File::isFile)
                .map(this::scanFile)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toSet());
    }

    private Optional<File> scanFile(@Nonnull final File file) {
        final File metadataFile = getOutputFile(file);

        // If another build step generated the file, don't override it.
        if (metadataFile.isFile()) {
            LOGGER.info("I18n metadata file already exists at '{}', skipping.", metadataFile.getAbsolutePath());
            return Optional.of(metadataFile);
        }

        // Run i18n usage scan for the file, tally usages
        final Collection<String> usedI18nKeys = scanFileByExtension(file);

        if (usedI18nKeys == null) {
            return Optional.empty();
        }

        // Convert the usage data in to a properties format
        final Properties props = new Properties();
        getI18nKeyUsageCount(usedI18nKeys).forEach((key, val) -> props.setProperty(key, String.valueOf(val)));

        // Write the i18n usage data
        try {
            storeProperties(props, metadataFile, "Generated by AMPS version: " + getVersion());
            LOGGER.info("I18n metadata written to '{}'", metadataFile.getAbsolutePath());
            return Optional.of(metadataFile);
        } catch (final MojoExecutionException e) {
            LOGGER.error(String.format("I18n metadata cannot be written for '%s', skipping", file.getAbsolutePath()), e);
            return Optional.empty();
        }
    }

    private Map<String, Long> getI18nKeyUsageCount(@Nonnull final Collection<String> keys) {
        return keys.stream()
                .sorted()
                .map(String::trim)
                .filter(StringUtils::isNotBlank)
                .collect(groupingBy(identity(), counting()));
    }

    /**
     *
     * @param file the file to scan and record i18n usages from
     * @return a list of used i18n keys in the file. if none are found, an empty list is returned. if the file was
     * not scanned, null is returned.
     */
    @Nullable
    private List<String> scanFileByExtension(@Nonnull final File file) {
        final String ext = getExtension(file.getName()).toLowerCase();
        final I18nScanner finder = SCANNERS.get(ext);
        if (finder == null) {
            LOGGER.debug("I18n scan for '{}' skipped as no scanner registered for '{}' extension", file.getAbsolutePath(), ext);
            return null;
        }
        LOGGER.info("I18n scan for '{}'", file.getAbsolutePath());
        final long start = System.currentTimeMillis();
        final List<String> results = finder.findI18nUsages(file);
        final long end = System.currentTimeMillis();
        LOGGER.info("I18n scan found {} key(s) used, took {}ms", results.size(), end - start);
        return results;
    }
}
