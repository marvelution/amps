package com.atlassian.maven.plugins.amps.license;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static java.lang.String.format;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.strip;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.util.EntityUtils.consume;

/**
 * A Java client for the License Backdoor Plugin.
 *
 * @since 8.2
 */
@ParametersAreNonnullByDefault
public class LicenseBackdoorClient {

    // As seen in java.net.InetSocketAddress.checkPort
    private static final int MAXIMUM_LEGAL_PORT_NUMBER = 0xFFFF;

    private static final String BACKDOOR_BASE_PATH = "/rest/license-backdoor/1.0/";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";

    private final int webPort;
    private final Log log;
    private final String contextPath;
    private final String hostname;
    private final String protocol;

    /**
     * Constructor.
     *
     * @param webPort     the web port used by the product; must be a real, static port number (not zero)
     * @param protocol    the web protocol used by the product
     * @param hostname    the hostname of the machine running the product
     * @param contextPath the context path of the product (leading or trailing slashes are ignored)
     * @param log         for logging messages
     */
    public LicenseBackdoorClient(
            final int webPort, final String protocol, final String hostname, final String contextPath, final Log log) {
        this.contextPath = requireNonNull(contextPath);
        this.hostname = requireNonNull(hostname);
        this.log = requireNonNull(log);
        this.protocol = requireNonNull(protocol);
        this.webPort = validateWebPort(webPort);
    }

    private static int validateWebPort(final int port) {
        if (port <= 0 || port > MAXIMUM_LEGAL_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid web port " + port);
        }
        return port;
    }

    /**
     * Sets the product's license to the given value.
     *
     * @param license the license to apply
     * @return {@code true} if the operation succeeded
     */
    public boolean installProductLicense(final String license) {
        try {
            final HttpResponse response = postLicenseAsAdmin(license);
            final StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HTTP_OK) {
                consume(response.getEntity());
                return true;
            }
            log.error(format("Could not set license: %d %s %s", statusLine.getStatusCode(),
                    statusLine.getReasonPhrase(), EntityUtils.toString(response.getEntity())));
            return false;
        } catch (final Exception e) {
            log.error("Could not set license", e);
            return false;
        }
    }

    private HttpResponse postLicenseAsAdmin(final String license) throws Exception {
        final HttpPost request = new HttpPost(addLicenseUri());
        request.setEntity(new StringEntity(license, APPLICATION_JSON));
        return callBackdoorAsAdmin(request);
    }

    private URI addLicenseUri() {
        return UriBuilder
                .fromPath(strip(contextPath, "/") + BACKDOOR_BASE_PATH + "licenses")
                .scheme(protocol)
                .host(hostname)
                .port(webPort)
                .build();
    }

    private static HttpResponse callBackdoorAsAdmin(final HttpUriRequest request) throws Exception {
        return HttpClientBuilder.create().build().execute(asAdmin(request));
    }

    private static <R extends HttpRequest> R asAdmin(final R request) throws AuthenticationException {
        final Credentials credentials = new UsernamePasswordCredentials(USERNAME, PASSWORD);
        request.addHeader(new BasicScheme().authenticate(credentials, request, null));
        return request;
    }
}
