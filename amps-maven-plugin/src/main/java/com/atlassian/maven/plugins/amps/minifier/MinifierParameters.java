package com.atlassian.maven.plugins.amps.minifier;

import com.google.common.collect.ImmutableMap;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Stores configuration values from the build that are relevant for any minification operations to be performed on
 * resources in the current build.
 */
public class MinifierParameters {
    private final boolean compressJs;
    private final boolean compressCss;
    private final Charset cs;
    private final Log log;
    private final Map<String, String> closureOptions;


    private final boolean closureJsdocWarningsEnabled;

    private boolean useClosureForJs = true;

    public MinifierParameters(boolean compressJs,
                              boolean compressCss,
                              Charset cs,
                              Log log,
                              @Nullable Map<String, String> closureOptions,
                              boolean closureJsdocWarningsEnabled) {
        this.compressJs = compressJs;
        this.compressCss = compressCss;
        this.cs = cs;
        this.log = log;
        ImmutableMap.Builder<String, String> opts = ImmutableMap.builder();
        opts.put("outputCharset", cs.name());
        if (closureOptions != null) {
            opts.putAll(closureOptions);
        }
        this.closureOptions = opts.build();
        this.closureJsdocWarningsEnabled = closureJsdocWarningsEnabled;
    }

    @Deprecated
    public MinifierParameters(boolean compressJs,
                              boolean compressCss,
                              boolean useClosureForJs,
                              boolean closureJsdocWarningsEnabled,
                              Charset cs, Log log,
                              Map<String, String> closureOptions) {
        this(compressJs, compressCss, cs, log, closureOptions, closureJsdocWarningsEnabled);
        this.useClosureForJs = useClosureForJs;
    }

    public boolean isCompressJs() {
        return compressJs;
    }

    public boolean isCompressCss() {
        return compressCss;
    }

    @Deprecated
    public boolean isUseClosureForJs() {
        return useClosureForJs;
    }

    public Charset getCs() {
        return cs;
    }

    public Log getLog() {
        return log;
    }

    @Nonnull
    public Map<String, String> getClosureOptions() {
        return closureOptions;
    }
    public boolean isClosureJsdocWarningsEnabled() {
        return closureJsdocWarningsEnabled;
    }


}
