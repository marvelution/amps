package com.atlassian.maven.plugins.amps.minifier.strategies.googleclosure;

import com.google.javascript.jscomp.CompilerOptions;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.Nonnull;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class GoogleClosureOptionsHandler {
    private CompilerOptions compilerOptions;
    private Log log;
    private final Map<String, Consumer<String>> configOptions = new HashMap<>();

    public GoogleClosureOptionsHandler(Log log) {
        this.compilerOptions = new CompilerOptions();
        this.log = log;

        // Set sensible default language levels.
        this.compilerOptions.setLanguageIn(CompilerOptions.LanguageMode.ECMASCRIPT_2018);

        // Disable 'use strict' by default, as in batch files,
        // a top-level scoped statement may cause older non-strict-compatible code to fail.
        this.compilerOptions.setEmitUseStrict(false);

        // Set what we've allowed users to configure.
        configOptions.put("emitUseStrict".toLowerCase(), useStrict -> compilerOptions.setEmitUseStrict(Boolean.parseBoolean(useStrict)));
        configOptions.put("languageIn".toLowerCase(), languageMode -> compilerOptions.setLanguageIn(CompilerOptions.LanguageMode.fromString(languageMode)));
        configOptions.put("languageOut".toLowerCase(), languageMode -> compilerOptions.setLanguageOut(CompilerOptions.LanguageMode.fromString(languageMode)));
        configOptions.put("outputCharset".toLowerCase(), charset -> compilerOptions.setOutputCharset(Charset.forName(charset)));
    }

    public void setOption(@Nonnull String optionName, String value) {
        final String key = optionName.toLowerCase();
        if (configOptions.containsKey(key)) {
            try {
                configOptions.get(key).accept(value);
            } catch (IllegalArgumentException e) {
                log.warn(optionName + " could not be configured.  Ignoring this option.", e);
            }
        } else {
            log.warn(optionName + " is not configurable. Ignoring this option.");
        }
    }

    public CompilerOptions getCompilerOptions() {
        return this.compilerOptions;
    }
}
