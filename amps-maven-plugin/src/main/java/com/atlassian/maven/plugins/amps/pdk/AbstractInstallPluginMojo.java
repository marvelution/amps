package com.atlassian.maven.plugins.amps.pdk;

import com.atlassian.maven.plugins.amps.AbstractProductAwareMojo;
import com.atlassian.maven.plugins.amps.PdkParams;
import com.atlassian.maven.plugins.amps.product.ProductHandler;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Base class for Mojos that install a plugin.
 */
public abstract class AbstractInstallPluginMojo extends AbstractProductAwareMojo {

    @Parameter(property = "atlassian.plugin.key")
    private String pluginKey;

    @Parameter(property = "project.groupId")
    private String groupId;

    @Parameter(property = "project.artifactId")
    private String artifactId;

    /**
     * HTTP port for the servlet containers
     */
    @Parameter(property = "http.port")
    private int httpPort;

    /**
     * Application context path
     */
    @Parameter(property = "context.path")
    private String contextPath;

    /**
     * Username of user that will install the plugin
     */
    @Parameter(property = "username", defaultValue = "admin")
    private String username;

    /**
     * Password of user that will install the plugin
     */
    @Parameter(property = "password", defaultValue = "admin")
    private String password;

    /**
     * Application server
     */
    @Parameter(property = "server", defaultValue = "localhost")
    private String server;

    protected final void installPlugin(final boolean isTestPlugin) throws MojoExecutionException {
        ensurePluginKeyExists();
        final ProductHandler productHandler = getProductHandler(getProductId());
        getMavenGoals().installPlugin(new PdkParams.Builder()
                .testPlugin(isTestPlugin)
                .pluginKey(pluginKey)
                .server(server)
                .port(getHttpPort(productHandler))
                .contextPath(getContextPath(productHandler))
                .username(username)
                .password(password)
                .build());
    }

    private void ensurePluginKeyExists() {
        if (pluginKey == null) {
            pluginKey = groupId + "." + artifactId;
        }
    }

    private int getHttpPort(final ProductHandler handler) {
        return httpPort == 0 ? handler.getDefaultHttpPort() : httpPort;
    }

    private String getContextPath(final ProductHandler handler) {
        return contextPath == null ? "/" + handler.getId() : contextPath;
    }
}
