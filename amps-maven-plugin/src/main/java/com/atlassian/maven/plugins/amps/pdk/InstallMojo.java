package com.atlassian.maven.plugins.amps.pdk;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Installs the current P2 plugin into a running product instance.
 */
@Mojo(name = "install")
public class InstallMojo extends AbstractInstallPluginMojo {

    public void execute() throws MojoExecutionException {
        installPlugin(false);
    }
}
