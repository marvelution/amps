package com.atlassian.maven.plugins.amps.pdk;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Installs the test plugin of the current P2 plugin project into a running product instance.
 */
@Mojo(name = "test-install")
public class TestInstallMojo extends AbstractInstallPluginMojo {

    public void execute() throws MojoExecutionException {
        if (shouldBuildTestPlugin()) {
            installPlugin(true);
        }
    }
}
