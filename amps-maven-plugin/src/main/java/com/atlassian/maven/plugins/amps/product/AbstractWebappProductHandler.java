package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.repository.RepositorySystem;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.maven.artifact.Artifact.LATEST_VERSION;
import static org.apache.maven.artifact.Artifact.RELEASE_VERSION;

/**
 * Superclass for {@link ProductHandler} implementations for webapp-based products, i.e. those deployed to Tomcat.
 */
public abstract class AbstractWebappProductHandler extends AbstractProductHandler {

    private static final String CARGO_CONTAINER_ID_PROPERTY = "amps.product.specific.cargo.container";
    private static final String SPECIFIC_CONTAINER_PROPERTY = "amps.product.specific.container";

    private final WebAppManager webAppManager;

    protected AbstractWebappProductHandler(
            final MavenContext context, final MavenGoals goals, final PluginProvider pluginProvider,
            final RepositorySystem repositorySystem, final ArtifactResolver artifactResolver,
            final WebAppManager webAppManager) {
        super(context, goals, pluginProvider, repositorySystem, artifactResolver);
        this.webAppManager = requireNonNull(webAppManager);
    }

    @Override
    public final void stop(@Nonnull final Product product) throws MojoExecutionException {
        webAppManager.stopWebapp(product, context);
    }

    @Override
    @Nonnull
    protected final File extractApplication(final Product product) throws MojoExecutionException {
        final ProductArtifact artifact = getArtifact(product);

        if (RELEASE_VERSION.equals(artifact.getVersion()) || LATEST_VERSION.equals(artifact.getVersion())) {
            setLatestStableVersion(product, artifact);
        }

        // Copy (not extract) the webapp war to target/<product>/<product>-original.war
        return goals.copyWebappWar(artifact, getBaseDirectory(product), product.getId());
    }

    @Override
    @Nonnull
    protected final List<Node> startProduct(
            final Product product, final File productFile, final List<Map<String, String>> systemProperties)
            throws MojoExecutionException {
        return webAppManager.startWebapp(productFile, systemProperties, getExtraContainerDependencies(product, productFile),
                getExtraProductDeployables(product), product, context);
    }

    @Override
    protected boolean supportsStaticPlugins() {
        return true;
    }

    @Override
    @Nonnull
    protected Optional<String> getLog4jPropertiesPath() {
        return Optional.of("WEB-INF/classes/log4j.properties");
    }

    @Override
    @Nonnull
    protected Optional<String> getLog4j2ConfigPath() {
        return Optional.of("WEB-INF/classes/log4j2.xml");
    }

    /**
     * Returns any extra artifacts to be put on the container's (Tomcat) classpath.
     * This implementation returns an empty list.
     *
     * @return see description
     */
    @Nonnull
    protected List<ProductArtifact> getExtraContainerDependencies(Product product, File productFile) {
        return emptyList();
    }

    /**
     * Returns any extra artifacts to be deployed to the container (typically WAR files).
     * This implementation returns an empty list.
     *
     * @param product the product being deployed
     * @return see description
     */
    @Nonnull
    protected List<ProductArtifact> getExtraProductDeployables(final Product product) {
        return emptyList();
    }

    @Nonnull
    @Override
    protected final Map<String, String> getSystemProperties(final Product product, final int nodeIndex) {
        final Map<String, String> systemProperties = new HashMap<>();
        systemProperties.putAll(generateJndiDataSourceSystemProperties(product));
        systemProperties.putAll(getProductSpecificSystemProperties(product, nodeIndex));
        return systemProperties;
    }

    /**
     * Subclasses should return any system properties they want to set, in addition to those for the {@link DataSource}.
     *
     * @param product the product being started
     * @param nodeIndex the zero-based index of the node being started
     * @return see description
     */
    @Nonnull
    protected abstract Map<String, String> getProductSpecificSystemProperties(Product product, int nodeIndex);

    private Map<String, String> generateJndiDataSourceSystemProperties(final Product product) {
        final List<DataSource> dataSources = getJndiDataSources(product);
        if (dataSources.size() == 1) {
            // TWData Cargo doesn't support other names than "cargo.datasource.datasource".
            // By AS: I'm pretty sure we don't use TWData Cargo any more, but this naming difference is benign.
            return singletonMap("cargo.datasource.datasource", dataSources.get(0).getCargoString());
        } else {
            // Zero or multiple JNDI data sources
            final ImmutableMap.Builder<String, String> systemProperties = ImmutableMap.builder();
            for (int i = 0; i < dataSources.size(); i++) {
                systemProperties.put("cargo.datasource.datasource." + i, dataSources.get(i).getCargoString());
            }
            return systemProperties.build();
        }
    }

    private List<DataSource> getJndiDataSources(final Product product) {
        final List<DataSource> dataSources = product.getDataSources();
        final DataSource defaultDataSource = getDefaultDataSource(product);

        if (defaultDataSource != null) {
            if (dataSources.isEmpty()) {
                dataSources.add(defaultDataSource);
            } else {
                dataSources.get(0).copyMissingValuesFrom(defaultDataSource);
            }
        }
        return dataSources.stream()
                .filter(ds -> isNotBlank(ds.getJndi()))
                .collect(toList());
    }

    /**
     * Opportunity for the product handler to define a default datasource for the product.
     * This implementation returns {@code null}.
     *
     * @return a bean containing the default values for the datasource.
     */
    @Nullable
    protected DataSource getDefaultDataSource(final Product product) {
        return null;
    }

    /**
     * Overrides version of webapp container artifact based on product pom
     *
     * @param product product context
     * @throws MojoExecutionException throw during creating effective pom
     */
    @Override
    protected void addOverridesFromProductPom(final Product product) throws MojoExecutionException {
        final ProductArtifact artifact = getArtifact(product);
        final File effectivePom = goals.generateEffectivePom(artifact, getBaseDirectory(product));

        final SAXReader reader = new SAXReader();
        try {
            final Document document = reader.read(effectivePom.getAbsoluteFile());
            final Element properties = document.getRootElement().element("properties");
            if (properties != null) {
                final Element customContainer = properties.element(SPECIFIC_CONTAINER_PROPERTY);
                final Element cargoId = properties.element(CARGO_CONTAINER_ID_PROPERTY);
                setPropertiesInProduct(product, customContainer, cargoId);
            }
        } catch (DocumentException e) {
            log.error("Error when reading effective pom", e);
        }
    }

    private void setPropertiesInProduct(final Product product, final Element customContainer, final Element cargoId) {
        if (customContainer != null && product.getCustomContainerArtifact() == null && product.isContainerNotSpecified()) {
            product.setCustomContainerArtifact(customContainer.getStringValue());
            if (cargoId != null) {
                product.setContainerId(cargoId.getStringValue());
            }
        }
    }
}
