package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.Application;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

/**
 * Provides mappings from products to the applications they support.
 */
class ApplicationMapper {
    // Map of productId -> applicationId -> application artifact
    private final Map<String, Map<String, GroupArtifactPair>> applicationKeys;

    ApplicationMapper(final Map<String, Map<String, GroupArtifactPair>> applicationKeys) {
        this.applicationKeys = unmodifiableMap(applicationKeys);
    }

    /**
     * Returns the artifacts that need to be installed for the given product's configured applications.
     *
     * @param product the product
     * @return see description
     */
    List<ProductArtifact> provideApplications(final Product product) {
        return Optional.ofNullable(applicationKeys.get(product.getId()))
                .map(applicationKeysForProduct -> {
                    final Predicate<Application> isApplicationSupportedByProduct = application -> {
                        final String applicationKey = application.getApplicationKey();
                        return applicationKeysForProduct.containsKey(applicationKey);
                    };
                    final Function<Application, ProductArtifact> toProductArtifact = application -> {
                        final String applicationKey = application.getApplicationKey();
                        final GroupArtifactPair groupArtifactPair = applicationKeysForProduct.get(applicationKey);
                        return groupArtifactPair.createProductArtifactWithVersion(application.getVersion());
                    };
                    return product.getApplications().stream()
                            .filter(isApplicationSupportedByProduct)
                            .map(toProductArtifact)
                            .collect(collectingAndThen(toList(), Collections::unmodifiableList));
                }).orElse(emptyList());
    }
}
