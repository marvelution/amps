package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.atlassian.maven.plugins.amps.util.ConfigFileUtils;
import com.atlassian.maven.plugins.amps.util.ConfigFileUtils.Replacement;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.repository.RepositorySystem;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.maven.plugins.amps.util.FileUtils.deleteDir;
import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.deleteDirectory;

public class BambooProductHandler extends AbstractWebappProductHandler {

    private static final String BUNDLED_PLUGINS_UNZIPPED = "WEB-INF/atlassian-bundled-plugins";
    private static final String BUNDLED_PLUGINS_ZIP = "WEB-INF/classes/atlassian-bundled-plugins.zip";

    public BambooProductHandler(final MavenContext context, final MavenGoals goals,
                                final RepositorySystem repositorySystem, final ArtifactResolver artifactResolver,
                                final WebAppManager webAppManager) {
        super(context, goals, new BambooPluginProvider(), repositorySystem, artifactResolver, webAppManager);
    }

    @Nonnull
    @Override
    public String getId() {
        return "bamboo";
    }

    @Nonnull
    public ProductArtifact getArtifact() {
        return new ProductArtifact("com.atlassian.bamboo", "atlassian-bamboo-web-app", "RELEASE");
    }

    @Nonnull
    public Optional<ProductArtifact> getTestResourcesArtifact() {
        return Optional.of(new ProductArtifact("com.atlassian.bamboo.plugins", "bamboo-plugin-test-resources"));
    }

    @Override
    public int getDefaultHttpPort() {
        return 6990;
    }

    @Override
    public int getDefaultHttpsPort() {
        return 8446;
    }

    @Override
    @Nonnull
    protected Map<String, String> getProductSpecificSystemProperties(final Product product, final int nodeIndex) {
        final ImmutableMap.Builder<String, String> properties = ImmutableMap.builder();
        properties.put("bamboo.home", getSingleNodeHomeDirectory(product).getPath());
        properties.put("org.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES", "false");
        properties.put("cargo.servlet.uriencoding", "UTF-8");
        properties.put("file.encoding", "UTF-8");
        return properties.build();
    }

    @Nonnull
    @Override
    public Optional<File> getUserInstalledPluginsDirectory(
            final Product product, final File webappDir, final File homeDir) {
        File baseDir = homeDir;
        File sharedHomeDir = new File(homeDir, "shared");
        if (sharedHomeDir.exists()) {
            baseDir = sharedHomeDir;
        }
        return Optional.of(new File(baseDir, "plugins"));
    }

    @Nonnull
    @Override
    public File getBundledPluginPath(final Product product, final File productDir) {
        // the zip became a directory in 5.9, so if the directory exists, use it, otherwise fall back to the old behaviour.
        final File bundleDir = new File(productDir, BUNDLED_PLUGINS_UNZIPPED);

        if (bundleDir.isDirectory()) {
            return bundleDir;
        } else {
            return new File(productDir, BUNDLED_PLUGINS_ZIP);
        }
    }

    @Override
    public void processHomeDirectory(final Product product, final int nodeIndex, final File homeDir)
            throws MojoExecutionException {
        super.processHomeDirectory(product, nodeIndex, homeDir);

        final String contextPath = product.getContextPath().replaceAll("^/|/$", "");
        final String baseUrl = format("%s://%s:%d/%s",
                product.getProtocol(), product.getServer(), product.getSingleNodeWebPort(), contextPath);
        //for bamboo dc configuration file is in shared directory
        File adminXml = new File(homeDir, "/shared/configuration/administration.xml");
        if (!adminXml.exists()) {
            adminXml = new File(homeDir, "/xml-data/configuration/administration.xml");
        }
        // The regex in the following search text is used to match IPv4 ([^:]+) or IPv6 (\[.+]) addresses.
        ConfigFileUtils.replaceAll(adminXml, "https?://(?:[^:]+|\\[.+]):8085", baseUrl);
    }

    @Override
    @Nonnull
    protected List<Replacement> getReplacements(@Nonnull final Product product, final int nodeIndex) {
        final List<Replacement> replacements = super.getReplacements(product, nodeIndex);
        final File homeDirectory = getSingleNodeHomeDirectory(product);
        // We don't re-wrap homes with these values:
        replacements.add(new Replacement("@project-dir@", homeDirectory.getParent(), false));
        replacements.add(new Replacement("/bamboo-home/", "/home/", false));
        replacements.add(new Replacement("${bambooHome}", homeDirectory.getAbsolutePath(), false));
        return replacements;
    }

    private File getSingleNodeHomeDirectory(@Nonnull final Product product) {
        final List<File> homeDirectories = getHomeDirectories(product);
        if (homeDirectories.size() != 1) {
            // Bamboo doesn't support multi-node operation
            throw new IllegalStateException("Expected one home directory, but found " + homeDirectories);
        }
        return homeDirectories.get(0);
    }

    @Nonnull
    @Override
    protected List<File> getConfigFiles(@Nonnull final Product product, @Nonnull final File homeDirectory) {
        final List<File> configFiles = super.getConfigFiles(product, homeDirectory);
        configFiles.add(new File(homeDirectory, "bamboo.cfg.xml"));
        configFiles.add(new File(homeDirectory, "database.log"));
        return configFiles;
    }

    @Override
    protected void cleanupProductHomeForZip(@Nonnull Product bamboo, @Nonnull File genDir) throws MojoExecutionException, IOException {
        File baseDir = genDir;
        File sharedDir = new File(baseDir, "shared");
        if (sharedDir.exists()) {
            // Used only for Bamboo DC (Bamboo DC contains plugins in <home>/shared/plugins directory)
            baseDir = sharedDir;
            deleteDirectory(new File(baseDir, "plugins"));
            deleteDirectory(new File(baseDir, "bundled-plugins"));
        }
        super.cleanupProductHomeForZip(bamboo, genDir);
        deleteDir(new File(baseDir, "jms-store"));
        deleteDir(new File(genDir, "caches"));
        deleteDir(new File(genDir, "logs"));
    }

    private static class BambooPluginProvider extends AbstractPluginProvider {

        @Override
        protected Collection<ProductArtifact> getSalArtifacts(String salVersion) {
            return Arrays.asList(
                    new ProductArtifact("com.atlassian.sal", "sal-api", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-bamboo-plugin", salVersion));
        }

    }
}
