package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.ProductArtifact;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.google.common.collect.Sets.newTreeSet;
import static com.google.common.io.CharStreams.readLines;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.split;

/**
 * Responsible for reading the bundled libraries manifest file from the COnfluence WAR.
 */
final class ConfluenceBundledLibrariesManifestReader {

    static final String BUNDLED_LIBS_METADATA_FILE_PATH = "META-INF/maven/bundled-libs.txt";

    public Optional<Set<ProductArtifact>> readBundledLibrariesManifest(File warFile) throws IOException {
        try (ZipFile zipFile = new ZipFile(warFile)) {
            final ZipEntry entry = zipFile.getEntry(BUNDLED_LIBS_METADATA_FILE_PATH);
            if (entry == null) {
                return empty();
            } else {
                try (Reader reader = new InputStreamReader(zipFile.getInputStream(entry), UTF_8)) {
                    final SortedSet<String> gavs = newTreeSet(readLines(reader));
                    final Set<ProductArtifact> artifacts = gavs.stream().map(gav -> {
                        String[] parts = split(gav, ':');
                        return new ProductArtifact(parts[0], parts[1], parts[2]);
                    }).collect(toSet());
                    return Optional.of(artifacts);
                }
            }
        }
    }
}
