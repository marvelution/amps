package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.product.common.XMLDocumentHandler;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentProcessor;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentTransformer;
import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Element;
import org.dom4j.Node;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.File;

import static com.atlassian.maven.plugins.amps.util.NetworkUtils.getLoopbackInterface;

/**
 * Makes the necessary changes to the Confluence configuration file to enable clustering. See
 * https://confluence.atlassian.com/doc/change-node-discovery-from-multicast-to-tcp-ip-or-aws-792297728.html
 * for what this involves.
 *
 * @since 8.3
 */
@ParametersAreNonnullByDefault
public class ConfluenceClusterConfigurer {

    private static String propertyCalled(final String name) {
        return String.format("//property[@name='%s']", name);
    }

    /**
     * Configures the given {@code Confluence.cfg.xml} file for clustering.
     *
     * @param configFile the file to modify; must be a file that exists
     * @param sharedHome the shared home of the Confluence instance
     * @param loopbackInterface the name of the IP loopback network interface
     * @throws MojoExecutionException if execution fails
     */
    public void configure(final File configFile, final File sharedHome, final String loopbackInterface)
            throws MojoExecutionException {
        new XMLDocumentProcessor(new XMLDocumentHandler(configFile))
                .load()
                .transform(setSetupTypeToCluster())
                .transform(enableClustering())
                .transform(setSharedHome(sharedHome))
                .transform(setClusterInterface(loopbackInterface))
                .transform(setClusterJoinType())
                .transform(setClusterPeers())
                .transform(removeClusterAddress())
                .saveIfModified();
    }

    private XMLDocumentTransformer setSetupTypeToCluster() {
        return document -> {
            final Element rootElement = document.getRootElement();
            final Node setupTypeNode = rootElement.selectSingleNode("//setupType");
            if (setupTypeNode == null) {
                // Add it
                final Element newElement = rootElement.addElement("setupType");
                newElement.setText("cluster");
                return true;
            } else if ("cluster".equals(setupTypeNode.getText())) {
                // Nothing to do
                return false;
            } else {
                // Update the existing element
                setupTypeNode.setText("cluster");
                return true;
            }
        };
    }

    private XMLDocumentTransformer enableClustering() {
        return document -> setProperty(document, "confluence.cluster", "true");
    }

    private XMLDocumentTransformer setSharedHome(final File sharedHome) {
        return document -> setProperty(document, "confluence.cluster.home", sharedHome.getAbsolutePath());
    }

    private XMLDocumentTransformer setClusterInterface(final String loopbackInterface) {
        return document -> setProperty(document, "confluence.cluster.interface", loopbackInterface);
    }

    private XMLDocumentTransformer setClusterJoinType() {
        return document -> setProperty(document, "confluence.cluster.join.type", "tcp_ip");
    }

    private XMLDocumentTransformer setClusterPeers() {
        // Only need one IP address for a local cluster
        return document -> setProperty(document,"confluence.cluster.peers", "127.0.0.1");
    }

    private boolean setProperty(final Node document, final String name, final String value) {
        final Node propertiesNode = document.selectSingleNode("//properties");
        if (propertiesNode == null) {
            throw new IllegalStateException("Could not find properties node");
        }
        final Node nodeToUpdate = propertiesNode.selectSingleNode(propertyCalled(name));
        if (nodeToUpdate == null) {
            // Add the property
            final Element newElement = ((Element) propertiesNode).addElement("property");
            newElement.addAttribute("name", name);
            newElement.setText(value);
            return true;
        } else if (value.equals(nodeToUpdate.getText())) {
            // Nothing to do
            return false;
        } else {
            // Update the existing property
            nodeToUpdate.setText(value);
            return true;
        }
    }

    private XMLDocumentTransformer removeClusterAddress() {
        return document -> {
            final Node clusterAddressNode =
                    document.selectSingleNode(propertyCalled("confluence.cluster.address"));
            if (clusterAddressNode == null) {
                return false;
            }
            clusterAddressNode.detach();
            return true;
        };
    }
}
