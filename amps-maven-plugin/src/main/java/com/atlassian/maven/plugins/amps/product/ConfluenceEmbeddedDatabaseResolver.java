package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toList;

/**
 * This class is responsible for determining if extra embedded database container dependencies are necessary in order to run
 * Confluence. From Confluence 8.0, H2 and HSQL are no longer shipped in the WAR, and either need to be specified in
 * libArtifacts in the AMPS config, or need to be automatically added as extra container dependencies by this class.
 * <p>
 * The class looks for the file META-INF/maven/bundled-libs.txt inside the Confluence WAR, present in Confluence 7.20 onwards,
 * which contains a manifest of every bundled library. If the file is absent (i.e an older Confluence version), then we
 * assume both H2 and HSQL are bundled and no extra dependencies are added. Similarly, if the manifest is present, and
 * shows H2 and HSQL are bundled, then no extra dependencies are added. If the manifest is present but lacks either H2 or HSQL,
 * then this class will add extra container dependencies for the missing ones.
 * <p>
 * Additionally, if {@link Product#getLibArtifacts()} contains H2 and/orHSQL dependencies, then no extra dependencies will be added.
 * This allows plugin devs to override the H2/HSQL versions that would otherwise be automatically used here.
 */
final class ConfluenceEmbeddedDatabaseResolver {
    static final ProductArtifact H2 = new ProductArtifact("com.h2database", "h2", "1.4.200");
    static final ProductArtifact HSQLDB = new ProductArtifact("org.hsqldb", "hsqldb", "2.3.0");

    private final Log log;
    private final WarFileBundledLibrariesManifestReader warFileBundledLibrariesMetadataReader;

    public ConfluenceEmbeddedDatabaseResolver(Log log) {
        this(log, new ConfluenceBundledLibrariesManifestReader()::readBundledLibrariesManifest);
    }

    public ConfluenceEmbeddedDatabaseResolver(Log log, WarFileBundledLibrariesManifestReader warFileBundledLibrariesMetadataReader) {
        this.log = requireNonNull(log);
        this.warFileBundledLibrariesMetadataReader = requireNonNull(warFileBundledLibrariesMetadataReader);
    }

    public List<ProductArtifact> getExtraEmbeddedDatabaseDependencies(Product product, File warFile) {
        Optional<Set<ProductArtifact>> bundledLibraries = readBundledLibrariesMetadata(warFile);
        bundledLibraries.ifPresent(libs -> info("Confluence WAR file contains bundled library metadata"));

        return Stream.of(H2, HSQLDB)
                .map(dbArtifact -> getExtraEmbeddedDatabaseDependency(product, dbArtifact, bundledLibraries.orElse(null)))
                .flatMap(this::stream)
                .collect(toList());
    }

    /**
     * Determine if an additional container dependency needs to be added for the given embedded database.
     */
    private Optional<ProductArtifact> getExtraEmbeddedDatabaseDependency(Product product,
                                                                         ProductArtifact dbArtifact,
                                                                         @Nullable Set<ProductArtifact> bundledLibraries) {

        if (bundledLibraries == null) {
            log.debug("Confluence WAR contains no bundled library metadata, assuming embedded database dependencies are bundled");
            return empty();

        } else if (containsGroupAndArtifactMatch(bundledLibraries, dbArtifact)) {
            info("Confluence WAR contains bundled embedded database %s:%s", dbArtifact.getGroupId(), dbArtifact.getArtifactId());
            return empty();

        } else if (containsGroupAndArtifactMatch(product.getLibArtifacts(), dbArtifact)) {
            // if the AMPS libArtifacts config contains an explicit entry for this dependency (e.g. to use a different version), then use that
            info("AMPS Configuration contains embedded database dependency %s:%s", dbArtifact.getGroupId(), dbArtifact.getArtifactId());
            return empty();

        } else {
            // else add an extra container dependency for it
            info("Neither Confluence WAR nor AMPS configuration contains %s:%s, adding extra container dependency %s", dbArtifact.getGroupId(), dbArtifact.getArtifactId(), dbArtifact);
            return Optional.of(dbArtifact);
        }
    }

    private Optional<Set<ProductArtifact>> readBundledLibrariesMetadata(File warFile) {
        try {
            return warFileBundledLibrariesMetadataReader.readBundledLibrariesManifest(warFile);
        } catch (IOException e) {
            log.warn("Failed to read WAR file to locate bundled library metadata", e);
            return empty();
        }
    }

    private static boolean containsGroupAndArtifactMatch(Collection<ProductArtifact> artifacts, ProductArtifact artifact) {
        return artifacts.stream()
                .anyMatch(candidate ->
                        Objects.equals(candidate.getGroupId(), artifact.getGroupId()) &&
                                Objects.equals(candidate.getArtifactId(), artifact.getArtifactId()));
    }

    private void info(String format, Object... args) {
        log.info(String.format(format, args));
    }

    private <T> Stream<T> stream(Optional<T> optional) {
        // Replace with Optional#stream when we're building against Java 9+
        return optional.map(Stream::of).orElseGet(Stream::empty);
    }

    @FunctionalInterface
    interface WarFileBundledLibrariesManifestReader {
        Optional<Set<ProductArtifact>> readBundledLibrariesManifest(File warFile) throws IOException;
    }
}
