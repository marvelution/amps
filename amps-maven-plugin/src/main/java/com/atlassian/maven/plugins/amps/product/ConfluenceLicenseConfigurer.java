package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.product.common.XMLDocumentHandler;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentProcessor;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentTransformer;
import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Element;
import org.dom4j.Node;

import java.io.File;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.remove;

/**
 * Modifies the Confluence configuration file to contain a specific license.
 *
 * @since 8.3
 */
public class ConfluenceLicenseConfigurer {

    private static final String LICENSE_PROPERTY = "atlassian.license.message";

    /**
     * Modifies the given configuration file to contain the given license.
     *
     * @param configFile the file to modify
     * @param license the license to use; cannot be blank
     * @throws MojoExecutionException if execution fails
     */
    public void configure(final File configFile, final String license) throws MojoExecutionException {
        if (isBlank(license)) {
            throw new IllegalArgumentException(format("Invalid license '%s'", license));
        }
        new XMLDocumentProcessor(new XMLDocumentHandler(configFile))
                .load()
                .transform(setLicense(license))
                .saveIfModified();
    }

    private XMLDocumentTransformer setLicense(final String license) {
        return document -> {
            final Element rootElement = document.getRootElement();
            final Node licenseNode = rootElement.selectSingleNode(licenseProperty());
            if (licenseNode == null) {
                // Unexpected
                throw new UnsupportedOperationException("Could not find property called " + LICENSE_PROPERTY);
            } else if (license.equals(licenseNode.getText())) {
                // Nothing to do
                return false;
            } else {
                // Update the existing element
                licenseNode.setText(remove(license, ' '));
                return true;
            }
        };
    }

    private String licenseProperty() {
        return format("//property[@name='%s']", LICENSE_PROPERTY);
    }
}
