package com.atlassian.maven.plugins.amps.product;

import javax.annotation.Nullable;

import static java.util.Arrays.stream;

public enum ImportMethod {

    /**
     * The import file contains SQL statements to be executed.
     */
    SQL("sql"),

    /**
     * The import file can be imported by Postgres' {@code psql} import tool.
     */
    PSQL("psql"),

    /**
     * The import file can be imported by Oracle's Import Data Pump tool.
     */
    IMPDP("impdp"),

    /**
     * The import file can be imported by the {@code sqlcmd} tool for Microsoft SQL Server.
     */
    SQLCMD("sqlcmd");

    /**
     * Returns the import method with the given name, ignoring case.
     *
     * @param importMethod the name to look up
     * @return null if there's no such import method
     */
    @Nullable
    public static ImportMethod getValueOf(final String importMethod) {
        return stream(values())
                .filter(value -> value.getMethod().equalsIgnoreCase(importMethod))
                .findFirst()
                .orElse(null);
    }

    private final String method;

    ImportMethod(final String method) {
        this.method = method;
    }

    public String getMethod() {
        return this.method;
    }

}
