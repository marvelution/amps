package com.atlassian.maven.plugins.amps.product;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Represents a package in the Java Module System.
 *
 * @since 8.3.0
 */
public final class JavaModulePackage {

    private static final String ALL_UNNAMED_MODULES = "ALL-UNNAMED";

    private final String moduleName;
    private final String packageName;

    public JavaModulePackage(String moduleName, String packageName) {
        this.moduleName = requireNonNull(moduleName);
        this.packageName = requireNonNull(packageName);
    }

    public String createAddOpensToUnnamed() {
        return createAddOpens(ALL_UNNAMED_MODULES);
    }

    public String createAddExportsToUnnamed() {
        return createAddExports(ALL_UNNAMED_MODULES);
    }

    private String createAddOpens(String targetModule) {
        return String.format("--add-opens %s/%s=%s", moduleName, packageName, targetModule);
    }

    private String createAddExports(String targetModule) {
        return String.format("--add-exports %s/%s=%s", moduleName, packageName, targetModule);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JavaModulePackage that = (JavaModulePackage) o;
        return moduleName.equals(that.moduleName) && packageName.equals(that.packageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moduleName, packageName);
    }

    /**
     * Convenience factory method for creating a {@link JavaModulePackage} for the java.base module.
     */
    static JavaModulePackage fromJavaBaseModule(String packageName) {
        return new JavaModulePackage("java.base", packageName);
    }

    /**
     * Convenience factory method for creating a {@link JavaModulePackage} for the java.rmi module.
     */
    static JavaModulePackage fromJavaRmiModule(String packageName) {
        return new JavaModulePackage("java.rmi", packageName);
    }
}
