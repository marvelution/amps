package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import org.apache.maven.plugin.MojoExecutionException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Provides the product-specific logic required by AMPS.
 */
@ParametersAreNonnullByDefault
public interface ProductHandler {

    /**
     * Returns a string that identifies this product, for example for use in the context path.
     * No further restriction is defined on this ID.
     *
     * @return a non-blank ID
     */
    @Nonnull
    String getId();

    /**
     * Extracts the product and its home, prepares them, and starts the product.
     *
     * @param product the product to start
     * @return the node(s) on which the product is running; a non-empty list
     */
    @Nonnull
    List<Node> start(Product product) throws MojoExecutionException;

    /**
     * Stops the given product.
     *
     * @param product the product to stop
     * @throws MojoExecutionException if execution fails
     */
    void stop(Product product) throws MojoExecutionException;

    /**
     * Returns the default HTTP port for this product.
     *
     * @return a non-zero port number
     */
    int getDefaultHttpPort();

    /**
     * Returns the default context path for this product.
     *
     * @return a context path starting with "{@code /}"
     */
    @Nonnull
    String getDefaultContextPath();

    /**
     * Returns the default Cargo container ID for this product.
     *
     * @return a non-blank value
     */
    @Nonnull
    String getDefaultContainerId();

    /**
     * Returns the default Cargo container ID for the given product configuration.
     *
     * @param product the product
     * @return a non-blank value
     * @throws MojoExecutionException if execution fails
     */
    @Nonnull
    String getDefaultContainerId(Product product) throws MojoExecutionException;

    /**
     * Return the directory(s) to snapshot when we want to restore the state of the instance.
     * <p>
     * Most often, equivalent to the home directory(s).
     *
     * @param product the product for which to get the snapshot directory
     * @return see description
     */
    @Nonnull
    List<File> getSnapshotDirectories(Product product);

    /**
     * Returns the home directories to use for the given product instance.
     *
     * @param product the product
     * @return a list of directories that may or may not exist
     */
    @Nonnull
    List<File> getHomeDirectories(Product product);

    /**
     * Returns the base directory for the given product instance.
     *
     * @param product the product for which to return the base directory
     * @return see description
     */
    @Nonnull
    File getBaseDirectory(Product product);

    /**
     * Snapshots the home directory. The goal is that the state is totally restored if we restart the application
     * with this minimal snapshot.
     *
     * @param homeDirectory The path to the previous run's home directory.
     * @param targetZip     The path to the final zip file.
     * @param product       The product
     * @since 3.1
     */
    void createHomeZip(File homeDirectory, File targetZip, Product product) throws MojoExecutionException;

    /**
     * Returns the default HTTPS port for this product.
     *
     * @since 5.0.4
     */
    int getDefaultHttpsPort();

    /**
     * Returns the product's Maven artifact (a WAR, a JAR, a binary, etc.).
     *
     * @return see description
     * @since 8.3 was in AbstractProductHandler
     */
    @Nonnull
    ProductArtifact getArtifact();

    /**
     * Returns the artifact containing the product's test resources.
     *
     * @return empty if none exists
     * @since 8.3 was in AbstractProductHandler
     */
    @Nonnull
    Optional<ProductArtifact> getTestResourcesArtifact();
}
