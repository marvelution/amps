package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.atlassian.maven.plugins.amps.util.MavenProjectLoader;
import com.google.common.collect.ImmutableList;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.repository.RepositorySystem;

import java.util.Collection;
import java.util.List;

/**
 * Factory for {@link ProductHandler} instances.
 */
public final class ProductHandlerFactory {

    public static final String REFAPP = "refapp";
    public static final String CONFLUENCE = "confluence";
    public static final String JIRA = "jira";
    public static final String BAMBOO = "bamboo";
    /**
     * @since 6.1.0
     */
    public static final String BITBUCKET = "bitbucket";
    public static final String FECRU = "fecru";
    public static final String CROWD = "crowd";
    public static final String CTK_SERVER = "ctk-server";

    private static final List<String> PRODUCT_IDS = ImmutableList.of(
            REFAPP, CONFLUENCE, JIRA, BAMBOO, BITBUCKET, FECRU, CROWD, CTK_SERVER);

    public static ProductHandler create(final String id, final MavenContext context, final MavenGoals goals,
                                        final RepositorySystem repositorySystem, final ProjectBuilder projectBuilder,
                                        final ArtifactResolver artifactResolver, final WebAppManager webAppManager) {
        if (REFAPP.equals(id)) {
            return new RefappProductHandler(context, goals, repositorySystem, artifactResolver, webAppManager);
        } else if (CONFLUENCE.equals(id)) {
            return new ConfluenceProductHandler(context, goals, repositorySystem, artifactResolver, webAppManager);
        } else if (JIRA.equals(id)) {
            return new JiraProductHandler(context, goals, repositorySystem, artifactResolver, webAppManager);
        } else if (BAMBOO.equals(id)) {
            return new BambooProductHandler(context, goals, repositorySystem, artifactResolver, webAppManager);
        } else if (BITBUCKET.equals(id)) {
            return new BitbucketProductHandler(context, goals, repositorySystem, new MavenProjectLoader(),
                    projectBuilder, artifactResolver, webAppManager);
        } else if (FECRU.equals(id)) {
            return new FeCruProductHandler(context, goals, repositorySystem, artifactResolver);
        } else if (CROWD.equals(id)) {
            return new CrowdProductHandler(context, goals, repositorySystem, artifactResolver, webAppManager);
        } else if (CTK_SERVER.equals(id)) {
            return new CtkServerProductHandler(context, goals);
        }

        throw new IllegalArgumentException("Unknown product ID: '" + id + "' Valid values: " + getIds());
    }

    /**
     * Returns the supported product IDs.
     *
     * @return IDs of the form "bamboo", "jira", etc.
     */
    public static Collection<String> getIds() {
        return PRODUCT_IDS;
    }

    private ProductHandlerFactory() {
        throw new UnsupportedOperationException("Not for instantiation");
    }
}
