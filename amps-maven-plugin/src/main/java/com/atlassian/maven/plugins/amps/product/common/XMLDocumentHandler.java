package com.atlassian.maven.plugins.amps.product.common;

import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.atlassian.plugins.codegen.util.Dom4jUtil.newSaxReader;
import static java.util.Objects.requireNonNull;

/**
 * Reads and saves an XML document on change.
 *
 * @since 8.2
 */
public class XMLDocumentHandler {

    private final File file;

    /**
     * Constructor.
     *
     * @param file the XML file to be transformed
     */
    public XMLDocumentHandler(final File file) {
        this.file = requireNonNull(file);
    }

    protected Document read() throws MojoExecutionException {
        final SAXReader reader = newSaxReader();
        try {
            return reader.read(file);
        } catch (final DocumentException ex) {
            throw new MojoExecutionException("Cannot parse XML file: " + file.getName(), ex);
        }
    }

    protected void write(final Document document) throws MojoExecutionException {
        try (final FileOutputStream fos = new FileOutputStream(file)) {
            final XMLWriter writer = new XMLWriter(fos, OutputFormat.createPrettyPrint());
            writer.write(document);
            writer.close();
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to write updated XML file: " + file.getName(), e);
        }
    }
}
