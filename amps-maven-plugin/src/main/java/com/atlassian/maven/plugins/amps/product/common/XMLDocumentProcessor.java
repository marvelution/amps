package com.atlassian.maven.plugins.amps.product.common;

import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Document;

import static java.util.Objects.requireNonNull;

/**
 * XMLDocumentProcessor should be used in correct order:
 * <ol>
 *  <li>load</li>
 *  <li>validate*</li>
 *  <li>transform*</li>
 *  <li>saveIfModified</li>
 * </ol>
 * (*) marked methods may be used zero or more times.
 * <p>
 * Different order may result in undefined behaviour.
 *
 * @since 8.2
 */
public class XMLDocumentProcessor {

    private final XMLDocumentHandler xmlDocumentHandler;

    private boolean modified;
    private Document document;

    public XMLDocumentProcessor(final XMLDocumentHandler xmlDocumentHandler) {
        this.xmlDocumentHandler = requireNonNull(xmlDocumentHandler);
    }

    /**
     * Loads the document into memory ready for transformation.
     */
    public XMLDocumentProcessor load() throws MojoExecutionException {
        document = xmlDocumentHandler.read();
        return this;
    }

    /**
     * Validates the XML document using the given validator.
     *
     * @param validator the validator to use
     * @return this processor
     * @throws ValidationException if validation fails
     */
    public XMLDocumentProcessor validate(final XMLDocumentValidator validator) throws ValidationException {
        validator.validate(document);
        return this;
    }

    /**
     * Transforms the XML document using the given transformer.
     *
     * @param transformer the transformer to apply
     * @return this processor
     * @throws MojoExecutionException if transformation fails
     */
    public XMLDocumentProcessor transform(final XMLDocumentTransformer transformer) throws MojoExecutionException {
        modified |= transformer.transform(document);
        return this;
    }

    /**
     * Saves the XML document if any changes have been made by {@link #transform(XMLDocumentTransformer)}.
     *
     * @throws MojoExecutionException if saving fails
     */
    public void saveIfModified() throws MojoExecutionException {
        if (this.modified) {
            this.xmlDocumentHandler.write(this.document);
        }
    }
}
