package com.atlassian.maven.plugins.amps.product.jira.config;

import com.atlassian.maven.plugins.amps.database.DatabaseType;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentTransformer;
import org.dom4j.Document;
import org.dom4j.Node;

import javax.annotation.ParametersAreNonnullByDefault;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * This transformer will update or create database-type node in dbconfig.xml to represent configured database type.
 */
@ParametersAreNonnullByDefault
public class DatabaseTypeUpdaterTransformer implements XMLDocumentTransformer {

    private final DatabaseType dbType;

    public DatabaseTypeUpdaterTransformer(final DatabaseType dbType) {
        this.dbType = dbType;
    }

    @Override
    public boolean transform(final Document document) {
        final Node dbTypeNode = document.selectSingleNode("/jira-database-config/database-type");

        // update database type
        if (dbTypeNode != null && isNotEmpty(dbTypeNode.getStringValue())) {
            String currentDbType = dbTypeNode.getStringValue();
            if (!currentDbType.equals(dbType.getOfBizName())) {
                dbTypeNode.setText(dbType.getOfBizName());
                return true;
            }
        }

        return false;
    }
}
