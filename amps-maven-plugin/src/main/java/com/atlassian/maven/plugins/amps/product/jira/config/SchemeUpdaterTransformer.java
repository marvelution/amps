package com.atlassian.maven.plugins.amps.product.jira.config;

import com.atlassian.maven.plugins.amps.database.DatabaseType;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentTransformer;
import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * This transformer will update or create schema-name node in dbconfig.xml
 * to represent configured schema on databases supporting it.
 * Otherwise, it will remove schema node from configuration,
 * if database is not supporting it.
 */
public class SchemeUpdaterTransformer implements XMLDocumentTransformer {

    private final DatabaseType databaseType;
    private final String schema;

    public SchemeUpdaterTransformer(final DatabaseType databaseType, final String schema) {
        this.databaseType = requireNonNull(databaseType);
        this.schema = schema;
    }

    @Override
    public boolean transform(final Document document) throws MojoExecutionException {
        final Node schemaNode = document.selectSingleNode("/jira-database-config/schema-name");

        // postgres, mssql, hsql
        if (databaseType.hasSchema()) {
            if (isEmpty(schema)) {
                throw new MojoExecutionException("Database configuration is missing the schema");
            }
            if (null == schemaNode) {
                // add schema-name node
                ((Element) document.selectSingleNode("/jira-database-config"))
                        .addElement("schema-name").addText(schema);
                return true;
            } else {
                if (isNotEmpty(schemaNode.getText()) && !schema.equals(schemaNode.getText())) {
                    schemaNode.setText(schema);
                    return true;
                }
            }
        }
        // mysql, oracle
        else {
            if (schemaNode != null) {
                // remove schema node
                schemaNode.detach();
                return true;
            }
        }
        return false;
    }
}
