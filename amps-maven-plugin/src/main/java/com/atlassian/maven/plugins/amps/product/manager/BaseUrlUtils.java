package com.atlassian.maven.plugins.amps.product.manager;

import javax.annotation.Nullable;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.prependIfMissing;

/**
 * Utility methods relating to product base URLs.
 *
 * @since 8.3
 */
public final class BaseUrlUtils {

    /**
     * Returns the product's base URL, based on the given inputs.
     *
     * @param server the TCP/IP host on which the product is running; should not be blank
     * @param actualWebPort the product's actual web port
     * @param contextPath the product's servlet context path
     * @return the base URL
     */
    public static String getBaseUrl(final String server, final int actualWebPort, @Nullable final String contextPath) {
        return formatServer(server) + formatPort(actualWebPort) + formatContextPath(contextPath);
    }

    private static String formatServer(final String server) {
        if (isBlank(server)) {
            throw new IllegalArgumentException(format("Invalid server name '%s'", server));
        }
        return prependIfMissing(server, "http://"); // why hardcode to HTTP? Does this break HTTPS?
    }

    private static String formatPort(final int port) {
        return port != 80 ? ":" + port : "";
    }

    private static String formatContextPath(final String contextPath) {
        if (isBlank(contextPath)) {
            return "";
        }
        return prependIfMissing(contextPath, "/");
    }

    private BaseUrlUtils() {}
}
