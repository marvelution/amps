package com.atlassian.maven.plugins.amps.product.manager;

import com.atlassian.maven.plugins.amps.ProductArtifact;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * A Cargo container, typically a Tomcat distribution.
 *
 * @since 8.3 was previously an inner class of {@code WebAppManagerImpl}
 */
class Container extends ProductArtifact {

    private final String id;
    private final String type;
    private final String classifier;

    /**
     * Installable container that can be downloaded by Maven.
     *
     * @param id         identifier of container, eg. "tomcat5x".
     * @param groupId    groupId of container.
     * @param artifactId artifactId of container.
     * @param version    version number of container.
     */
    Container(final String id, final String groupId, final String artifactId, final String version) {
        super(groupId, artifactId, version);
        this.id = id;
        this.type = "installed";
        this.classifier = "";
    }

    /**
     * Installable container that can be downloaded by Maven.
     *
     * @param id         identifier of container, eg. "tomcat5x".
     * @param groupId    groupId of container.
     * @param artifactId artifactId of container.
     * @param version    version number of container.
     * @param classifier classifier of the container.
     */
    Container(final String id, final String groupId, final String artifactId, final String version, final String classifier) {
        super(groupId, artifactId, version);
        this.id = id;
        this.type = "installed";
        this.classifier = classifier;
    }

    /**
     * Embedded container packaged with Cargo.
     *
     * @param id identifier of container, eg. "jetty6x".
     */
    Container(final String id) {
        this.id = id;
        this.type = "embedded";
        this.classifier = "";
    }

    /**
     * @return identifier of container.
     */
    public String getId() {
        return id;
    }

    /**
     * @return "installed" or "embedded".
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * @return classifier the classifier of the ProductArtifact
     */
    public String getClassifier() {
        return classifier;
    }

    /**
     * @return <code>true</code> if the container type is "embedded".
     */
    public boolean isEmbedded() {
        return "embedded".equals(type);
    }

    /**
     * @param buildDir project.build.directory.
     * @return root directory of the container that will house the container installation and configuration.
     */
    @Nonnull
    public String getRootDirectory(final String buildDir) {
        return buildDir + File.separator + "container" + File.separator + getId();
    }

    /**
     * Returns the directory into which this container is to be installed.
     *
     * @param buildDir project.build.directory
     * @return directory housing the installed container
     */
    @Nonnull
    public String getInstallDirectory(final String buildDir) {
        return getRootDirectory(buildDir) + File.separator + getFileName();
    }

    /**
     * Returns the bare file name of this container.
     *
     * @return see description
     */
    @Nonnull
    public String getFileName() {
        final String version = getVersion().replace("-atlassian-hosted", "");
        return getArtifactId() + "-" + version;
    }

    /**
     * @param buildDir project.build.directory.
     * @param productId product name.
     * @return directory to house the container configuration for the specified product.
     */
    @Nonnull
    public String getConfigDirectory(final String buildDir, final String productId) {
        return getRootDirectory(buildDir) + File.separator + "cargo-" + productId + "-home";
    }
}
