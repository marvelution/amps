package com.atlassian.maven.plugins.amps.product.manager;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.Node;
import org.apache.maven.plugin.MojoExecutionException;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Manages the web applications of the products, which includes:
 * <ul>
 *     <li>identifying, downloading, installing, starting, and stopping their servlet container (e.g. Tomcat) and</li>
 *     <li>deploying their WAR file to that container.</li>
 * </ul>
 * Does not concern itself with the product's home directories (local or shared).
 */
public interface WebAppManager {

    /**
     * Starts the servlet container and deploy the given product WAR into it.
     *
     * @param productWar                 the product's WAR file, to deploy to the servlet container
     * @param systemProperties           any system properties to be passed to the servlet container; also passes them
     *                                   to Cargo as <a href="https://codehaus-cargo.github.io/cargo/Configuration+properties.html">
     *                                   configuration properties</a>; one map per product node
     * @param extraContainerDependencies any extra artifacts to deploy to the container's own {@code lib} directory
     * @param extraProductDeployables    any artifacts to deploy to the container, in addition to the given WAR
     * @param product                    the product being deployed
     * @param mavenContext               the Maven context
     * @return the started product node(s), a non-empty list
     * @throws MojoExecutionException if something goes wrong
     */
    @Nonnull
    List<Node> startWebapp(File productWar, List<Map<String, String>> systemProperties,
                           List<ProductArtifact> extraContainerDependencies,
                           List<ProductArtifact> extraProductDeployables,
                           Product product, MavenContext mavenContext) throws MojoExecutionException;

    /**
     * Stops the servlet container of the given product.
     *
     * @param product the product whose servlet container is being stopped
     * @param mavenContext the Maven context
     * @throws MojoExecutionException if something goes wrong
     */
    void stopWebapp(Product product, MavenContext mavenContext) throws MojoExecutionException;
}
