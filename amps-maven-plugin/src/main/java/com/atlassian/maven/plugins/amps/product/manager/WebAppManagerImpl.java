package com.atlassian.maven.plugins.amps.product.manager;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.XmlOverride;
import com.atlassian.maven.plugins.amps.util.MojoExecutorWrapper;
import com.google.common.annotations.VisibleForTesting;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.HttpMethod;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.atlassian.maven.plugins.amps.product.manager.Containers.findContainer;
import static java.io.File.createTempFile;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MINUTES;
import static org.apache.commons.io.FileUtils.copyDirectory;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.apache.commons.lang3.StringUtils.upperCase;
import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.goal;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;

// This code was previously in {@code MavenGoals}; parts have since been moved out into other classes in this package.
public class WebAppManagerImpl extends AbstractLogEnabled implements WebAppManager {

    @VisibleForTesting
    static final String AJP_PORT_PROPERTY = "cargo.tomcat.ajp.port";
    private static final String PROPERTIES = "properties";

    private final MojoExecutorWrapper mojoExecutorWrapper;

    @Inject
    public WebAppManagerImpl(final MojoExecutorWrapper mojoExecutorWrapper) {
        this.mojoExecutorWrapper = requireNonNull(mojoExecutorWrapper);
    }

    @Override
    @Nonnull
    public List<Node> startWebapp(final File productWar, final List<Map<String, String>> systemProperties,
                                  final List<ProductArtifact> extraContainerDependencies,
                                  final List<ProductArtifact> extraProductDeployables,
                                  final Product product, final MavenContext mavenContext) throws MojoExecutionException {
        final List<Node> nodes = product.getNodes();
        final Container container = getContainer(product);
        for (int nodeIndex = 0; nodeIndex < nodes.size(); nodeIndex++) {
            final Node node = nodes.get(nodeIndex);
            final ContainerConfig containerConfig = new ContainerConfig(container, product, nodeIndex);
            if (!container.isEmbedded()) {
                unpackOrReuse(containerConfig, mavenContext);
            }
            getLogger().info(getStartingMessage(product, container, node, nodeIndex));

            mojoExecutorWrapper.execute(
                    cargoPlugin(mavenContext),
                    goal("start"),
                    getCargoStartConfiguration(productWar, systemProperties.get(nodeIndex), extraContainerDependencies,
                            extraProductDeployables, containerConfig, mavenContext),
                    mavenContext.getExecutionEnvironment()
            );
            final Optional<URI> statusUri = product.getStatusUri(nodeIndex);
            if (statusUri.isPresent()) {
                waitForOkResponse(statusUri.get()); // can't use ifPresent because of thrown exception
            }
        }
        return nodes;
    }

    private String getStartingMessage(
            final Product product, final Container container, final Node node, final int nodeIndex) {
        final String messageFormat =
                "Starting instance '%s'%s on the %s container on ports %d (%s), %d (RMI) and %d (AJP)";
        final String nodeBit = product.isMultiNode() ? format(" (node %d)", nodeIndex) : "";
        return format(messageFormat, product.getInstanceId(), nodeBit, container.getId(), node.getWebPort(),
                upperCase(product.getProtocol()), node.getRmiPort(), node.getAjpPort());
    }

    private static void waitForOkResponse(final URI statusUri) throws MojoExecutionException {
        final int totalWaitTimeInMinutes = 3;
        final int retryIntervalInSeconds = 5;
        final long maxRetries = MINUTES.toSeconds(totalWaitTimeInMinutes) / retryIntervalInSeconds;
        for (long i = 0; i < maxRetries; i++) {
            if (getStatusCode(statusUri) == HTTP_OK) {
                return;
            }
            waitForSeconds(retryIntervalInSeconds);
        }
        throw new MojoExecutionException(format("%s did not return 200 OK after %d attempts", statusUri, maxRetries));
    }

    private static int getStatusCode(final URI statusUri) throws MojoExecutionException {
        try {
            final URL url = statusUri.toURL();
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(HttpMethod.GET);
            connection.connect();
            return connection.getResponseCode();
        } catch (final IOException e) {
            throw new MojoExecutionException("Could not get status code from " + statusUri, e);
        }
    }

    @SuppressWarnings("SameParameterValue")
    private static void waitForSeconds(final int seconds) {
        final long waitTimeMillis = TimeUnit.SECONDS.toMillis(seconds);
        try {
            sleep(waitTimeMillis);
        } catch (final InterruptedException e) {
            currentThread().interrupt();
        }
    }

    @Nullable
    private MojoExecutor.Element xmlReplacementsElement(final Collection<XmlOverride> cargoXmlOverrides) {
        if (cargoXmlOverrides == null) {
            return null;
        }

        final MojoExecutor.Element[] xmlReplacementsElements = cargoXmlOverrides.stream().map(xmlOverride ->
                element(name("xmlReplacement"),
                        element(name("file"), xmlOverride.getFile()),
                        element(name("xpathExpression"), xmlOverride.getxPathExpression()),
                        element(name("attributeName"), xmlOverride.getAttributeName()),
                        element(name("value"), xmlOverride.getValue()))
        ).toArray(MojoExecutor.Element[]::new);

        return element(name("xmlReplacements"), xmlReplacementsElements);
    }

    private Xpp3Dom getCargoStartConfiguration(
            final File productWar, final Map<String, String> systemProperties,
            final List<ProductArtifact> extraContainerDependencies, final List<ProductArtifact> extraProductDeployables,
            final ContainerConfig containerConfig, final MavenContext mavenContext)
            throws MojoExecutionException {
        final Container container = containerConfig.getContainer();
        final Product product = containerConfig.getProduct();
        return configurationWithoutNullElements(
                element(name("deployables"), getDeployables(productWar, extraProductDeployables, product)),
                element(name("container"),
                        element(name("containerId"), container.getId()),
                        element(name("type"), container.getType()),
                        element(name("home"), containerConfig.getInstallDirectory(mavenContext.getBuildDirectory())),
                        element(name("output"), containerConfig.getLogFile()),
                        element(name("systemProperties"),
                                getContainerSystemProperties(systemProperties, product, containerConfig.getNode())),
                        element(name("dependencies"), getContainerDependencies(extraContainerDependencies, product)),
                        element(name("timeout"), String.valueOf(getStartupTimeout(product)))
                ),
                element(name("configuration"), removeNullElements(
                        element(name("configfiles"), getExtraContainerConfigurationFiles()),
                        element(name("home"), containerConfig.getConfigDirectory(mavenContext.getBuildDirectory())),
                        element(name("type"), "standalone"),
                        element(name(PROPERTIES), getConfigurationProperties(systemProperties, containerConfig)),
                        xmlReplacementsElement(product.getCargoXmlOverrides()))
                ),
                // Fix issue AMPS copy 2 War files to container
                // Refer to Cargo documentation: when project's packaging is war, ear, ejb
                // the generated artifact will automatic copy to target container.
                // For avoiding this behavior, just add an empty <deployer/> element
                element(name("deployer"))
        );
    }

    /**
     * Returns the <a href="https://codehaus-cargo.github.io/cargo/Configuration+properties.html">properties</a> to
     * apply to the Cargo configuration.
     *
     * @param systemProperties any user-provided properties
     * @param containerConfig the container configuration
     * @return XML elements whose names are the property keys and value are the property values
     */
    @VisibleForTesting
    MojoExecutor.Element[] getConfigurationProperties(
            final Map<String, String> systemProperties, final ContainerConfig containerConfig) {
        final Product product = containerConfig.getProduct();
        final Node node = containerConfig.getNode();
        final List<MojoExecutor.Element> props = new ArrayList<>();
        for (final Map.Entry<String, String> entry : systemProperties.entrySet()) {
            props.add(element(name(entry.getKey()), entry.getValue()));
        }
        props.add(element(name("cargo.servlet.port"), String.valueOf(node.getWebPort())));

        if (TRUE.equals(product.getUseHttps())) {
            getLogger().debug("starting Tomcat using HTTPS via Cargo with the following parameters:");
            getLogger().debug("cargo.servlet.port = " + node.getWebPort());
            getLogger().debug("cargo.protocol = " + product.getProtocol());
            props.add(element(name("cargo.protocol"), product.getProtocol()));

            getLogger().debug("cargo.tomcat.connector.clientAuth = " + product.getHttpsClientAuth());
            props.add(element(name("cargo.tomcat.connector.clientAuth"), product.getHttpsClientAuth()));

            getLogger().debug("cargo.tomcat.connector.sslProtocol = " + product.getHttpsSSLProtocol());
            props.add(element(name("cargo.tomcat.connector.sslProtocol"), product.getHttpsSSLProtocol()));

            getLogger().debug("cargo.tomcat.connector.keystoreFile = " + product.getHttpsKeystoreFile());
            props.add(element(name("cargo.tomcat.connector.keystoreFile"), product.getHttpsKeystoreFile()));

            getLogger().debug("cargo.tomcat.connector.keystorePass = " + product.getHttpsKeystorePass());
            props.add(element(name("cargo.tomcat.connector.keystorePass"), product.getHttpsKeystorePass()));

            getLogger().debug("cargo.tomcat.connector.keyAlias = " + product.getHttpsKeyAlias());
            props.add(element(name("cargo.tomcat.connector.keyAlias"), product.getHttpsKeyAlias()));

            getLogger().debug("cargo.tomcat.httpSecure = " + product.getHttpsHttpSecure());
            props.add(element(name("cargo.tomcat.httpSecure"), product.getHttpsHttpSecure().toString()));
        }

        props.add(element(name(AJP_PORT_PROPERTY), String.valueOf(node.getAjpPort())));
        props.add(element(name("cargo.rmi.port"), String.valueOf(node.getRmiPort())));
        props.add(element(name("cargo.jvmargs"), product.getJvmArgs() + node.getDebugArgs()));
        return props.toArray(new MojoExecutor.Element[0]);
    }

    private static Xpp3Dom configurationWithoutNullElements(final MojoExecutor.Element... elements) {
        return configuration(removeNullElements(elements));
    }

    private static MojoExecutor.Element[] removeNullElements(final MojoExecutor.Element... elements) {
        return stream(elements)
                .filter(Objects::nonNull)
                .toArray(MojoExecutor.Element[]::new);
    }

    private MojoExecutor.Element[] getContainerSystemProperties(
            final Map<String, String> systemProperties, final Product product, final Node node) {
        final Map<String, String> containerSystemProperties = new HashMap<>(systemProperties);
        containerSystemProperties.put("baseurl", product.getBaseUrlForPort(node.getWebPort()));
        return containerSystemProperties.entrySet().stream()
                .map(e -> element(name(e.getKey()), e.getValue()))
                .toArray(MojoExecutor.Element[]::new);
    }

    private int getStartupTimeout(final Product product) {
        if (FALSE.equals(product.getSynchronousStartup())) {
            return 0;
        }
        return product.getStartupTimeout();
    }

    private Container getCustomContainer(final Product product) {
        final String customContainerArtifactStr = product.getCustomContainerArtifact().trim();
        final String[] containerData = customContainerArtifactStr.split(":");
        if (containerData.length < 3 || containerData.length > 5) {
            throw new IllegalArgumentException(format("Container artifact string must have the format" +
                    " groupId:artifactId:version[:packaging:classifier] or groupId:artifactId:version:classifier;" +
                    " actual string = '%s'", customContainerArtifactStr));
        }
        final String cargoContainerId = findContainer(product.getContainerId()).getId();
        final String groupId = containerData[0];
        final String artifactId = containerData[1];
        final String version = containerData[2];
        switch (containerData.length) {
            case 3:
                return new Container(cargoContainerId, groupId, artifactId, version);
            case 4:
                return new Container(cargoContainerId, groupId, artifactId, version, containerData[3]);
            case 5:
                // cause unpack have hardcoded packaging
                return new Container(cargoContainerId, groupId, artifactId, version, containerData[4]);
            default:
                // We checked the array length earlier, this should never happen
                throw new IllegalStateException(format("Unexpected container data %s", Arrays.toString(containerData)));
        }
    }

    private MojoExecutor.Element[] getContainerDependencies(
            final List<ProductArtifact> extraContainerDependencies, final Product product)
            throws MojoExecutionException {
        final List<MojoExecutor.Element> dependencyElements = new ArrayList<>();
        final List<ProductArtifact> containerDependencies = new ArrayList<>(extraContainerDependencies);
        product.getDataSources().forEach(ds -> containerDependencies.addAll(ds.getLibArtifacts()));
        for (final ProductArtifact containerDependency : containerDependencies) {
            dependencyElements.add(element(name("dependency"),
                    element(name("location"), product.getArtifactRetriever().resolve(containerDependency))
            ));
        }
        return dependencyElements.toArray(new MojoExecutor.Element[0]);
    }

    // See https://codehaus-cargo.github.io/cargo/Configuration+files+option.html
    private MojoExecutor.Element[] getExtraContainerConfigurationFiles() throws MojoExecutionException {
        return new MojoExecutor.Element[]{
                // For AMPS-1429, apply a custom context.xml with a correctly configured JarScanFilter
                element("configfile",
                        element("file", getContextXml().getAbsolutePath()),
                        element("todir", "conf"),
                        element("tofile", "context.xml"),
                        element("configfile", "true")
                )
        };
    }

    private MojoExecutor.Element[] getDeployables(
            final File productWar, final List<ProductArtifact> extraDeployables, final Product webappContext) {
        final List<MojoExecutor.Element> deployables = new ArrayList<>();
        // Add the given product WAR file
        deployables.add(element(
                name("deployable"),
                element(name("groupId"), "foo"),  // presumably not used
                element(name("artifactId"), "bar"),   // presumably not used
                element(name("type"), "war"),
                element(name("location"), productWar.getPath()),
                element(name(PROPERTIES),
                        element(name("context"), webappContext.getContextPath())
                )
        ));

        for (final ProductArtifact artifact : extraDeployables) {
            deployables.add(element(name("deployable"),
                    element(name("groupId"), artifact.getGroupId()),
                    element(name("artifactId"), artifact.getArtifactId()),
                    element(name("type"), artifact.getType()),
                    element(name("location"), artifact.getPath())
            ));
        }
        return deployables.toArray(new MojoExecutor.Element[0]);
    }

    private Container getContainer(final Product product) {
        if (product.getCustomContainerArtifact() == null) {
            return findContainer(product.getContainerId());
        } else {
            return getCustomContainer(product);
        }
    }

    private void unpackOrReuse(final ContainerConfig containerConfig, final MavenContext mavenContext)
            throws MojoExecutionException {
        final Container container = containerConfig.getContainer();
        final File containerDir = new File(containerConfig.getInstallDirectory(mavenContext.getBuildDirectory()));
        // Container dir is like:
        // .../target/container/tomcat8x/apache-tomcat-8.5.40 for node 0
        // .../target/container/tomcat8x/apache-tomcat-8.5.40-1 for node 1
        // .../target/container/tomcat8x/apache-tomcat-8.5.40-2 for node 2, etc
        if (containerDir.isDirectory()) {
            getLogger().info("Reusing unpacked container '" + container.getId() + "' from " + containerDir.getPath());
        } else if (containerConfig.isFirstNode()) {
            getLogger().info("Unpacking container '" + container.getId() + "' from container artifact: " + container);
            unpackContainer(container, mavenContext);
        } else {
            // Container not found, not the first node => copy the first node's container
            getLogger().info("Reusing container '" + container.getId() + "' from node 0");
            final File nodeZeroContainerDir = new File(containerDir.getParentFile(), container.getFileName());
            try {
                copyDirectory(nodeZeroContainerDir, containerDir);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Uses the maven-dependency-plugin to download the given container's artifact from Maven and unzip it into the
     * directory where all the Tomcat instances are located. We only ever call this method for the first node.
     *
     * @param container the container to unpack
     * @param mavenContext the Maven context
     * @throws MojoExecutionException if unpacking fails
     */
    private void unpackContainer(final Container container, final MavenContext mavenContext)
            throws MojoExecutionException {
        mojoExecutorWrapper.executeWithMergedConfig(
                mavenContext.getPlugin("org.apache.maven.plugins", "maven-dependency-plugin"),
                goal("unpack"),
                configuration(
                        element(name("artifactItems"),
                                element(name("artifactItem"),
                                        element(name("groupId"), container.getGroupId()),
                                        element(name("artifactId"), container.getArtifactId()),
                                        element(name("version"), container.getVersion()),
                                        element(name("classifier"), container.getClassifier()),
                                        element(name("type"), "zip"))),  // see AMPS-1527
                        element(name("outputDirectory"), container.getRootDirectory(mavenContext.getBuildDirectory()))
                ),
                mavenContext.getExecutionEnvironment()
        );
    }

    /**
     * Returns the <code>context.xml</code> file to be copied into the Tomcat instance.
     *
     * @return an extant file
     */
    private File getContextXml() throws MojoExecutionException {
        try {
            // Because Cargo needs an absolute file path, we copy context.xml from the AMPS JAR to a temp file
            final File tempContextXml = createTempFile("context.xml", null);
            final InputStream contextXmlToCopy = requireNonNull(getClass().getResourceAsStream("context.xml"));
            copyInputStreamToFile(contextXmlToCopy, tempContextXml);
            return tempContextXml;
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to create Tomcat context.xml", e);
        }
    }

    /**
     * Returns the Cargo plugin to be used.
     *
     * @param mavenContext the Maven context
     */
    private Plugin cargoPlugin(final MavenContext mavenContext) {
        final Plugin cargoPlugin = mavenContext.getPlugin("org.codehaus.cargo", "cargo-maven2-plugin");
        getLogger().info("using codehaus cargo v" + cargoPlugin.getVersion());
        return cargoPlugin;
    }

    @Override
    public void stopWebapp(final Product product, final MavenContext mavenContext)
            throws MojoExecutionException {
        final Container container = getContainer(product);
        final int shutdownTimeout = TRUE.equals(product.getSynchronousStartup()) ? 0 : product.getShutdownTimeout();
        for (final Node node : product.getNodes()) {
            stopNode(container, product, node, mavenContext, shutdownTimeout);
        }
    }

    private void stopNode(final Container container, final Product product, final Node node,
                          final MavenContext mavenContext, final int shutdownTimeout)
            throws MojoExecutionException {
        mojoExecutorWrapper.execute(
                cargoPlugin(mavenContext),
                goal("stop"),
                configuration(
                        element(name("container"),
                                element(name("containerId"), container.getId()),
                                element(name("type"), container.getType()),
                                element(name("timeout"), String.valueOf(shutdownTimeout)),
                                element(name("home"), container.getInstallDirectory(mavenContext.getBuildDirectory()))
                        ),
                        element(name("configuration"),
                                element(name("home"), container.getConfigDirectory(
                                        mavenContext.getBuildDirectory(), product.getInstanceId())),
                                element(name(PROPERTIES), createShutdownPortsPropertiesConfiguration(node))
                        )
                ),
                mavenContext.getExecutionEnvironment()
        );
    }

    /**
     * Cargo waits (AbstractCatalinaInstalledLocalContainer#waitForCompletion(boolean)) for the HTTP and AJP ports to
     * close before it considers the container to be stopped.
     * <p>
     * Since {@link WebAppManager#startWebapp} can use random ports for HTTP and AJP, it's possible the container isn't
     * using the ports defined on the {@link Product}. For the HTTP port we just accept that risk, on the assumption
     * it's likely to be a minority case. For the AJP port, rather than waiting for the AJP port, we configure it to use
     * the HTTP port as the AJP port.
     * <p>
     * Note that the RMI port <i>is intentionally not configured here</i>. Earlier versions of AMPS set the HTTP port
     * as the RMI port as well, but <a href="https://codehaus-cargo.atlassian.net/browse/CARGO-1337">CARGO-1337</a>
     * resulted in a change in Cargo that causes it to no longer wait for the RMI port to stop. That means if we set
     * the AJP and HTTP ports to a value that matches the RMI port Cargo doesn't wait for <i>any</i> of the sockets
     * to be closed and just immediately concludes the container has stopped.
     */
    private MojoExecutor.Element[] createShutdownPortsPropertiesConfiguration(final Node node) {
        final String webPort = String.valueOf(node.getWebPort());
        final List<MojoExecutor.Element> properties = new ArrayList<>();
        properties.add(element(name("cargo.servlet.port"), webPort));
        properties.add(element(name(AJP_PORT_PROPERTY), webPort));
        return properties.toArray(new MojoExecutor.Element[0]);
    }
}
