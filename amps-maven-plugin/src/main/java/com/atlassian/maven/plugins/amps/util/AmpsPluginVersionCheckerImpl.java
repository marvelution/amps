package com.atlassian.maven.plugins.amps.util;

import com.atlassian.plugins.codegen.AmpsVersionUpdate;
import com.atlassian.plugins.codegen.MavenProjectRewriter;
import com.atlassian.plugins.codegen.PluginProjectChangeset;
import com.atlassian.plugins.codegen.ProjectRewriter;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.utils.logging.MessageUtils;
import org.codehaus.plexus.components.interactivity.Prompter;
import org.codehaus.plexus.components.interactivity.PrompterException;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.dom4j.DocumentException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.prefs.Preferences;

import static com.atlassian.maven.plugins.amps.util.AmpsPluginVersionCheckerImpl.PomVersionElement.MANAGEMENT;
import static com.atlassian.maven.plugins.amps.util.AmpsPluginVersionCheckerImpl.PomVersionElement.PLUGIN;
import static com.atlassian.plugins.codegen.AmpsVersionUpdate.ampsVersionUpdate;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;
import static java.util.prefs.Preferences.userNodeForPackage;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.substringBetween;

/**
 * Compares the running version of AMPS to the version stated in the POM.
 * If the POM version is lower than the SDK version, it will prompt the developer
 * asking if they would like to have their pom updated with the new version.
 */
public class AmpsPluginVersionCheckerImpl extends AbstractLogEnabled implements AmpsPluginVersionChecker {
    private static final List<String> YN_ANSWERS = new ArrayList<>(Arrays.asList("Y", "y", "N", "n"));

    public static final String NO_VERSION_DEFINED = "no-version-defined";
    public static final String POM_UPDATE_PREF_PREFIX = "sdk-pom-update-check";

    // probably set by Maven's DI framework
    @SuppressWarnings("unused")
    private Prompter prompter;

    private boolean skipCheck;

    @Override
    public void checkAmpsVersionInPom(final String currentVersion, final MavenProject project) {
        if (skipCheck || project.getFile() == null || !project.getFile().exists()) {
            return;
        }

        final String prefKey = POM_UPDATE_PREF_PREFIX + "-" + currentVersion + "-" + md5Hex(project.getArtifactId());
        final Preferences prefs = userNodeForPackage(AmpsPluginVersionCheckerImpl.class);
        final String alreadyRan = prefs.get(prefKey, null);

        if (alreadyRan != null) {
            return;
        }

        prefs.put(prefKey, "true");

        try {
            final ArtifactVersion runningVersion = new DefaultArtifactVersion(currentVersion);
            final VersionInfo versionInfo = getVersionAndRewriter(project);
            final ArtifactVersion versionInPom = versionInfo.artifactVersion;
            final PomVersionElement pomElement = versionInfo.pomVersionElement;
            final String pomElementType = pomElement.getType();
            final ProjectRewriter rewriter = versionInfo.projectRewriter;

            final boolean managementNeedsUpdate =
                    pomElementType.equals(MANAGEMENT) && versionInPomIsOlderOrNotDefined(runningVersion, versionInPom);
            final boolean pluginNeedsUpdate =
                    pomElementType.equals(PLUGIN) && versionInPomIsOlderOrNotDefined(runningVersion, versionInPom);

            if ((pluginNeedsUpdate || managementNeedsUpdate) && promptToUpdatePom(versionInPom, runningVersion)) {
                rewriter.applyChanges(getPluginProjectChanges(currentVersion, managementNeedsUpdate, pluginNeedsUpdate));

                // update the property in the correct POM
                final ProjectRewriter propRewriter;
                final String versionProp = pomElement.getVersionProperty();
                if (isNotBlank(versionProp)) {
                    propRewriter = getRewriterForVersionProp(versionProp, project).orElse(null);
                } else {
                    propRewriter = rewriter;
                }

                if (propRewriter == null) {
                    getLogger().error("unable to update AMPS version in POM");
                } else {
                    final PluginProjectChangeset propChanges = new PluginProjectChangeset().with(ampsVersionUpdate(
                            currentVersion, AmpsVersionUpdate.PLUGIN, false, true));
                    propRewriter.applyChanges(propChanges);
                    getLogger().info("AMPS version in pom updated to " + currentVersion);
                }
            }
        } catch (Exception t) {
            getLogger().error("unable to check AMPS version in POM...", t);
        }
    }

    private PluginProjectChangeset getPluginProjectChanges(
            final String currentVersion, final boolean managementNeedsUpdate, final boolean pluginNeedsUpdate) {
        PluginProjectChangeset changes = new PluginProjectChangeset();
        if (pluginNeedsUpdate) {
            changes = changes.with(ampsVersionUpdate(currentVersion, AmpsVersionUpdate.PLUGIN, true, false));
        }
        if (managementNeedsUpdate) {
            changes = changes.with(ampsVersionUpdate(currentVersion, AmpsVersionUpdate.MANAGEMENT, true, false));
        }
        return changes;
    }

    private boolean versionInPomIsOlderOrNotDefined(
            final ArtifactVersion runningVersion, final ArtifactVersion versionInPom) {
        return NO_VERSION_DEFINED.equalsIgnoreCase(versionInPom.toString()) || runningVersion.compareTo(versionInPom) > 0;
    }

    private static class VersionInfo {

        private final ArtifactVersion artifactVersion;
        private final PomVersionElement pomVersionElement;
        private final ProjectRewriter projectRewriter;

        private VersionInfo(final ArtifactVersion artifactVersion,
                            final PomVersionElement pomVersionElement,
                            final ProjectRewriter projectRewriter) {
            this.artifactVersion = artifactVersion;
            this.pomVersionElement = pomVersionElement;
            this.projectRewriter = projectRewriter;
        }
    }

    private VersionInfo getVersionAndRewriter(final MavenProject project) throws IOException, DocumentException {
        if (project.getFile() == null || !project.getFile().exists()) {
            return new VersionInfo(
                    new DefaultArtifactVersion(NO_VERSION_DEFINED),
                    new PomVersionElement(MANAGEMENT, null),
                    new NOOPProjectRewriter());
        }

        final MavenProjectRewriter rewriter = new MavenProjectRewriter(project.getFile());

        final String managementVersionInPom = rewriter.getAmpsPluginManagementVersionInPom();
        String pluginVersionInPom = "";

        final ArtifactVersion ampsManagementVersionInPom = getPomVersion(managementVersionInPom, project);
        ArtifactVersion ampsVersionInPom;
        try {
            pluginVersionInPom = rewriter.getAmpsVersionInPom();
            ampsVersionInPom = getPomVersion(pluginVersionInPom, project);
        } catch (IllegalStateException e) {
            ampsVersionInPom = new DefaultArtifactVersion(NO_VERSION_DEFINED);
        }

        String versionProp = "";
        if (managementVersionInPom.startsWith("$")) {
            versionProp = managementVersionInPom;
        }
        if (pluginVersionInPom.startsWith("$")) {
            versionProp = pluginVersionInPom;
        }

        if (project.hasParent() && !isDefined(ampsManagementVersionInPom) && !isDefined(ampsVersionInPom)) {
            return getVersionAndRewriter(project.getParent());
        } else if (isDefined(ampsManagementVersionInPom)) {
            return new VersionInfo(ampsManagementVersionInPom, new PomVersionElement(MANAGEMENT, versionProp), rewriter);
        } else {
            return new VersionInfo(ampsVersionInPom, new PomVersionElement(PLUGIN, versionProp), rewriter);
        }
    }

    private static boolean isDefined(final ArtifactVersion artifactVersion) {
        return artifactVersion != null && !NO_VERSION_DEFINED.equalsIgnoreCase(artifactVersion.toString());
    }

    @Nonnull
    private Optional<ProjectRewriter> getRewriterForVersionProp(final String versionProp, final MavenProject project)
            throws IOException, DocumentException {
        if (project.getFile() == null || !project.getFile().exists()) {
            return empty();
        }

        final MavenProjectRewriter rewriter = new MavenProjectRewriter(project.getFile());
        final String propName = substringBetween(versionProp, "${", "}");

        if (isNotBlank(propName) && rewriter.definesProperty(propName)) {
            return Optional.of(rewriter);
        } else if (project.hasParent()) {
            return getRewriterForVersionProp(versionProp, project.getParent());
        } else {
            return empty();
        }
    }

    @Override
    public void skipPomCheck(boolean skip) {
        this.skipCheck = skip;
    }

    private boolean promptToUpdatePom(final ArtifactVersion ampsVersionInPom, final ArtifactVersion runningVersion)
            throws PrompterException {
        final String prompt = MessageUtils.buffer()
                .warning("You are running AMPS plugin version ")
                .warning(runningVersion)
                .warning(" but your POM is using version ")
                .warning(ampsVersionInPom)
                .newline()
                .strong("Would you like to have your POM updated?")
                .toString();

        return promptForBoolean(prompt);
    }

    @Nullable
    private DefaultArtifactVersion getPomVersion(final String ampsVersionOrProperty, final MavenProject project) {
        DefaultArtifactVersion ampsVersionInPom = null;

        if (isNotBlank(ampsVersionOrProperty) && ampsVersionOrProperty.startsWith("$")) {
            final String propName = substringBetween(ampsVersionOrProperty, "${", "}");
            if (isNotBlank(propName)) {
                ampsVersionInPom = new DefaultArtifactVersion(project.getProperties().getProperty(propName, "0.0"));
            }
        } else if (isNotBlank(ampsVersionOrProperty)) {
            ampsVersionInPom = new DefaultArtifactVersion(ampsVersionOrProperty);
        } else {
            ampsVersionInPom = new DefaultArtifactVersion(NO_VERSION_DEFINED);
        }

        return ampsVersionInPom;
    }

    private boolean promptForBoolean(final String message) throws PrompterException {
        final String answer;
        if (isBlank("Y")) {
            answer = prompter.prompt(message, YN_ANSWERS);
        } else {
            answer = prompter.prompt(message, YN_ANSWERS, "Y");
        }

        return "y".equalsIgnoreCase(answer);
    }

    static class PomVersionElement {
        public static final String PLUGIN = "plugin";
        public static final String MANAGEMENT = "management";

        private final String type;
        private final String versionProperty;

        private PomVersionElement(String type, String versionProperty) {
            this.type = checkNotNull(type, "type");
            this.versionProperty = versionProperty;
        }

        public String getType() {
            return type;
        }

        public String getVersionProperty() {
            return versionProperty;
        }
    }
}
