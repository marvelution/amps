package com.atlassian.maven.plugins.amps.util;

import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.google.common.annotations.VisibleForTesting;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.metadata.ArtifactRepositoryMetadata;
import org.apache.maven.artifact.repository.metadata.Metadata;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadata;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataManager;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataResolutionException;
import org.apache.maven.artifact.repository.metadata.SnapshotArtifactRepositoryMetadata;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.repository.RepositorySystem;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

import static com.atlassian.maven.plugins.amps.product.FeCruProductHandler.isFecruRelease;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ArtifactRetriever {

    private final RepositorySystem repositorySystem;
    private final ArtifactRepository localRepository;
    private final ArtifactResolver artifactResolver;
    private final List<ArtifactRepository> remoteRepositories;
    private final RepositoryMetadataManager repositoryMetadataManager;

    public ArtifactRetriever(final ArtifactResolver artifactResolver,
                             final RepositorySystem repositorySystem,
                             final ArtifactRepository localRepository,
                             final List<ArtifactRepository> remoteRepositories,
                             final RepositoryMetadataManager repositoryMetadataManager) {
        this.repositorySystem = requireNonNull(repositorySystem);
        this.artifactResolver = requireNonNull(artifactResolver);
        this.localRepository = requireNonNull(localRepository);
        this.remoteRepositories = requireNonNull(remoteRepositories);
        this.repositoryMetadataManager = requireNonNull(repositoryMetadataManager);
    }

    public String resolve(final ProductArtifact dependency) throws MojoExecutionException {
        final Artifact artifact = this.repositorySystem.createArtifact(dependency.getGroupId(),
                dependency.getArtifactId(), dependency.getVersion(), "compile", "jar");
        final ArtifactResolutionRequest artifactResolutionRequest = new ArtifactResolutionRequest();
        artifactResolutionRequest.setArtifact(artifact);
        artifactResolutionRequest.setLocalRepository(localRepository);
        artifactResolutionRequest.setRemoteRepositories(remoteRepositories);
        final ArtifactResolutionResult resolutionResult = artifactResolver.resolve(artifactResolutionRequest);
        if (resolutionResult.isSuccess()) {
            return artifact.getFile().getPath();
        }
        throw new MojoExecutionException("Cannot resolve " + artifact, new ArtifactResolutionException(resolutionResult));
    }

    public String getLatestStableVersion(final Artifact artifact) throws MojoExecutionException {
        RepositoryMetadata metadata;

        if (!artifact.isSnapshot() || Artifact.LATEST_VERSION.equals(artifact.getBaseVersion()) || Artifact.RELEASE_VERSION.equals(artifact.getBaseVersion())) {
            metadata = new ArtifactRepositoryMetadata(artifact);
        } else {
            metadata = new SnapshotArtifactRepositoryMetadata(artifact);
        }

        try {
            repositoryMetadataManager.resolve(metadata, remoteRepositories, localRepository);
            artifact.addMetadata(metadata);

            Metadata repoMetadata = metadata.getMetadata();
            String version = null;

            if (repoMetadata != null && repoMetadata.getVersioning() != null) {
                version = getLatestOfficialRelease(repoMetadata.getVersioning().getVersions(), artifact);
            }

            if (version == null) {
                // use the local copy, or if it doesn't exist - go to the remote repo for it
                version = artifact.getBaseVersion();
            }

            return version;
        } catch (RepositoryMetadataResolutionException e) {
            throw new MojoExecutionException("Error resolving stable version", e);
        }
    }

    /**
     * Returns the latest of the given versions, excluding unofficial releases.
     *
     * @param versions the versions from which to get the latest
     * @param artifact the artifact to which these versions relate
     * @return null if no official release versions were given
     */
    @Nullable
    @VisibleForTesting
    static String getLatestOfficialRelease(final Collection<String> versions, final Artifact artifact) {
        return versions.stream()
                .map(DefaultArtifactVersion::new)
                .filter(version -> isOfficialRelease(version, artifact))
                .max(ArtifactVersion::compareTo)
                .map(ArtifactVersion::toString)
                .orElse(null);
    }

    private static boolean isOfficialRelease(final ArtifactVersion version, final Artifact artifact) {
        // Most products' final releases are like 1.2.3 (no qualifier), but Fecru has a timestamp suffix
        return isBlank(version.getQualifier()) || isFecruRelease(version, artifact);
    }
}
