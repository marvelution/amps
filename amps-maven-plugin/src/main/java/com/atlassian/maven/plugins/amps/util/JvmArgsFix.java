package com.atlassian.maven.plugins.amps.util;

import com.atlassian.maven.plugins.amps.product.JavaModulePackage;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.addAll;
import static java.util.Collections.singletonMap;
import static org.apache.commons.lang3.JavaVersion.JAVA_11;
import static org.apache.commons.lang3.JavaVersion.JAVA_9;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.SystemUtils.isJavaVersionAtLeast;

/**
 * @since 3.8
 */
public class JvmArgsFix {
    private final Map<String, String> defaultParams;

    private final Set<JavaModulePackage> addOpens = new LinkedHashSet<>();
    private final Set<JavaModulePackage> addExports = new LinkedHashSet<>();
    private final Set<File> argsFiles = new LinkedHashSet<>();

    private JvmArgsFix(Map<String, String> initial) {
        defaultParams = new LinkedHashMap<>(initial);
    }

    private JvmArgsFix() {
        defaultParams = new LinkedHashMap<>();
    }

    public static JvmArgsFix defaults() {
        return new JvmArgsFix(singletonMap("-Xmx", "512m"));
    }

    public static JvmArgsFix empty() {
        return new JvmArgsFix();
    }

    public JvmArgsFix with(String param, String value) {
        defaultParams.put(param, value);
        return this;
    }

    /**
     * @since 8.3.0
     */
    public JvmArgsFix withAddOpens(JavaModulePackage... packages) {
        addAll(addOpens, packages);
        return this;
    }

    /**
     * @since 8.3.0
     */
    public JvmArgsFix withAddOpens(Collection<JavaModulePackage> packages) {
        addOpens.addAll(packages);
        return this;
    }

    /**
     * @since 8.3.0
     */
    public JvmArgsFix withAddExports(JavaModulePackage... packages) {
        addAll(addExports, packages);
        return this;
    }

    /**
     * @since 8.3.0
     */
    public JvmArgsFix withAddExports(Collection<JavaModulePackage> packages) {
        addExports.addAll(packages);
        return this;
    }

    public JvmArgsFix withArgsFile(File argsFile) {
        argsFiles.add(argsFile);
        return this;
    }

    public String apply(String jvmArgs) {
        final List<String> args = new ArrayList<>();
        if (isNotBlank(jvmArgs)) {
            args.add(jvmArgs);
        }

        for (Map.Entry<String, String> param : defaultParams.entrySet()) {
            if (!contains(jvmArgs, param.getKey())) {
                args.add(param.getKey() + param.getValue());
            }
        }

        if (isJavaVersionAtLeast(JAVA_9)) {
            for (JavaModulePackage addOpen : addOpens) {
                args.add(addOpen.createAddOpensToUnnamed());
            }
            for (JavaModulePackage addOpen : addExports) {
                args.add(addOpen.createAddExportsToUnnamed());
            }
        }

        if (isJavaVersionAtLeast(JAVA_11)) { // args files supported from Java 11 onwards
            argsFiles.stream()
                    .filter(File::isFile)
                    .forEach(argFile -> args.add("@" + argFile.getPath()));
        }

        return join(args, ' ');
    }
}
