package com.atlassian.maven.plugins.amps.util;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.joining;

/**
 * {@link java.util.Map}-related utility methods.
 *
 * @since 6.2.8
 */
@ParametersAreNonnullByDefault
public final class MapUtils {

    /**
     * Joins the given map into a string using the given delimiters.
     *
     * @param map               the map to stringify
     * @param keyValueSeparator the delimiter between the key and value of each map entry
     * @param entrySeparator    the delimiter between successive map entries
     * @return see above
     */
    @Nonnull
    public static String join(final Map<?, ?> map,
                              final String keyValueSeparator,
                              final CharSequence entrySeparator) {
        return map.entrySet().stream()
                .map(entry -> entry.getKey() + keyValueSeparator + entry.getValue())
                .collect(joining(entrySeparator));
    }

    /**
     * Merges the two given maps, with the second map's entries taking precedence in the case of key clashes.
     *
     * @param losers the first map
     * @param winners the second map (wins any clashes)
     * @param <V> the value type
     * @return the merged map
     * @since 8.3
     */
    @Nonnull
    public static <V> Map<String, V> merge(final Map<String, V> losers, final Map<String, V> winners) {
        final Map<String, V> merged = new HashMap<>();
        merged.putAll(losers);
        merged.putAll(winners);
        return merged;
    }

    private MapUtils() {
    }
}
