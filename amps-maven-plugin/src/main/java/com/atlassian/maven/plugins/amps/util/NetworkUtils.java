package com.atlassian.maven.plugins.amps.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.stream.Stream;

import static com.atlassian.maven.plugins.amps.util.StreamUtils.stream;
import static java.util.stream.Stream.empty;

/**
 * Networking-related utility methods.
 *
 * @since 8.3
 */
public final class NetworkUtils {

    private NetworkUtils() {}

    /**
     * Returns the name of the loopback interface.
     *
     * @return see description
     * @see NetworkInterface#getNetworkInterfaces()
     * @see NetworkInterface#isLoopback()
     * @see NetworkInterface#getName()
     */
    public static String getLoopbackInterface() {
            return getNetworkInterfaces()
                    .filter(NetworkUtils::isLoopback)
                    .map(NetworkInterface::getName)
                    .findFirst()
                    .orElseThrow(IllegalStateException::new);
    }

    private static Stream<NetworkInterface> getNetworkInterfaces() {
        try {
            return stream(NetworkInterface.getNetworkInterfaces());
        } catch (SocketException e) {
            return empty();
        }
    }

    private static boolean isLoopback(final NetworkInterface networkInterface) {
        try {
            return networkInterface.isLoopback();
        } catch (final SocketException e) {
            return false;
        }
    }
}
