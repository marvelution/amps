package com.atlassian.maven.plugins.amps.util;

import com.atlassian.maven.plugins.amps.MavenContext;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.maven.plugins.amps.util.FileUtils.file;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @since 3.6.1
 */
public final class PluginXmlUtils {
    private static final String XPATH_PLUGIN = "/atlassian-plugin";
    private static final String XPATH_PLUGIN_INFO = XPATH_PLUGIN + "/plugin-info";
    private static final String XPATH_REST = XPATH_PLUGIN + "/rest";
    private static final String XPATH_PACKAGE_RELATIVE = "package";

    public static PluginInfo getPluginInfo(MavenContext context) {
        File pluginXml = getPluginXml(context);
        PluginInfo pluginInfo = null;

        if (pluginXml.exists()) {
            try {
                Document pluginDoc = getXmlDocument(pluginXml);
                Node root = pluginDoc.selectSingleNode(XPATH_PLUGIN);
                String name = "";
                String desc = "";

                if (null != root) {
                    name = root.valueOf("@name");
                    Node infoNode = pluginDoc.selectSingleNode(XPATH_PLUGIN_INFO);

                    if (null != infoNode) {
                        Node descNode = infoNode.selectSingleNode("description");
                        if (null != descNode) {
                            desc = descNode.valueOf("text()");
                        }
                    }
                }

                pluginInfo = new PluginInfo(name, desc);


            } catch (Exception e) {
                //print the error but continue since this is not a critical feature
                context.getLog().warn(e.getMessage());
            }
        }

        if (null == pluginInfo) {
            pluginInfo = new PluginInfo("unknown plugin", "");
        }

        return pluginInfo;
    }

    public static List<RESTModuleInfo> getRestModules(final MavenContext context) {
        final File pluginXml = getPluginXml(context);
        if (!pluginXml.isFile()) {
            // Not necessarily an error, for example current module is not a P2 plugin
            return emptyList();
        }
        final List<RESTModuleInfo> modules = new ArrayList<>();
        try {
            final Document pluginDoc = getXmlDocument(pluginXml);
            @SuppressWarnings("unchecked")
            final List<Node> restNodes = pluginDoc.selectNodes(XPATH_REST);
            restNodes.stream()
                    .map(PluginXmlUtils::getRestModule)
                    .forEach(modules::add);
        } catch (final Exception e) {
            // print the error but continue, since this is not a critical feature
            context.getLog().warn(e.getMessage());
        }
        return modules;
    }

    private static RESTModuleInfo getRestModule(final Node restNode) {
        final String name = restNode.valueOf("@name");
        String desc = restNode.valueOf("@description");
        final List<String> packagesToScan = new ArrayList<>();

        if (isBlank(desc)) {
            final Node descNode = restNode.selectSingleNode("description");
            if (null != descNode) {
                desc = descNode.valueOf("text()");
            }
        }

        final List<Node> packageNodes = restNode.selectNodes(XPATH_PACKAGE_RELATIVE);
        for (Node packageNode : packageNodes) {
            packagesToScan.add(packageNode.valueOf("text()"));
        }

        return new RESTModuleInfo(name, desc, packagesToScan);
    }

    /**
     * Returns the plugin XML file from the target/classes directory.
     *
     * @param context the Maven context
     * @return a file that may or may not exist; it only gets copied/filtered into the above directory when the user
     * runs the Maven {@code process-resources} phase (directly or via some later phase in the default lifecycle)
     */
    @Nonnull
    public static File getPluginXml(final MavenContext context) {
        return file(context.getProject()
                .getBuild()
                .getOutputDirectory(), "atlassian-plugin.xml");
    }

    protected static Document getXmlDocument(File xmlFile) throws DocumentException {
        SAXReader reader = new SAXReader();
        return reader.read(xmlFile);
    }

    private PluginXmlUtils() {
    }

    public static class RESTModuleInfo {
        private String name;
        private String description;
        private List<String> packagesToScan;

        public RESTModuleInfo(String name, String description, List<String> packagesToScan) {
            this.name = name;
            this.description = description;
            this.packagesToScan = packagesToScan;
        }

        public String getName() {
            return name;
        }

        public List<String> getPackagesToScan() {
            return packagesToScan;
        }

        public String getDescription() {
            return description;
        }

    }

    public static class PluginInfo {
        private String name;
        private String description;

        public PluginInfo(String name, String description) {
            this.name = name;
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

    }
}
