package com.atlassian.maven.plugins.amps.util;

import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier;
import static javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Utility methods for use by ProductHandlers.
 */
@ParametersAreNonnullByDefault
public final class ProductHandlerUtil {

    private ProductHandlerUtil() {
        throw new UnsupportedOperationException("Do not implement");
    }

    @SuppressWarnings("squid:S4830")
    private static final X509TrustManager PERMISSIVE_TRUST_MANAGER = new X509TrustManager() {

        @Override
        public void checkClientTrusted(final X509Certificate[] certificates, final String authType) {
            // Allow all
        }

        @Override
        public void checkServerTrusted(final X509Certificate[] certificates, final String authType) {
            // Allow all
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };

    public static List<ProductArtifact> toArtifacts(@Nullable final String val) {
        if (isBlank(val)) {
            return new ArrayList<>();
        }

        return stream(val.split(",")).map(artifact -> {
            final String[] items = artifact.split(":");
            if (items.length < 2 || items.length > 3) {
                throw new IllegalArgumentException("Invalid artifact pattern: " + artifact);
            }
            final String groupId = items[0].trim();
            final String artifactId = items[1].trim();
            final String version = items.length == 3 ? items[2].trim() : "LATEST";
            return new ProductArtifact(groupId, artifactId, version);
        }).collect(toList());
    }

    /**
     * Tests to check if a port is available. If a {@code bindAddress} is provided, the
     * selected port will be verified against that interface.
     *
     * @param requestedPort the port to check
     * @param bindAddress   the local address to bind to, which may be {@code null} to bind to any interface
     * @return A boolean indicating if the port is free
     * @since 8.0
     */
    public static boolean isPortFree(final int requestedPort, @Nullable final InetAddress bindAddress) {
        try (final ServerSocket ignored = new ServerSocket(requestedPort, 1, bindAddress)) {
            return true;
        } catch (final IOException e) {
            return false;
        }
    }

    /**
     * Tests to check if a port is available.
     *
     * @param requestedPort the port to check
     * @return A boolean indicating if the port is free
     * @since 8.0
     */
    public static boolean isPortFree(final int requestedPort) {
        return isPortFree(requestedPort, null);
    }

    /**
     * Picks a free port, preferring the requested port if it's available. If a {@code bindAddress} is provided, the
     * selected port will be verified against that interface.
     * <p>
     * If the requested port is {@code 0}, or the requested port is already in use, a {@code ServerSocket} is used
     * to select a random open port on the system. If an open port can be found, it is returned.
     *
     * @param requestedPort the preferred port, or {@code 0} to select a random port
     * @param bindAddress   the local address to bind to, which may be {@code null} to bind to any interface
     * @return the selected port
     * @since 6.3.4
     */
    public static int pickFreePort(final int requestedPort, @Nullable final InetAddress bindAddress) {
        try (final ServerSocket socket = new ServerSocket(requestedPort, 1, bindAddress)) {
            return requestedPort == 0 ? socket.getLocalPort() : requestedPort;
        } catch (final IOException e) {
            if (requestedPort == 0) {
                throw new IllegalStateException("Error opening socket", e);
            }
            return pickFreePort(0, bindAddress);
        }
    }

    /**
     * Picks a free port, preferring the requested port if it's available.
     * <p>
     * If the requested port is {@code 0}, or the requested port is already in use, a {@code ServerSocket} is used
     * to select a random open port on the system. If an open port can be found, it is returned.
     *
     * @param requestedPort the preferred port, or {@code 0} to select a random port
     * @return the selected port
     * @since 6.3
     */
    public static int pickFreePort(final int requestedPort) {
        return pickFreePort(requestedPort, null);
    }

    /**
     * Wait until the given node of the given product is up or stopped, depending on the {@code startingUp} parameter.
     *
     * @param product the product in question
     * @param node the node in question
     * @param startingUp true if the product is being started up; false if it's being shut down
     * @throws MojoExecutionException if the product didn't reach the desired state before the relevant timeout
     */
    public static void awaitStateChange(final Product product, final Node node, final boolean startingUp, final Log log)
            throws MojoExecutionException {
        final int port = node.getWebPort();
        if (port != 0) {
            final String startStop = startingUp ? "start" : "stop";
            final String url = product.getProtocol() + "://" + product.getServer() + ":" + port +
                    defaultString(product.getContextPath(), "");
            final int timeout = product.getTimeout(startingUp);
            final long end = System.nanoTime() + MILLISECONDS.toNanos(timeout);
            final URL urlToPing = toUrl(url);
            final Optional<SSLFactoryAndVerifier> previousHttpsConfig =
                    configureSslConnection(product, startStop, url, timeout);

            // keep hitting the URL until a good response is returned, within the timeout
            String lastMessage = "";
            boolean success = false;
            HttpURLConnection connection = null;
            while (System.nanoTime() < end) {
                try {
                    connection = (HttpURLConnection) urlToPing.openConnection();
                    int response = connection.getResponseCode();
                    // Tomcat returns 404 until the webapp is up
                    lastMessage = "Last response code is " + response;
                    success = startingUp == (response < 400);
                } catch (Exception e) {
                    lastMessage = e.getMessage();
                    success = !startingUp;
                }

                if (success || sleepInterrupted(url, log, startStop)) {
                    break;
                }
            }
            closeInputStream(connection);
            previousHttpsConfig.ifPresent(SSLFactoryAndVerifier::apply);
            if (!success) {
                throw new MojoExecutionException(String.format("The product %s didn't %s after %ds at %s. %s",
                        product.getInstanceId(), startStop, MILLISECONDS.toSeconds(timeout), url, lastMessage));
            }
        }
    }

    private static boolean sleepInterrupted(final String url, final Log log, final String startStop) {
        log.info("Waiting for " + url + " to " + startStop);
        try {
            SECONDS.sleep(1);
            // We slept without being interrupted
            return false;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return true;
        }
    }

    private static void closeInputStream(@Nullable final HttpURLConnection connection) {
        if (connection != null) {
            try {
                connection.getInputStream().close();
            } catch (final IOException ignored) {
                // Don't do anything
            }
        }
    }

    private static Optional<SSLFactoryAndVerifier> configureSslConnection(
            final Product product, final String startStop, final String url, final int timeout)
            throws MojoExecutionException {
        try {
            return configureConnection(product.getUseHttps());
        } catch (final NoSuchAlgorithmException | KeyManagementException e) {
            throw new MojoExecutionException(String.format("The product %s didn't %s after %ds at %s. %s",
                    product.getInstanceId(), startStop, MILLISECONDS.toSeconds(timeout), url, e.getMessage()));
        }
    }

    private static URL toUrl(final String url) throws MojoExecutionException {
        try {
            return new URL(url);
        } catch (final MalformedURLException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private static class SSLFactoryAndVerifier {

        private final HostnameVerifier verifier;
        private final SSLSocketFactory sslSocketFactory;

        public SSLFactoryAndVerifier(final HostnameVerifier verifier, final SSLSocketFactory factory) {
            this.verifier = requireNonNull(verifier);
            this.sslSocketFactory = requireNonNull(factory);
        }

        public void apply() {
            setDefaultSSLSocketFactory(sslSocketFactory);
            setDefaultHostnameVerifier(verifier);
        }
    }

    /**
     * If the product is using https, we configure the SSLSocketFactory and Verifier to accept all certificates without
     * throwing an error when we ping. We return the old configurations so we can reset once we're done.
     *
     * @param useHttps true if we are using https
     * @return an optional of the old configurations depending upon whether we use https or not.
     * @throws NoSuchAlgorithmException if {@code useHttps} is {@code true} and TLS is not supported
     * @throws KeyManagementException   if the SSL context can't be initialized
     */
    private static Optional<SSLFactoryAndVerifier> configureConnection(final boolean useHttps)
            throws NoSuchAlgorithmException, KeyManagementException {
        if (!useHttps) {
            return Optional.empty();
        }
        final HostnameVerifier previousHostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
        final SSLSocketFactory previousSslSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
        // Set the connections to accept any SSL certificate
        final SSLContext sslContext = SSLContext.getInstance("TLS");
        final TrustManager[] trustManagers = {PERMISSIVE_TRUST_MANAGER};
        sslContext.init(null, trustManagers, new SecureRandom());
        setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        setDefaultHostnameVerifier((s, sslSession) -> sslSession != null);
        return Optional.of(new SSLFactoryAndVerifier(previousHostnameVerifier, previousSslSocketFactory));
    }
}
