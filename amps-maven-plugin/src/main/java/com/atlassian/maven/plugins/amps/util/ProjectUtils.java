package com.atlassian.maven.plugins.amps.util;

import com.atlassian.maven.plugins.amps.MavenContext;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.project.MavenProject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.Objects;

import static com.atlassian.maven.plugins.amps.util.FileUtils.file;
import static java.util.Arrays.stream;

/**
 * Utility methods dealing with Maven projects.
 *
 * @since 3.3
 */
public final class ProjectUtils {

    /**
     * @return If the test jar should be built based on atlassian-plugin.xml residing in src/test/resources
     */
    public static boolean shouldDeployTestJar(MavenContext context) {
        File testResources = file(context.getProject().getBasedir(), "src", "test", "resources");
        File pluginXml = new File(testResources, "atlassian-plugin.xml");

        return pluginXml.exists();
    }

    /**
     * Returns the first non null value of the given array.
     *
     * @return the first non null value of values
     * @throws NullPointerException if all values are {@code null}
     */
    public static <T> T firstNotNull(T... values) {
        return stream(values)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(() -> new NullPointerException("All values are null"));
    }

    /**
     * Creates the given directory if it doesn't already exist.
     *
     * @param dir the directory to create
     * @return the given directory as an existing directory
     * @throws IllegalStateException if the directory didn't exist but couldn't be created
     */
    @Nonnull
    public static File createDirectory(@Nonnull final File dir) {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IllegalStateException("Failed to create directory " + dir.getAbsolutePath());
        }
        return dir;
    }

    /**
     * Attempts to retrieve a project artifact with the given group ID and artifact ID.
     *
     * @param context    the Maven context
     * @param groupId    the {@code groupId}
     * @param artifactId the {@code artifactId}
     * @return {@code null} if not found
     * @see MavenProject#getArtifacts()
     */
    @Nullable
    public static Artifact getReactorArtifact(final MavenContext context, final String groupId, final String artifactId) {
        return context.getProject().getArtifacts().stream()
                .filter(a -> a.getGroupId().equals(groupId))
                .filter(a -> a.getArtifactId().equals(artifactId))
                .findFirst()
                .orElse(null);
    }

    private ProjectUtils() {}
}
