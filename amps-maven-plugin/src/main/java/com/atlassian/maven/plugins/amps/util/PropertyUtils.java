package com.atlassian.maven.plugins.amps.util;

import org.apache.maven.plugin.MojoExecutionException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Optional;
import java.util.Properties;

import static java.util.Arrays.stream;
import static org.apache.commons.lang3.StringUtils.split;

/**
 * Utility methods relating to {@link Properties}.
 *
 * @since 6.2.9
 */
@ParametersAreNonnullByDefault
public final class PropertyUtils {

    private PropertyUtils() {
    }

    /**
     * Parses the given String of property keys and values into a {@link Properties} object.
     *
     * @param stringOfProperties the string to parse
     * @param keyValueDelimiter  the delimiter within each property's key-value string, e.g. "=" in "a=1"
     * @param propertyDelimiter  the delimiter between each property pair, e.g. "," in "a=1,b=2"
     * @return see above
     */
    @Nonnull
    public static Properties parse(
            @Nullable final String stringOfProperties, final char keyValueDelimiter, final char propertyDelimiter) {
        final Properties properties = new Properties();
        final String[] keyValuePairs = split(stringOfProperties, propertyDelimiter);
        if (keyValuePairs != null) {
            stream(keyValuePairs)
                    .map(pair -> split(pair, keyValueDelimiter))
                    .filter(parts -> parts.length == 2)
                    .forEach(keyAndValue ->
                            properties.put(keyAndValue[0], keyAndValue[1]));
        }
        return properties;
    }

    /**
     * Given a standard .properties file, extracts key-value pairs into a {@link Properties} object.
     * @param propertiesFile File containing key-value pairs.
     * @return see above
     * @throws MojoExecutionException if there's an I/O error.
     * @since 8.3.0
     */
    @Nonnull
    public static Properties load(final File propertiesFile) throws MojoExecutionException {
        try (InputStream config = new FileInputStream(propertiesFile)) {
            Properties properties = new Properties();
            properties.load(config);
            return properties;
        } catch (IOException e) {
            throw new MojoExecutionException("Could not load file " + propertiesFile, e);
        }
    }

    /**
     * Stores the given properties to the given file, overwriting it if it exists.
     *
     * @param properties the properties to store
     * @param file the destination file
     * @param message an optional comment to put at the top of the file
     * @throws MojoExecutionException if there's an I/O error
     * @see Properties#store(Writer, String)
     * @since 8.3
     */
    public static void storeProperties(final Properties properties, final File file, @Nullable final String message)
            throws MojoExecutionException {
        try (final Writer fileWriter = new FileWriter(file)) {
            properties.store(fileWriter, message);
        } catch (final IOException e) {
            throw new MojoExecutionException("Cannot write properties to " + file, e);
        }
    }
}
