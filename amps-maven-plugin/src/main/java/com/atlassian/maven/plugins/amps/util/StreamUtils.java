package com.atlassian.maven.plugins.amps.util;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Enumeration;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.Long.MAX_VALUE;
import static java.util.Objects.requireNonNull;

/**
 * Stream-related utility methods.
 *
 * @since 8.3
 */
@ParametersAreNonnullByDefault
public final class StreamUtils {

    private StreamUtils() {}

    /**
     * Converts the given enumeration to a stream.
     *
     * @param enumeration the enumeration
     * @param <T> the element type
     * @return the stream
     */
    public static <T> Stream<T> stream(@Nullable final Enumeration<T> enumeration) {
        if (enumeration == null) {
            return Stream.empty();
        }
        final EnumerationSpliterator<T> spliterator = new EnumerationSpliterator<>(enumeration);
        return StreamSupport.stream(spliterator, false);
    }

    private static class EnumerationSpliterator<T> extends Spliterators.AbstractSpliterator<T> {

        private final Enumeration<T> enumeration;

        private EnumerationSpliterator(final Enumeration<T> enumeration) {
            super(MAX_VALUE, ORDERED);
            this.enumeration = requireNonNull(enumeration);
        }

        @Override
        public boolean tryAdvance(final Consumer<? super T> action) {
            if (enumeration.hasMoreElements()) {
                action.accept(enumeration.nextElement());
                return true;
            }
            return false;
        }

        @Override
        public void forEachRemaining(final Consumer<? super T> action) {
            while (enumeration.hasMoreElements()) {
                action.accept(enumeration.nextElement());
            }
        }
    }
}
