package com.atlassian.maven.plugins.amps.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class VersionUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(VersionUtils.class);

    private static final String RESOURCE_NAME = "META-INF/maven/com.atlassian.maven.plugins/amps-maven-plugin/pom.properties";

    private VersionUtils() {
    }

    public static String getVersion() {
        final ClassLoader classLoader = VersionUtils.class.getClassLoader();

        try (InputStream in = classLoader.getResourceAsStream(RESOURCE_NAME)) {
            if (in != null) {
                final Properties props = new Properties();
                props.load(in);
                return props.getProperty("version");
            }
        } catch (final IOException e) {
            LOGGER.warn("Couldn't read version from {}", RESOURCE_NAME, e);
        }
        return "RELEASE";
    }
}
