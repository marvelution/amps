package com.atlassian.maven.plugins.amps.util;

/**
 * This class exists only so that the
 * {@link com.atlassian.maven.plugins.amps.analytics.visitordata.PreferencesVisitorDataDao}
 * uses the correct node in the {@link java.util.prefs.Preferences} tree for storing
 * {@link com.dmurph.tracking.VisitorData}.
 *
 * If you move it to a different package, you will invalidate all such data currently stored on AMPS user's machines
 * throughout the Atlassian P2 ecosystem.
 */
public final class VisitorDataDaoPreferencesNode {

    private VisitorDataDaoPreferencesNode() {
        throw new UnsupportedOperationException("Not for instantiation");
    }
}
