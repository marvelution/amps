package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.logging.Log;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    private static final String OTHER_SHARED_HOME = "otherSharedHome";
    private static final String THIS_SHARED_HOME = "thisSharedHome";
    private static final int OTHER_PORT = 667;
    private static final int THIS_PORT = 666;

    private Product thisProduct;

    @Mock
    private Product otherProduct;

    @Before
    public void setUp() {
        thisProduct = new Product();
    }

    @Test
    public void mergingShouldPreserveSharedHomeWhenItExists() {
        thisProduct.setSharedHome(THIS_SHARED_HOME);
        assertEquals(THIS_SHARED_HOME, thisProduct.merge(otherProduct).getSharedHome());
        verify(otherProduct, never()).getSharedHome();
    }

    @Test
    public void mergingShouldSetOtherSharedHomeWhenItDoesNotExist() {
        when(otherProduct.getSharedHome()).thenReturn(OTHER_SHARED_HOME);
        assertNull(thisProduct.getSharedHome());
        assertEquals(OTHER_SHARED_HOME, thisProduct.merge(otherProduct).getSharedHome());
    }

    @Test
    public void mergingShouldPreserveAjpPortWhenItExists() {
        thisProduct.setAjpPort(THIS_PORT);
        assertEquals(THIS_PORT, thisProduct.merge(otherProduct).getAjpPort());
        verify(otherProduct, never()).getAjpPort();
    }

    @Test
    public void mergingShouldSetOtherAjpPortWhenItDoesNotExist() {
        when(otherProduct.getAjpPort()).thenReturn(OTHER_PORT);
        assertEquals(0, thisProduct.getAjpPort());
        assertEquals(OTHER_PORT, thisProduct.merge(otherProduct).getAjpPort());
    }

    @Test
    public void mergingShouldPreserveRmiPortWhenItExists() {
        thisProduct.setRmiPort(THIS_PORT);
        assertEquals(THIS_PORT, thisProduct.merge(otherProduct).getRmiPort());
        verify(otherProduct, never()).getRmiPort();
    }

    @Test
    public void mergingShouldSetOtherRmiPortWhenItDoesNotExist() {
        when(otherProduct.getRmiPort()).thenReturn(OTHER_PORT);
        assertEquals(0, thisProduct.getRmiPort());
        assertEquals(OTHER_PORT, thisProduct.merge(otherProduct).getRmiPort());
    }

    @Test
    public void getBaseUrl_whenGivenNonZeroPort_shouldReturnExpectedValue() {
        // Arrange
        thisProduct.setContextPath("theContextPath");
        thisProduct.setServer("theServer");

        // Act
        final String baseUrl = thisProduct.getBaseUrlForPort(42);

        // Assert
        assertThat(baseUrl, is("http://theServer:42/theContextPath"));
    }

    @Test
    public void defaultJvmArgs_whenJvmArgsAlreadySet_shouldDoNothing() {
        // Arrange
        final String originalJvmArgs = "theOriginalJvmArgs";
        thisProduct.setJvmArgs(originalJvmArgs);

        // Act
        thisProduct.defaultJvmArgs("newJvmArgs");

        // Assert
        assertThat(thisProduct.getJvmArgs(), is(originalJvmArgs));
    }

    @Test
    public void defaultJvmArgs_whenJvmArgsNotAlreadySet_shouldSetGivenArgs() {
        // Arrange
        thisProduct.setJvmArgs(null);
        final String newJvmArgs = "theNewJvmArgs";

        // Act
        thisProduct.defaultJvmArgs(newJvmArgs);

        // Assert
        assertThat(thisProduct.getJvmArgs(), is(newJvmArgs));
    }

    @Test
    public void setNodeDebugArgs_whenNodesNotSet_shouldThrowException() {
        // Arrange
        final Log log = mock(Log.class);
        thisProduct.setInstanceId("foo");

        // Act
        final IllegalStateException exception = assertThrows(IllegalStateException.class,
                () -> thisProduct.setNodeDebugArgs(true, log));

        // Assert
        assertThat(exception.getMessage(), is("No nodes set for instance 'foo'"));
    }

    @Test
    public void setNodeDebugArgs_whenTheyAreBlank_shouldSetThemBasedOnPortAndSuspend() {
        // Arrange
        final Log log = mock(Log.class);
        final int debugPort = 42;
        thisProduct.initialiseNodes();
        final Node node = thisProduct.getNodes().get(0);
        node.setDebugArgs(null);
        node.setJvmDebugPort(debugPort);

        // Act
        thisProduct.setNodeDebugArgs(true, log);

        // Assert
        assertThat(node.getDebugArgs(),
                is(" -Xdebug -Xrunjdwp:transport=dt_socket,address=42,suspend=y,server=y "));
    }

    @Test
    public void setNodeDebugArgs_whenTheyAreNotBlank_shouldLeaveThemIntact() {
        // Arrange
        final Log log = mock(Log.class);
        final int debugPort = 43;
        thisProduct.initialiseNodes();
        final Node node = thisProduct.getNodes().get(0);
        final String userConfiguredDebugArgs = "theUserConfiguredDebugArgs";
        node.setDebugArgs(userConfiguredDebugArgs);
        node.setJvmDebugPort(debugPort);

        // Act
        thisProduct.setNodeDebugArgs(false, log);

        // Assert
        assertThat(node.getDebugArgs(), is(userConfiguredDebugArgs));
    }

    @Test
    public void defaultJvmArgs_whenPreviousJvmArgsAreNotBlank_shouldLeaveThemUnchanged() {
        final String previousJvmArgs = " previousJvmArgs ";
        assertDefaultJvmArgs(previousJvmArgs, previousJvmArgs);
    }

    @Test
    public void defaultJvmArgs_whenPreviousJvmArgsAreNull_shouldApplyTheDefault() {
        assertDefaultJvmArgsAppliesDefault(null);
    }

    @Test
    public void defaultJvmArgs_whenPreviousJvmArgsAreEmpty_shouldApplyTheDefault() {
        assertDefaultJvmArgsAppliesDefault("");
    }

    @Test
    public void defaultJvmArgs_whenPreviousJvmArgsAreBlank_shouldApplyTheDefault() {
        assertDefaultJvmArgsAppliesDefault(" ");
    }

    private void assertDefaultJvmArgsAppliesDefault(final String previousJvmArgs) {
        assertDefaultJvmArgs(previousJvmArgs, " defaultJvmArgs ");
    }

    private void assertDefaultJvmArgs(final String previousJvmArgs, final String expectedJvmArgs) {
        // Arrange
        thisProduct.setJvmArgs(previousJvmArgs);

        // Act
        thisProduct.defaultJvmArgs(" defaultJvmArgs ");

        // Assert
        assertThat(thisProduct.getJvmArgs(), is(expectedJvmArgs));
    }

    @Test
    public void getSystemPropertiesForNode_whenNoPropertiesExist_shouldReturnEmptyMap() {
        // Arrange
        final Product product = new Product();
        final Node node = mock(Node.class);
        when(node.getSystemProperties()).thenReturn(emptyMap());
        product.setNodes(singletonList(node));

        // Act
        final Map<String, String> systemProperties = product.getSystemPropertiesForNode(0);

        // Assert
        assertThat(systemProperties, is(emptyMap()));
    }

    @Test
    public void getSystemPropertiesForNode_whenNodeClashesWithProduct_shouldGivePrecedenceToNode() {
        // Arrange
        final Product product = new Product();
        final Node node = mock(Node.class);
        final String unconflictedProductKey = "k1";
        final String conflictedKey = "k2";
        final String unconflictedNodeKey = "k3";
        final String unconflictedProductValue = "v1";
        final String conflictedProductValue = "v2";
        final String unconflictedNodeValue = "v3";
        final String conflictedNodeValue = "v4";
        final Map<String, Object> productProperties = new HashMap<>();
        productProperties.put(unconflictedProductKey, unconflictedProductValue);
        productProperties.put(conflictedKey, conflictedProductValue);
        product.setSystemPropertyVariables(productProperties);
        final Map<String, String> nodeProperties = new HashMap<>();
        nodeProperties.put(conflictedKey, conflictedNodeValue);
        nodeProperties.put(unconflictedNodeKey, unconflictedNodeValue);
        when(node.getSystemProperties()).thenReturn(nodeProperties);
        product.setNodes(singletonList(node));

        // Act
        final Map<String, String> systemProperties = product.getSystemPropertiesForNode(0);

        // Assert
        assertThat(systemProperties.size(), is(3));
        assertThat(systemProperties, hasEntry(unconflictedProductKey, unconflictedProductValue));
        assertThat(systemProperties, hasEntry(unconflictedNodeKey, unconflictedNodeValue));
        assertThat(systemProperties, hasEntry(conflictedKey, conflictedNodeValue));
    }

    @Test
    public void getWebPortForNode_whenWebPortIsZero_shouldThrowException() {
        // Arrange
        final Product product = new Product();
        final Node node = mock(Node.class);
        product.setNodes(singletonList(node));
        when(node.getWebPort()).thenReturn(0);

        // Act
        final IllegalStateException exception =
                assertThrows(IllegalStateException.class, () -> product.getWebPortForNode(0));

        // Assert
        assertThat(exception.getMessage(), is("Web port not set or resolved for node 0"));
    }

    @Test
    public void getWebPortForNode_whenWebPortIsSet_shouldReturnIt() {
        // Arrange
        final Product product = new Product();
        final Node node = mock(Node.class);
        product.setNodes(singletonList(node));
        final int mockedWebPort = 42;
        when(node.getWebPort()).thenReturn(mockedWebPort);

        // Act
        final int actualWebPort = product.getWebPortForNode(0);

        // Assert
        assertThat(actualWebPort, is(mockedWebPort));
    }

    @Test
    public void getNodes_whenOneNodeExists_shouldEnsureAllNonDebugPortsOfThatNodeAreSet() {
        // Arrange
        final Product product = new Product();
        final String instanceId = "theInstanceId";
        product.setInstanceId(instanceId);
        final Node node = mock(Node.class);
        product.setNodes(singletonList(node));

        // Act
        product.initialiseNodes();

        // Assert
        verify(node).ensureNonDebugPortsAreSet(instanceId);
    }

    private Optional<String> getUserConfiguredLicense(final String license) {
        final Product product = new Product();
        product.setLicense(license);
        return product.getUserConfiguredLicense();
    }

    @Test
    public void getEffectiveLicense_whenNoLicenseDetailsSet_shouldReturnEmpty() {
        assertThat(getUserConfiguredLicense(null), is(empty()));
    }

    @Test
    public void getEffectiveLicense_whenOnlyLicenseStringSet_shouldReturnIt() {
        final String license = "theLicense";
        assertThat(getUserConfiguredLicense(license), is(Optional.of(license)));
    }

    @Test
    public void defaultSystemProperty_shouldUpdateUnsetProperty() {
        final Map<String, Object> productProperties = new HashMap<>();
        thisProduct.setSystemPropertyVariables(productProperties);
        thisProduct.defaultSystemProperty("testKey", () -> "someValue");

        assertThat(thisProduct.getSystemPropertyVariables(),
                hasEntry("testKey", "someValue"));
    }

    @Test
    public void defaultSystemProperty_shouldNotUpdateSetProperty() {
        final Map<String, Object> productProperties = new HashMap<>();
        productProperties.put("testKey", "testValue");
        thisProduct.setSystemPropertyVariables(productProperties);
        thisProduct.defaultSystemProperty("testKey", () -> "someOtherValue");

        assertThat(thisProduct.getSystemPropertyVariables(),
                hasEntry("testKey", "testValue"));
    }
}
