package com.atlassian.maven.plugins.amps;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.maven.plugins.amps.AbstractProductHandlerMojo.NO_TEST_GROUP;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.when;

public class TestIntegrationTestMojoGroupIncludes {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule().silent();

    private static final String TEST_GROUP_ONE_ID = "one";
    private static final String TEST_GROUP_TWO_ID = "two";
    private static final String CUSTOM_TEST_PATTERN = "SOME_RANDOM_TEST_PATTERN";
    private static final String ONE_TEST_INCLUSION_ELEMENT = "OneTest";

    @InjectMocks
    private IntegrationTestMojo integrationTestMojo;
    
    @Mock
    private TestGroup testGroupOne;

    @Mock
    private TestGroup testGroupTwo;

    @Before
    public void setUp() {
        setUpTestGroup(testGroupOne, TEST_GROUP_ONE_ID, singletonList(ONE_TEST_INCLUSION_ELEMENT));
        setUpTestGroup(testGroupTwo, TEST_GROUP_TWO_ID, emptyList());
        integrationTestMojo.functionalTestPattern = CUSTOM_TEST_PATTERN;
        integrationTestMojo.testGroups = asList(testGroupOne, testGroupTwo);
    }

    private static void setUpTestGroup(final TestGroup testGroup, final String id, final List<String> includes) {
        when(testGroup.getId()).thenReturn(id);
        when(testGroup.getIncludes()).thenReturn(includes);
    }

    @Test
    public void getIncludesForTestGroup_whenTestGroupIsNoTestGroup_shouldReturnConfiguredTestPattern() {
        assertIncludesForTestGroup(NO_TEST_GROUP, CUSTOM_TEST_PATTERN);
    }

    @Test
    public void getIncludesForTestGroup_whenItHasIncludes_shouldReturnThem() {
        assertIncludesForTestGroup(TEST_GROUP_ONE_ID, ONE_TEST_INCLUSION_ELEMENT);
    }

    @Test
    public void getIncludesForTestGroup_whenItHasNoIncludes_shouldReturnConfiguredTestPattern() {
        assertIncludesForTestGroup(TEST_GROUP_TWO_ID, CUSTOM_TEST_PATTERN);
    }

    @Test
    public void getIncludesForTestGroup_whenItDoesNotExist_shouldReturnConfiguredTestPattern() {
        assertIncludesForTestGroup("NoSuchId", CUSTOM_TEST_PATTERN);
    }

    private void assertIncludesForTestGroup(final String testGroupId, final String expectedInclude) {
        assertThat(integrationTestMojo.getIncludesForTestGroup(testGroupId), contains(expectedInclude));

    }
}
