package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.product.ProductHandler;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.CONFLUENCE;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.JIRA;
import static com.google.common.collect.Maps.immutableEntry;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The IntegrationTestMojo sets various execution-specific system properties.
 * They differ depending on whether we execute one or multiple products.
 * This class tests some of those variables, focusing on differences between these two execution modes.
 */
@SuppressWarnings("unchecked")
public class TestIntegrationTestMojoProperties {

    private static final int JIRA_PORT = 2990;

    private static final String CONFLUENCE_BASE_URL = "http://localhost:5990/confluence";
    private static final String JIRA_BASE_URL = "http://localhost:2990/jira";

    private static final BinaryOperator<Product> NO_MERGE = (p1, p2) -> {
        throw new UnsupportedOperationException();
    };

    private Product setUpProduct(
            final String productId, final String productVersion, final String instanceId, final String baseUrl,
            final String protocol, final int webPort, final String contextPath, final String homeDirectory) {
        final Product product = mock(Product.class, RETURNS_DEEP_STUBS);
        when(product.getBaseUrlForPort(webPort)).thenReturn(baseUrl);
        when(product.getContextPath()).thenReturn(contextPath);
        when(product.getId()).thenReturn(productId);
        when(product.getInstanceId()).thenReturn(instanceId);
        when(product.getProtocol()).thenReturn(protocol);
        when(product.getVersion()).thenReturn(productVersion);
        final Node node = mock(Node.class);
        when(node.getWebPort()).thenReturn(webPort);
        when(product.getNodes()).thenReturn(singletonList(node));
        when(productHandler.getHomeDirectories(product)).thenReturn(singletonList(new File(homeDirectory)));
        return product;
    }

    private Product setUpConfluence() {
        return setUpProduct(CONFLUENCE, "9.0.0", "confluence-amps",
                CONFLUENCE_BASE_URL, "https", 5990, "/confluence",
                "/confluence-home");
    }

    private Product setUpJira() {
        return setUpProduct(JIRA, "8.5.1", "jira-amps",
                JIRA_BASE_URL, "http", JIRA_PORT, "/jira",
                "/jira-home");
    }

    private IntegrationTestMojo integrationTestMojo;
    private ProductHandler productHandler;

    @Before
    public void setUp() {
        productHandler = mock(ProductHandler.class);
        integrationTestMojo = new IntegrationTestMojo() {
            @Override
            protected ProductHandler getProductHandler(final String productId) {
                return productHandler;
            }
        };
    }

    @Test
    public void shouldPopulateSystemPropertiesCorrectlyWhenOnlyOneProductIsRun() {
        assertPopulateProperties(
                singletonList(setUpJira()),
                // Expected properties
                getNonProductSpecificProperties(),
                getPropertiesThatShouldBeForSingleProductButAreNot(JIRA_BASE_URL, "/jira-home"),
                getJiraAmpsSpecificProperties(true));
    }

    @Test
    public void shouldPopulateSystemPropertiesCorrectlyWhenMultipleProductsAreRun() {
        assertPopulateProperties(
                asList(setUpConfluence(), setUpJira()),
                // Expected properties
                getConfluenceAmpsSpecificProperties(),
                getJiraAmpsSpecificProperties(false),
                getPropertiesThatShouldBeForSingleProductButAreNot(CONFLUENCE_BASE_URL, "/confluence-home"));
    }

    private void assertPopulateProperties(final List<Product> products,
                                          final Collection<Map.Entry<String, String>>... expectedProperties) {
        // Arrange
        final Map<Integer, Product> productsByWebPort = products.stream()
                .collect(toMap(TestIntegrationTestMojoProperties::getFirstWebPort, identity(), NO_MERGE,
                        LinkedHashMap::new));
        final Map<String, Object> systemProperties = new HashMap<>();

        // Act
        integrationTestMojo.populateProductProperties(
                systemProperties,
                productsByWebPort
        );

        // Assert
        assertThat(systemProperties.entrySet(), containsInAnyOrder(asMapEntries(expectedProperties)));
    }

    private static int getFirstWebPort(final Product product) {
        return product.getNodes().stream()
                .findFirst()
                .map(Node::getWebPort)
                .orElseThrow(IllegalStateException::new);
    }

    private Map.Entry<String, String>[] asMapEntries(final Collection<Map.Entry<String, String>>[] expectedPropertyMaps) {
        return Stream.of(expectedPropertyMaps)  // now we have a Stream<Stream<Entry>>
                .flatMap(Collection::stream)            // now we have a Stream<Entry>
                .toArray(Map.Entry[]::new);     // which we return as an Entry[]
    }

    private Collection<Map.Entry<String, String>> getJiraAmpsSpecificProperties(final boolean onlyProduct) {
        final Collection<Map.Entry<String, String>> properties = new ArrayList<>(asList(
                immutableEntry("baseurl.jira-amps", JIRA_BASE_URL),
                immutableEntry("homedir.jira-amps", "/jira-home"),
                immutableEntry("homedir.jira-amps.0", "/jira-home"),
                immutableEntry("context.jira-amps.path", "/jira"),
                immutableEntry("http.jira-amps.url", JIRA_BASE_URL),
                immutableEntry("http.jira-amps.port", "2990"),
                immutableEntry("http.jira-amps.protocol", "http"),
                immutableEntry("product.jira-amps.id", "jira"),
                immutableEntry("product.jira-amps.version", "8.5.1")
        ));
        if (onlyProduct) {
            // In the multi-product test, this entry is replaced by that for Confluence
            properties.add(immutableEntry("homedir.0", "/jira-home"));
        }
        return properties;
    }

    private Collection<Map.Entry<String, String>> getConfluenceAmpsSpecificProperties() {
        return asList(
                immutableEntry("baseurl.confluence-amps", CONFLUENCE_BASE_URL),
                immutableEntry("homedir.0", "/confluence-home"),
                immutableEntry("homedir.confluence-amps", "/confluence-home"),
                immutableEntry("homedir.confluence-amps.0", "/confluence-home"),
                immutableEntry("context.confluence-amps.path", "/confluence"),
                immutableEntry("http.confluence-amps.url", CONFLUENCE_BASE_URL),
                immutableEntry("http.confluence-amps.port", "5990"),
                immutableEntry("http.confluence-amps.protocol", "https"),
                immutableEntry("product.confluence-amps.id", "confluence"),
                immutableEntry("product.confluence-amps.version", "9.0.0")
        );
    }

    // It is quite strange that these properties are injected on a multi-product run
    private Collection<Map.Entry<String, String>> getPropertiesThatShouldBeForSingleProductButAreNot(
            final String baseurl, final String homedir) {
        return asList(
                immutableEntry("baseurl", baseurl),
                immutableEntry("homedir", homedir)
        );
    }

    private Collection<Map.Entry<String, String>> getNonProductSpecificProperties() {
        // These property names are not product-specific, is the point here, even though the values are for Jira
        return asList(
                immutableEntry("http.port", String.valueOf(JIRA_PORT)),
                immutableEntry("context.path", "/jira")
        );
    }
}
