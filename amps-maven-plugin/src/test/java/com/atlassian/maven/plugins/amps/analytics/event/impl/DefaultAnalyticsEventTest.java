package com.atlassian.maven.plugins.amps.analytics.event.impl;

import org.junit.Test;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.DefaultAnalyticsEvent.labelledEvent;
import static com.atlassian.maven.plugins.amps.analytics.event.impl.DefaultAnalyticsEvent.unlabelledEvent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DefaultAnalyticsEventTest {

    private static final String ACTION = "theAction";
    private static final String LABEL = "theLabel";

    @Test
    public void toString_whenLabelled_shouldIncludeLabel() {
        assertToString(labelledEvent(ACTION, LABEL), ACTION + " - " + LABEL);
    }

    @Test
    public void toString_whenUnlabelled_shouldNotIncludeLabel() {
        assertToString(unlabelledEvent(ACTION), ACTION);
    }

    private static void assertToString(final Object object, final String expectedToString) {
        assertThat(object.toString(), is(expectedToString));
    }
}
