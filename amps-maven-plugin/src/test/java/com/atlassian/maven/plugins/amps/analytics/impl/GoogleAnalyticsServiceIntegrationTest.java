package com.atlassian.maven.plugins.amps.analytics.impl;

import com.atlassian.maven.plugins.amps.analytics.AnalyticsService;
import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;
import com.atlassian.maven.plugins.amps.util.VisitorDataDaoPreferencesNode;
import org.apache.maven.plugin.logging.Log;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.prefs.Preferences;

import static com.atlassian.maven.plugins.amps.analytics.impl.GoogleAnalyticsService.newGoogleAnalyticsService;
import static java.util.prefs.Preferences.userNodeForPackage;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GoogleAnalyticsServiceIntegrationTest {

    // Deliberate copy of com.atlassian.maven.plugins.amps.analytics.visitordata.PreferencesVisitorDataDao.PREF_NAME
    private static final String PREF_NAME = "ga_visitor_data";

    private Preferences prefs;

    @Before
    public void setup() throws Exception {
        this.prefs = userNodeForPackage(VisitorDataDaoPreferencesNode.class);
        prefs.remove(PREF_NAME);
        prefs.flush();
    }

    private static AnalyticsEvent mockEventForAction(final String action) {
        final AnalyticsEvent event = mock(AnalyticsEvent.class);
        when(event.getAction()).thenReturn(action);
        return event;
    }

    @Test
    public void firstTrackCreatesNewVisitor() {
        // Arrange
        final Log log = mock(Log.class);
        final AnalyticsService tracker =
                newGoogleAnalyticsService("googleTrackingTester", "any", "any", log);
        final AnalyticsEvent initialEvent = mockEventForAction("testInitialTrack");

        // Act
        tracker.send(initialEvent);

        // Assert
        final String visitorData = prefs.get(PREF_NAME, null);
        assertNotNull("visitor data was not created!", visitorData);
        assertThat(visitorData, Matchers.endsWith(":1")); // visits = 1
    }

    @Test
    public void secondTrackCreatesNewSession() throws Exception {
        // Arrange
        final Log logger = mock(Log.class);
        final AnalyticsService firstTracker =
                newGoogleAnalyticsService("googleTrackingTester", "any", "any", logger);
        final AnalyticsEvent firstEvent = mockEventForAction("testFirstTrack");
        firstTracker.send(firstEvent);
        final String firstVisData = prefs.get(PREF_NAME, null);
        assertNotNull("first visitor data was not created", firstVisData);
        Thread.sleep(2000);  // makes sure system clock ticks over to a new second
        final AnalyticsService secondTracker =
                newGoogleAnalyticsService("googleTrackingTester", "any", "any", logger);
        final AnalyticsEvent secondEvent = mockEventForAction("testSecondTrack");

        // Act
        secondTracker.send(secondEvent);

        // Assert
        final String secondVisData = prefs.get(PREF_NAME, null);
        assertNotNull("second visitor data was not created", secondVisData);

        final String[] firstParts = firstVisData.split(":");
        final int firstVisId = Integer.parseInt(firstParts[0]);
        final long firstStartTS = Long.parseLong(firstParts[1]);
        final long firstPrevTS = Long.parseLong(firstParts[2]);
        final int firstVisits = Integer.parseInt(firstParts[3]);

        final String[] secondParts = secondVisData.split(":");
        final int secondVisId = Integer.parseInt(secondParts[0]);
        final long secondStartTS = Long.parseLong(secondParts[1]);
        final long secondPrevTS = Long.parseLong(secondParts[2]);
        final int secondVisits = Integer.parseInt(secondParts[3]);

        assertEquals("visitor ids do not match", firstVisId, secondVisId);
        assertEquals("second visits not ticked", firstVisits + 1, secondVisits);
        assertEquals("first timestamp not preserved", firstStartTS, secondStartTS);
        assertNotEquals("timestamps not updated", firstPrevTS, secondPrevTS);
    }
}
