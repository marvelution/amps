package com.atlassian.maven.plugins.amps.analytics.visitordata;

import com.dmurph.tracking.VisitorData;
import org.junit.Before;
import org.junit.Test;

import static java.lang.System.currentTimeMillis;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VisitorDataMarshallerTest {

    private VisitorDataMarshaller marshaller;

    @Before
    public void setUp() {
        marshaller = new VisitorDataMarshaller();
    }

    @Test
    public void marshalAndUnmarshal_whenRoundTripping_shouldMutateCorrectFields() {
        // Arrange
        final int visitorId = 666;
        final int visits = 42;
        final long current = 1234;
        final long first = 5678;
        final VisitorData visitorIn = mock(VisitorData.class);
        when(visitorIn.getVisitorId()).thenReturn(visitorId);
        when(visitorIn.getTimestampFirst()).thenReturn(first);
        when(visitorIn.getTimestampCurrent()).thenReturn(current);
        when(visitorIn.getVisits()).thenReturn(visits);
        final long nowInSeconds = currentTimeMillis() / 1000;

        // Act
        final VisitorData visitorOut = marshaller.unmarshalAndUpdate(marshaller.marshal(visitorIn));

        // Assert
        assertThat(visitorOut.getVisitorId(), is(visitorId));
        assertThat(visitorOut.getTimestampFirst(), is(first));
        assertThat(visitorOut.getTimestampPrevious(), is(current));
        assertThat((double) visitorOut.getTimestampCurrent(), closeTo(nowInSeconds, 1L));
        assertThat(visitorOut.getVisits(), is(visits + 1));
    }
}
