package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.junit.Rule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public abstract class DatabaseTestCase<T extends AbstractDatabase> {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    protected DataSource dataSource;

    @Mock
    protected Log log;

    protected abstract T getDatabase();

    protected final void assertDatabaseName(
            final String driverClass, final String url, final String expectedDatabaseName)
            throws MojoExecutionException {
        // Arrange
        when(dataSource.getDriver()).thenReturn(driverClass);
        when(dataSource.getUrl()).thenReturn(url);

        // Act
        final String databaseName = getDatabase().getDatabaseName(dataSource);

        // Assert
        assertThat(databaseName, is(expectedDatabaseName));
    }
}
