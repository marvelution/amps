package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.LibArtifact;
import org.apache.maven.plugin.logging.Log;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.jdbc.support.DatabaseMetaDataCallback;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DatabaseTypeFactoryTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DataSource dataSource;

    @Mock
    private Log log;

    @InjectMocks
    private DatabaseTypeFactory factory;

    @Test
    public void mssqlDatabaseFactory() {
        assertDatabaseClass("jdbc:sqlserver://host/eeee",
                "com.microsoft.sqlserver.jdbc.SQLServerDriver", AbstractMssqlDatabase.class);
    }

    @Test
    public void mssqlJtdsDatabaseFactory() {
        assertDatabaseClass("jdbc:jtds:sqlserver://localhost:1433/ppppp",
                "net.sourceforge.jtds.jdbc.Driver", AbstractMssqlDatabase.class);
    }

    @Test
    public void getDatabase_whenDriverIsDeprecatedMySQL_shouldReturnMySQLDatabase() {
        assertDatabaseClass("jdbc:mysql://localhost:3306/ffffff",
                "com.mysql.jdbc.Driver", MySQL.class);
    }

    @Test
    public void getDatabase_whenDriverIsMySQL_shouldReturnMySQLDatabase() {
        assertDatabaseClass("jdbc:mysql://localhost:3306/ffffff",
                "com.mysql.cj.jdbc.Driver", MySQL.class);
    }

    @Test
    public void oracle10gDatabaseFactory() {
        assertDatabaseClass("jdbc:oracle:thin:@localhost:1521:XE",
                "oracle.jdbc.OracleDriver", Oracle10g.class);
    }

    @Test
    public void postgresDatabaseFactory() {
        assertDatabaseClass("jdbc:postgresql://host/eeee",
                "org.postgresql.Driver", Postgres.class);
    }

    // Asserts that for a DataSource with the given URL and driver, the factory instantiates the given class.
    private void assertDatabaseClass(
            final String jdbcUrl, final String driverClass, final Class<? extends DatabaseType> expectedClass) {
        // Arrange
        when(dataSource.getUrl()).thenReturn(jdbcUrl);
        when(dataSource.getDriver()).thenReturn(driverClass);

        // Act
        final DatabaseType databaseType = factory.getDatabaseType(dataSource).orElseThrow(AssertionError::new);

        // Assert
        assertThat(databaseType, instanceOf(expectedClass));
    }

    private void assertDbType(@Nullable final String url, @Nullable final String driverClass,
                              @Nonnull final Class<? extends DatabaseType> expectedDbType) {
        // Set up
        final DataSource dataSource = mockDataSource(url, driverClass, null);

        // Invoke
        final Optional<DatabaseType> dbType = factory.getDatabaseType(dataSource);

        // Check
        final DatabaseType databaseType = dbType.orElseThrow(AssertionError::new);
        assertThat(databaseType, instanceOf(expectedDbType));
    }

    private void assertNoDbType(@Nullable final String url, @Nullable final String driverClass) {
        // Set up
        final DataSource dataSource = mockDataSource(url, driverClass, null);

        // Invoke
        final Optional<DatabaseType> dbType = factory.getDatabaseType(dataSource);

        // Check
        assertThat(dbType, is(empty()));
    }

    private DataSource mockDataSource(
            @Nullable final String url, @Nullable final String driverClass, @Nullable final String driverArtifactId) {
        final DataSource dataSource = mock(DataSource.class);
        when(dataSource.getDriver()).thenReturn(driverClass);
        when(dataSource.getUrl()).thenReturn(url);
        when(dataSource.getJdbcMetaData(any(DatabaseMetaDataCallback.class))).thenReturn(empty());
        if (driverArtifactId != null) {
            final LibArtifact driver = mock(LibArtifact.class);
            when(driver.getArtifactId()).thenReturn(driverArtifactId);
            when(dataSource.getLibArtifacts()).thenReturn(singletonList(driver));
        }
        return dataSource;
    }

    @Test
    public void shouldReturnEmptyForNullUrlAndDriver() {
        assertNoDbType(null, null);
    }

    @Test
    public void shouldRecognisePostgresUriAndDriver() {
        assertDbType("jdbc:postgresql://localhost:5432/amps-test", "org.postgresql.Driver",
                Postgres.class);
    }

    @Test
    public void shouldRecogniseSqlServerUriAndDriver() {
        assertDbType("jdbc:sqlserver://amps-test", "com.microsoft.sqlserver.jdbc.SQLServerDriver",
                MssqlMicrosoft.class);
    }

    @Test
    public void shouldRecogniseOracle10gUriAndDriver() {
        assertDbType("jdbc:oracle:thin:@localhost:1521:XE", "oracle.jdbc.OracleDriver",
                Oracle10g.class);
    }

    @Test
    public void shouldRecogniseOracle12gFromJdbcMetadata() {
        // Set up
        final DataSource dataSource = mockDataSource(
                "jdbc:oracle:thin:@localhost:1521:XE", "oracle.jdbc.OracleDriver", "ojdbc7");
        final String dummyDbVersion = Oracle12c.ORACLE_12C_VERSION_PREFIX + " some random suffix";
        when(dataSource.getJdbcMetaData(any(DatabaseMetaDataCallback.class))).thenReturn(Optional.of(dummyDbVersion));

        // Invoke
        final DatabaseType dbType = factory.getDatabaseType(dataSource).orElseThrow(AssertionError::new);

        // Check
        assertThat(dbType, instanceOf(Oracle12c.class));
    }

    @Test
    public void shouldReturnEmptyForMicrosoftStyleUriWithJtdsDriver() {
        assertNoDbType("jdbc:sqlserver://amps-test;user=MyUserName;password=*****;",
                "net.sourceforge.jtds.jdbc.Driver");
    }
}
