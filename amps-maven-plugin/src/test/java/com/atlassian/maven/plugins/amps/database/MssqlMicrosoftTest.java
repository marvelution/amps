package com.atlassian.maven.plugins.amps.database;

import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class MssqlMicrosoftTest extends DatabaseTestCase<MssqlMicrosoft> {

    private static final String MICROSOFT_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    @InjectMocks
    private MssqlMicrosoft database;

    @Override
    protected MssqlMicrosoft getDatabase() {
        return database;
    }

    @Test
    public void getDatabaseName_whenNoOptionsInMicrosoftUrl_shouldReturnCorrectDbName() throws Exception {
        assertDatabaseName(MICROSOFT_DRIVER, "jdbc:sqlserver://localhost:1433;databaseName=fff", "fff");
    }

    @Test
    public void getDatabaseName_whenOptionsInMicrosoftUrl_shouldReturnCorrectDbName() throws Exception {
        assertDatabaseName(MICROSOFT_DRIVER, "jdbc:sqlserver://127.0.0.1;databaseName=ggg;autoCommit=false", "ggg");
    }

    @Test
    public void testCreateDatabaseSql() throws Exception {
        // expected result
        final String expectedSQLGenerated = "USE [master]; \n"
                + "IF EXISTS(SELECT * FROM SYS.DATABASES WHERE name='productdb') \n"
                + "DROP DATABASE [productdb];\n"
                + "USE [master]; \n"
                + "IF EXISTS(SELECT * FROM SYS.SERVER_PRINCIPALS WHERE name = 'product_user') \n"
                + "DROP LOGIN product_user; \n"
                + "USE [master]; \n"
                + " CREATE DATABASE [productdb]; \n"
                + "USE [master]; \n"
                + " CREATE LOGIN product_user WITH PASSWORD = 'product_pwd'; \n"
                + "USE [productdb];\n"
                + "CREATE USER product_user FROM LOGIN product_user; \n"
                + "EXEC SP_ADDROLEMEMBER 'DB_OWNER', 'product_user'; \n"
                + "ALTER LOGIN product_user WITH DEFAULT_DATABASE = [productdb]; \n";
        // setup
        when(dataSource.getDriver()).thenReturn(MICROSOFT_DRIVER);
        when(dataSource.getPassword()).thenReturn("product_pwd");
        when(dataSource.getUsername()).thenReturn("product_user");
        when(dataSource.getSystemUrl()).thenReturn("theSystemUrl");
        when(dataSource.getSystemUsername()).thenReturn("theSystemUser");
        when(dataSource.getUrl()).thenReturn("jdbc:jtds:sqlserver://localhost:1433/productdb");

        // execute
        final Xpp3Dom sqlMavenCreateConfiguration = database.getSqlMavenCreateConfiguration(dataSource);

        // assert
        final String initDatabaseSQL = sqlMavenCreateConfiguration.getChild("sqlCommand").getValue();
        assertThat("Generated SQL should be: " + expectedSQLGenerated,
                initDatabaseSQL, is(expectedSQLGenerated));
    }
}
