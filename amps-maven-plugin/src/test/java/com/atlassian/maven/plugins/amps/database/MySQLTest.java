package com.atlassian.maven.plugins.amps.database;

import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class MySQLTest extends DatabaseTestCase<MySQL> {

    private static final String DUMP_FILE_PATH = "theDumpFilePath";
    private static final String JDBC_DRIVER = "theDriver";
    private static final String JDBC_URL = "theUrl";
    private static final String PASSWORD = "thePassword";
    private static final String USERNAME = "theUsername";
    private static final String MYSQL_JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";

    @InjectMocks
    private MySQL database;

    private void setUpDriver() {
        when(dataSource.getDriver()).thenReturn(JDBC_DRIVER);
    }

    private void setUpJdbcUrl() {
        when(dataSource.getUrl()).thenReturn(JDBC_URL);
    }

    private void setUpLogin() {
        when(dataSource.getUsername()).thenReturn(USERNAME);
        when(dataSource.getPassword()).thenReturn(PASSWORD);
    }

    @Test
    public void getDatabaseName_whenUrlContainsNoOption_shouldReturnCorrectName() throws Exception {
        assertDatabaseName(MYSQL_JDBC_DRIVER, "jdbc:mysql://localhost:3306/ddd", "ddd");
    }

    @Test
    public void getDatabaseName_whenUrlContainsOption_shouldReturnCorrectName() throws Exception {
        assertDatabaseName(MYSQL_JDBC_DRIVER, "jdbc:mysql://127.0.0.1:3306/eeee?profileSQL=true",
                "eeee");
    }

    @Test
    public void testCreateDatabaseSql() throws Exception {
        // expected result
        final String expectedSQLGenerated = "DROP DATABASE IF EXISTS `productdb`;\n"
                + "GRANT USAGE ON *.* TO `product_user`@localhost;\n"
                + "DROP USER `product_user`@localhost;\n"
                + "CREATE DATABASE `productdb` CHARACTER SET utf8 COLLATE utf8_bin;\n"
                + "CREATE USER `product_user`@localhost IDENTIFIED BY 'product_pwd';\n"
                + "GRANT ALL ON `productdb`.* TO `product_user`@localhost;\n";
        // setup
        when(dataSource.getDriver()).thenReturn("com.mysql.jdbc.Driver");
        when(dataSource.getPassword()).thenReturn("product_pwd");
        when(dataSource.getSystemUrl()).thenReturn("theSystemUrl");
        when(dataSource.getSystemUsername()).thenReturn("theSystemUser");
        when(dataSource.getUrl()).thenReturn("jdbc:mysql://localhost:3307/productdb");
        when(dataSource.getUsername()).thenReturn("product_user");

        // execute
        final String initDatabaseSQL = database.getSqlMavenCreateConfiguration(dataSource).getChild("sqlCommand").getValue();

        // assert
        assertThat(initDatabaseSQL, is(expectedSQLGenerated));
    }

    @Test
    public void testImportConfiguration() {
        // Arrange
        setUpDriver();
        setUpJdbcUrl();
        setUpLogin();
        when(dataSource.getDumpFilePath()).thenReturn(DUMP_FILE_PATH);

        // Act
        final Xpp3Dom importConfiguration = database.getSqlMavenFileImportConfiguration(dataSource);

        // Assert
        assertThat(importConfiguration.getChild("autocommit").getValue(), is("true"));
        assertThat(importConfiguration.getChild("driver").getValue(), is(JDBC_DRIVER));
        assertThat(importConfiguration.getChild("password").getValue(), is(PASSWORD));
        assertThat(importConfiguration.getChild("url").getValue(), is(JDBC_URL));
        assertThat(importConfiguration.getChild("username").getValue(), is(USERNAME));
        final Xpp3Dom srcFiles = importConfiguration.getChild("srcFiles");
        assertThat(srcFiles.getChildCount(), is(1));
        assertThat(srcFiles.getChild(0).getValue(), is(DUMP_FILE_PATH));
    }

    @Override
    protected MySQL getDatabase() {
        return database;
    }
}
