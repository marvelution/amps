package com.atlassian.maven.plugins.amps.database;

import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.List;
import java.util.Properties;

import static com.atlassian.maven.plugins.amps.database.Postgres.PGDBNAME;
import static com.atlassian.maven.plugins.amps.database.Postgres.PGHOST;
import static com.atlassian.maven.plugins.amps.database.Postgres.PGPORT;
import static com.atlassian.maven.plugins.amps.product.ImportMethod.IMPDP;
import static com.atlassian.maven.plugins.amps.product.ImportMethod.PSQL;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

public class PostgresTest extends DatabaseTestCase<Postgres> {

    private static final String DUMP_FILE_PATH = "theDumpFilePath";
    private static final String JDBC_DRIVER = "org.postgresql.Driver"; // real driver so it can be classloaded
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/theDb"; // real-looking Postgres URL
    private static final String PASSWORD = "thePassword";
    private static final String USERNAME = "theUsername";

    @InjectMocks
    private Postgres database;

    private void setUpDriver() {
        when(dataSource.getDriver()).thenReturn(JDBC_DRIVER);
    }

    private void setUpJdbcUrl() {
        when(dataSource.getUrl()).thenReturn(JDBC_URL);
    }

    private void setUpLogin() {
        when(dataSource.getUsername()).thenReturn(USERNAME);
        when(dataSource.getPassword()).thenReturn(PASSWORD);
    }

    @Test
    public void getDatabaseName_whenNoHostSpecified_shouldReturnCorrectValue() throws Exception {
        assertDatabaseName(JDBC_DRIVER, "jdbc:postgresql:ddd", "ddd");
    }

    @Test
    public void getDatabaseName_whenNoPortSpecified_shouldReturnCorrectValue() throws Exception {
        assertDatabaseName(JDBC_DRIVER, "jdbc:postgresql://host/eee", "eee");
    }

    @Test
    public void getDatabaseName_whenPortButNoOptionsSpecified_shouldReturnCorrectValue() throws Exception {
        assertDatabaseName(JDBC_DRIVER, "jdbc:postgresql://host:6969/fff", "fff");
    }

    @Test
    public void getDatabaseName_whenPortAndOptionsSpecified_shouldReturnCorrectValue() throws Exception {
        assertDatabaseName(JDBC_DRIVER, "jdbc:postgresql://localhost:6969/ttt?user=aaa&password=bbb&uuuuu=ppp", "ttt");
    }

    @Test
    public void testCreateDatabaseSql() throws Exception {
        // setup
        setUpDriver();
        setUpJdbcUrl();
        setUpLogin();
        when(dataSource.getSystemUrl()).thenReturn("theSystemUrl");
        when(dataSource.getSystemUsername()).thenReturn("theSystemUser");


        // execute
        final Xpp3Dom sqlMavenCreateConfiguration = database.getSqlMavenCreateConfiguration(dataSource);

        // assert
        final String initDatabaseSQL = sqlMavenCreateConfiguration.getChild("sqlCommand").getValue();
        final String expectedSQLGenerated = "DROP DATABASE IF EXISTS \"theDb\";"
                + "DROP USER IF EXISTS \"theUsername\";"
                + "CREATE DATABASE \"theDb\";"
                + "CREATE USER \"theUsername\" WITH PASSWORD 'thePassword' ;"
                + "ALTER ROLE \"theUsername\" superuser; "
                + "ALTER DATABASE \"theDb\" OWNER TO \"theUsername\";";
        assertThat(initDatabaseSQL, is(expectedSQLGenerated));
    }

    @Test
    public void testImportConfiguration() {
        // Arrange
        setUpDriver();
        setUpJdbcUrl();
        setUpLogin();
        when(dataSource.getDumpFilePath()).thenReturn(DUMP_FILE_PATH);

        // Act
        final Xpp3Dom importConfiguration = database.getSqlMavenFileImportConfiguration(dataSource);

        // Assert
        assertThat(importConfiguration.getChild("autocommit").getValue(), is("true"));
        assertThat(importConfiguration.getChild("driver").getValue(), is(JDBC_DRIVER));
        assertThat(importConfiguration.getChild("password").getValue(), is(PASSWORD));
        assertThat(importConfiguration.getChild("url").getValue(), is(JDBC_URL));
        assertThat(importConfiguration.getChild("username").getValue(), is(USERNAME));
        final Xpp3Dom srcFiles = importConfiguration.getChild("srcFiles");
        assertThat(srcFiles.getChildren().length, is(1));
        assertThat(srcFiles.getChild(0).getValue(), is(DUMP_FILE_PATH));
    }

    @Override
    protected Postgres getDatabase() {
        return database;
    }

    @Test
    public void parseUrl_whenItContainsNonDefaultValues_shouldCorrectlyParseThem() {
        // Arrange
        final String postgresUrl = "jdbc:postgresql://mybox:5433/theDb";

        // Act
        final Properties urlProperties = Postgres.parseURL(postgresUrl);

        // Assert
        assertThat(urlProperties, notNullValue());
        assertThat(urlProperties.getProperty(PGHOST), is("mybox"));
        assertThat(urlProperties.getProperty(PGPORT), is("5433"));
        assertThat(urlProperties.getProperty(PGDBNAME), is("theDb"));
    }

    @Test
    public void parseUrl_whenItContainsNoHostName_shouldUseDefaultHostNameAndPort() {
        // Arrange
        final String postgresUrl = "jdbc:postgresql:theDb";

        // Act
        final Properties urlProperties = Postgres.parseURL(postgresUrl);

        // Assert
        assertThat(urlProperties, notNullValue());
        assertThat(urlProperties.getProperty(PGHOST), is("localhost"));
        assertThat(urlProperties.getProperty(PGPORT), is("5432"));
        assertThat(urlProperties.getProperty(PGDBNAME), is("theDb"));
    }

    @Test
    public void getExecMavenToolImportConfiguration_whenNotUsingPsql_shouldReturnNull() throws Exception {
        // Arrange
        when(dataSource.getImportMethod()).thenReturn(IMPDP.getMethod());

        // Act
        final Xpp3Dom importConfiguration = database.getExecMavenToolImportConfiguration(dataSource);

        // Assert
        assertNull(importConfiguration);
    }

    @Test
    public void getExecMavenToolImportConfiguration_whenUsingPsql_shouldProvideRequiredConfiguration()
            throws Exception {
        // Arrange
        setUpDriver();
        final String dumpFilePath = "theDumpFilePath";
        when(dataSource.getDumpFilePath()).thenReturn(dumpFilePath);
        when(dataSource.getImportMethod()).thenReturn(PSQL.getMethod());
        when(dataSource.getUrl()).thenReturn("jdbc:postgresql://my-box:5433/targetDb");
        when(dataSource.getUsername()).thenReturn(USERNAME);

        // Act
        final Xpp3Dom importConfiguration = database.getExecMavenToolImportConfiguration(dataSource);

        // Assert
        assertNotNull(importConfiguration);
        assertThat(importConfiguration.getName(), is("configuration"));
        assertThat(importConfiguration.getChild("executable").getValue(), is("psql"));
        final List<String> arguments = stream(importConfiguration.getChild("arguments").getChildren("argument"))
                .map(Xpp3Dom::getValue)
                .collect(toList());
        assertThat(arguments, containsInAnyOrder(
                "-f" + dumpFilePath,
                "-U" + USERNAME,
                "-hmy-box",
                "targetDb"
        ));
    }
}
