package com.atlassian.maven.plugins.amps.i18n;

import org.junit.Test;

import java.io.File;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anEmptyMap;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class FindI18nUsageInJavascriptTest extends AbstractI18nScannerTestCase {

    @Test
    public void testFindSingleI18nUsageInJavascriptFile() throws Exception {
        copyTestResource("single.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(results, hasSize(1));
        assertThat(getProperties(results, "single.js.i18n.properties"), hasEntry("plugin.i18n.string", "1"));
    }

    @Test
    public void testFindSingleI18nKeyUsedMultipleTimesInJavascriptFile() throws Exception {
        copyTestResource("reused.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(results, hasSize(1));
        assertThat(getProperties(results, "reused.js.i18n.properties"), hasEntry("plugin.i18n.string", "3"));
    }

    @Test
    public void testFindMultipleI18nUsageInJavascriptFiles() throws Exception {
        copyTestResource("multi-01.js");
        copyTestResource("multi-02.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("multi-01.js.i18n.properties", "multi-02.js.i18n.properties"));
        assertThat(getProperties(results, "multi-01.js.i18n.properties"), allOf(
                hasEntry("plugin.i18n.multi.first", "1"),
                hasEntry("plugin.i18n.multi.second", "1"),
                hasEntry("plugin.i18n.multi.third", "1"))
        );
        assertThat(getProperties(results, "multi-02.js.i18n.properties"), allOf(
                hasEntry("plugin.i18n.multi.first", "1"),
                hasEntry("plugin.i18n.multi.fourth", "1"),
                hasEntry("plugin.i18n.multi.fifth", "1"),
                hasEntry("plugin.i18n.multi.sixth", "1"))
        );
    }

    @Test
    public void testWritesEmptyFileForJavascriptFileWithNoI18nUsage() throws Exception {
        copyTestResource("none.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("none.js.i18n.properties"));
        assertThat(getProperties(results, "none.js.i18n.properties"), is(anEmptyMap()));
    }

    @Test
    public void testFindsNoInvalidUsageInJavascriptFiles() throws Exception {
        copyTestResource( "invalid.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("invalid.js.i18n.properties"));
        assertThat(getProperties(results, "invalid.js.i18n.properties"), is(anEmptyMap()));
    }

    @Test
    public void testFindUsageAcrossNestedJavascriptFiles() throws Exception {
        newFolder("features", "first");
        newFolder("features", "second", "sub-feature");
        copyTestResource("single.js", "features/first/simple.js");
        copyTestResource("single.js", "features/second/sub-feature/simple.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getRelativePaths(results), containsInAnyOrder("features/first/simple.js.i18n.properties", "features/second/sub-feature/simple.js.i18n.properties"));
    }
}
