package com.atlassian.maven.plugins.amps.i18n;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anEmptyMap;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class FindI18nUsageInSoyTest extends AbstractI18nScannerTestCase {

    @Test
    public void testFindSingleI18nUsageInSoyFile() throws Exception {
        copyTestResource("single.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(results, hasSize(1));
        assertThat(getProperties(results, "single.soy.i18n.properties"), hasEntry("plugin.i18n.string", "1"));
    }

    @Test
    public void testFindSingleI18nKeyUsedMultipleTimesInSoyFile() throws Exception {
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/reused.soy", "reused.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(results, hasSize(1));
        assertThat(getProperties(results, "reused.soy.i18n.properties"), hasEntry("plugin.i18n.string", "3"));
    }

    @Test
    public void testFindMultipleI18nUsageInSoyFiles() throws Exception {
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/multi-01.soy", "multi-01.soy");
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/multi-02.soy", "multi-02.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("multi-01.soy.i18n.properties", "multi-02.soy.i18n.properties"));
        assertThat(getProperties(results, "multi-01.soy.i18n.properties"), allOf(
                hasEntry("plugin.i18n.multi.first", "1"),
                hasEntry("plugin.i18n.multi.second", "1"),
                hasEntry("plugin.i18n.multi.third", "1"))
        );
        assertThat(getProperties(results, "multi-02.soy.i18n.properties"), allOf(
                hasEntry("plugin.i18n.multi.first", "1"),
                hasEntry("plugin.i18n.multi.fourth", "1"),
                hasEntry("plugin.i18n.multi.fifth", "1"),
                hasEntry("plugin.i18n.multi.sixth", "1"))
        );
    }

    @Test
    public void testWritesEmptyFileForSoyFileWithNoI18nUsage() throws Exception {
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/none.soy", "none.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("none.soy.i18n.properties"));
        assertThat(getProperties(results, "none.soy.i18n.properties"), is(anEmptyMap()));
    }

    @Test
    public void testFindsNoInvalidUsageInSoyFiles() throws Exception {
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/invalid.soy", "invalid.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("invalid.soy.i18n.properties"));
        assertThat(getProperties(results, "invalid.soy.i18n.properties"), is(anEmptyMap()));
    }

    @Test
    public void testFindUsageAcrossNestedSoyFiles() throws Exception {
        newFolder("features", "first");
        newFolder("features", "second", "sub-feature");
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/single.soy", "features/first/simple.soy");
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/single.soy", "features/second/sub-feature/simple.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getRelativePaths(results), containsInAnyOrder("features/first/simple.soy.i18n.properties", "features/second/sub-feature/simple.soy.i18n.properties"));
    }

    @Test
    @Ignore("Needs to use the AST to solve these ones")
    public void testFindUsageWhenDevelopersDoAwkwardButSyntacticallyValidThings() throws Exception {
        copyTestResource("com/atlassian/maven/plugins/amps/i18n/valid-ast.soy", "valid-ast.soy");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getFilenames(results), containsInAnyOrder("valid-ast.soy.amps.properties"));
        assertThat(getProperties(results, "valid-ast.soy.i18n.properties"), allOf(
                hasEntry("plugin.i18n.ast.first", "2"),
                hasEntry("plugin.i18n.ast.second", "3"),
                hasEntry("plugin.i18n.ast.third", "1"),
                hasEntry("plugin.i18n.ast.fourth", "1"),
                hasEntry("plugin.i18n.ast.fifth", "1"),
                hasEntry("plugin.i18n.ast.string-concat", "1"),
                hasEntry("plugin.i18n.ast.dynamic-string-gen", "1")
        ));
    }
}
