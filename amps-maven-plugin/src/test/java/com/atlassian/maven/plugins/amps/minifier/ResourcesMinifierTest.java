package com.atlassian.maven.plugins.amps.minifier;

import org.apache.commons.compress.utils.Lists;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Suite.class)
@SuiteClasses({
        ResourcesMinifierTest.StrategyTest.class,
        ResourcesMinifierTest.DirectoryTest.class
})
public class ResourcesMinifierTest {

    abstract static class TestMinifierBase {
        @Rule
        public MockitoRule rule = MockitoJUnit.rule();
        @Rule
        public TemporaryFolder src = new TemporaryFolder();
        @Rule
        public TemporaryFolder dst = new TemporaryFolder();
        @Mock
        protected Log log;

        protected Resource getBaseResourceMock(final String srcDir) {
            Resource resource = mock(Resource.class);
            when(resource.getDirectory()).thenReturn(srcDir);
            when(resource.getIncludes()).thenReturn(Lists.newArrayList());
            when(resource.getExcludes()).thenReturn(Lists.newArrayList());
            return resource;
        }

        protected List<String> getFilesystemFor(final TemporaryFolder folder) throws IOException {
            return Files.walk(folder.getRoot().toPath())
                    .map(String::valueOf)
                    .sorted()
                    .collect(Collectors.toList());
        }
    }

    @RunWith(Parameterized.class)
    public static class StrategyTest extends TestMinifierBase {

        @Parameters
        public static Iterable<?> testData() {
            return Arrays.asList(new Object[][]{
                    // Test all file types; use Closure compiler
                    {
                            true, // Compress JS
                            true, // Compress CSS
                            true, // Use Closure compiler
                            true, // Closure Jsdoc Warnings Enabled

                            // Expected
                            asList(
                                    "dummy-empty-min.css",
                                    "dummy-empty-min.js",
                                    "dummy-min.css",
                                    "dummy-min.js",
                                    "dummy.xml",
                                    "dummy-empty.xml"),
                            // Unexpected
                            asList(
                                    "dummy-empty.css",
                                    "dummy-empty.js",
                                    "dummy.css",
                                    "dummy.js",
                                    "dummy-min.xml",
                                    "dummy-empty-min.xml"),
                    },
                    {
                            true,  // Compress JS
                            true,  // Compress CSS
                            true,  // Use Closure compiler
                            false, // Closure Jsdoc Warnings Disabled

                            // Expected
                            asList(
                                    "dummy-empty-min.css",
                                    "dummy-empty-min.js",
                                    "dummy-min.css",
                                    "dummy-min.js",
                                    "dummy.xml",
                                    "dummy-empty.xml"),
                            // Unexpected
                            asList(
                                    "dummy-empty.css",
                                    "dummy-empty.js",
                                    "dummy.css",
                                    "dummy.js",
                                    "dummy-min.xml",
                                    "dummy-empty-min.xml"),
                    },
                    // Minimize JS file type with YUI compressor; XML is always included
                    {
                            true,  // Compress JS
                            false, // Do not compress CSS
                            false, // Do not Use Closure compiler
                            false, // Closure Jsdoc Warnings Disabled

                            // Expected
                            asList(
                                    "dummy-empty-min.js",
                                    "dummy-min.js",
                                    "dummy.xml",
                                    "dummy-empty.xml"),
                            // Unexpected
                            asList(
                                    "dummy-empty.js",
                                    "dummy.js",
                                    "dummy-min.xml",
                                    "dummy-empty-min.xml"),
                    }
            });
        }

        private final boolean compressJs;
        private final boolean compressCss;
        private final boolean useClosureCompiler;
        private final List<String> expectedFiles;
        private final List<String> unexpectedFiles;
        private final boolean closureJsdocWarningsEnabled;

        public StrategyTest(boolean compressJs,
                            boolean compressCss,
                            boolean useClosureCompiler,
                            boolean closureJsdocWarningsEnabled,
                            List<String> expectedFiles,
                            List<String> unexpectedFiles) {
            this.compressJs = compressJs;
            this.compressCss = compressCss;
            this.useClosureCompiler = useClosureCompiler;
            this.closureJsdocWarningsEnabled = closureJsdocWarningsEnabled;
            this.expectedFiles = expectedFiles;
            this.unexpectedFiles = unexpectedFiles;
        }

        @Test
        public void testMinifyStrategy() throws MojoExecutionException, IOException {
            MinifierParameters params = new MinifierParameters(
                    this.compressJs,
                    this.compressCss,
                    this.useClosureCompiler,
                    this.closureJsdocWarningsEnabled,
                    UTF_8,
                    log,
                    new HashMap<>()
                    );

            String resourceDir = ResourcesMinifierTest.class.getResource(".").getPath();
            Resource resource = getBaseResourceMock(resourceDir);

            new ResourcesMinifier(params).minify(resource, dst.getRoot().toString());

            List<String> results = getFilesystemFor(dst);

            expectedFiles.forEach(filename -> assertThat(results, hasItems(endsWith(filename))));
            unexpectedFiles.forEach(filename -> assertThat(results, not(hasItems(endsWith(filename)))));
        }
    }

    public static class DirectoryTest extends TestMinifierBase {

        @Test
        public void testPreMinifiedFilesAreCopied() throws Exception {
            MinifierParameters params = new MinifierParameters(
                    true,   // compress JS
                    true,  // compress CSS
                    UTF_8,
                    log,
                    new HashMap<>(),
                    true);

            src.newFile("test" + "myapp1-min.css");
            src.newFile("test" + "myapp2.min.js");
            src.newFile("test.some.awkward.dots" + "myapp3-min.js");
            src.newFile("test.notminified" + "myapp-minnope.js");

            Resource resource = getBaseResourceMock(src.getRoot().getPath());
            new ResourcesMinifier(params).minify(resource, dst.getRoot().toString());

            List<String> results = getFilesystemFor(dst);
            assertThat(results, hasItems(
                    endsWith("myapp1-min.css"),
                    endsWith("myapp2-min.js"),
                    endsWith("myapp3-min.js"),
                    endsWith("myapp-minnope-min.js")
            ));
        }

        @Test
        public void testDirectoryStructurePreservedInOutput() throws Exception {
            MinifierParameters params = new MinifierParameters(
                    true,   // compress JS
                    true,  // compress CSS
                    UTF_8,
                    log,
                    new HashMap<>(),
                    true);

            File subdir1 = src.newFolder("subfolder");
            File subdir2 = src.newFolder("deep", "nested", "folder");

            new File(src.getRoot(), "main.js").createNewFile();
            new File(subdir1, "sub-feature.js").createNewFile();
            new File(subdir2, "deep-feature.js").createNewFile();

            Resource resource = getBaseResourceMock(src.getRoot().getPath());
            new ResourcesMinifier(params).minify(resource, dst.getRoot().toString());

            List<String> results = getFilesystemFor(dst);
            assertThat(results, hasItems(
                    endsWith("main-min.js"),
                    endsWith("subfolder/sub-feature-min.js"),
                    endsWith("deep/nested/folder/deep-feature-min.js")
            ));
        }
    }
}
