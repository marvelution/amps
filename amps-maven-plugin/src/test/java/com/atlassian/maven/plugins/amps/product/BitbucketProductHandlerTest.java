package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.atlassian.maven.plugins.amps.util.MavenProjectLoader;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.DependencyManagement;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.repository.RepositorySystem;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.maven.plugins.amps.product.BitbucketProductHandler.EMBEDDED_ELASTICSEARCH_HTTP_PORT;
import static com.atlassian.maven.plugins.amps.product.BitbucketProductHandler.EMBEDDED_ELASTICSEARCH_TCP_PORT;
import static com.atlassian.maven.plugins.amps.product.BitbucketProductHandler.HAZELCAST_GROUP;
import static com.atlassian.maven.plugins.amps.product.BitbucketProductHandler.HAZELCAST_PORT;
import static com.atlassian.maven.plugins.amps.product.BitbucketProductHandler.SSH_PORT;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.apache.commons.lang3.JavaVersion.JAVA_9;
import static org.apache.commons.lang3.SystemUtils.isJavaVersionAtLeast;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assume.assumeTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BitbucketProductHandlerTest {

    private static final String SEARCH_VERSION = "1.0.0";
    private static final String SEARCH_GROUP_ID = "com.atlassian.bitbucket.search";
    private static final String EMBEDDED_ARTIFACT_ID = "embedded-elasticsearch-plugin";

    private static final ProductArtifact embeddedBundle =
            new ProductArtifact(SEARCH_GROUP_ID, EMBEDDED_ARTIFACT_ID, SEARCH_VERSION);

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private RepositorySystem repositorySystem;

    @Mock
    private ArtifactResolver artifactResolver;

    @Mock
    private Log log;

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MavenGoals mavenGoals;

    @Mock
    private MavenProject mavenProject;

    @Mock
    private MavenProjectLoader mavenProjectLoader;

    @Mock
    private Product product;

    @Mock
    private ProjectBuilder projectBuilder;

    @Mock
    private WebAppManager webAppManager;

    @Mock
    private Build build;

    // Can't use @InjectMocks because of method calls on constructor args
    private BitbucketProductHandler bitbucketProductHandler;

    @Before
    public void setUp()  {
        when(mavenContext.getLog()).thenReturn(log);
        when(mavenContext.getProject()).thenReturn(mavenProject);
        when(mavenProject.getBuild()).thenReturn(build);
        bitbucketProductHandler = new BitbucketProductHandler(mavenContext, mavenGoals, repositorySystem,
                mavenProjectLoader, projectBuilder, artifactResolver, webAppManager);
    }

    private void setUpSearchDependency() throws Exception {
        // Mocks for the getting dependencies
        final MavenProject mavenProject = mock(MavenProject.class);
        final DependencyManagement dependencyManagement = mock(DependencyManagement.class);
        final Dependency searchDependency = mock(Dependency.class);

        when(mavenProjectLoader.loadMavenProject(any(), any(), any()))
                .thenReturn(Optional.of(mavenProject));
        when(mavenProject.getDependencyManagement()).thenReturn(dependencyManagement);
        when(dependencyManagement.getDependencies()).thenReturn(singletonList(searchDependency));

        // Mocks for dependency attributes
        when(searchDependency.getGroupId()).thenReturn(SEARCH_GROUP_ID);
        when(searchDependency.getVersion()).thenReturn(SEARCH_VERSION);

        final Artifact parentArtifact = mock(Artifact.class);
        when(repositorySystem.createProjectArtifact(anyString(), anyString(), anyString())).thenReturn(parentArtifact);
    }

    private void assertPluginAdded(final String bitbucketVersion, final boolean expectedResult) throws Exception {
        // Arrange
        when(product.getVersion()).thenReturn(bitbucketVersion);
        if (expectedResult) {
            setUpSearchDependency();
        }

        // Act
        final List<ProductArtifact> plugins = bitbucketProductHandler.getAdditionalPlugins(product);

        // Assert
        final boolean actualResult = plugins.stream().anyMatch(p -> p.equals(embeddedBundle));
        assertThat(actualResult, is(expectedResult));
    }

    @Test
    public void testGetAdditionalPluginsBeforeFirstSearchVersion() throws Exception {
        assertPluginAdded("4.5.0", false);
    }

    @Test
    public void testGetAdditionalPluginsAtFirstSearchVersion() throws Exception {
        assertPluginAdded("4.6.0", true);
    }

    @Test
    public void testGetAdditionalPluginsAtSearchEAPVersion() throws Exception {
        assertPluginAdded("4.6.0-search-eap1", true);
    }

    @Test
    public void testGetAdditionalPluginsAtSearchMilestoneVersion() throws Exception {
        assertPluginAdded("4.6.0-m1", true);
    }

    @Test
    public void testGetAdditionalPluginsAtSearchReleaseCandidateVersion() throws Exception {
        assertPluginAdded("4.6.0-rc1", true);
    }

    @Test
    public void testGetAdditionalPluginsAtSearchAlphaVersion() throws Exception {
        assertPluginAdded("4.6.0-a0", true);
    }

    @Test
    public void testGetAdditionalPluginsAfterFirstSearchVersion() throws Exception {
        assertPluginAdded("4.7.0", true);
    }

    @Test
    public void testGetSystemPropertiesSingleNode_doesNotDefaultSsh() throws Exception {
        setUpProductMockForGetSysProps();
        when(product.isMultiNode()).thenReturn(false);
        doReturn(mockNodes(1)).when(product).getNodes();

        bitbucketProductHandler.getSystemProperties(product, 0);
        Node node = product.getNodes().get(0);
        verify(node, never()).defaultSystemProperty(eq(SSH_PORT), any());
    }

    @Test
    public void testGetSystemPropertiesMultiNode_defaultsSsh() throws Exception {
        setUpProductMockForGetSysProps();
        when(product.isMultiNode()).thenReturn(true);
        doReturn(mockNodes(2)).when(product).getNodes();

        bitbucketProductHandler.getSystemProperties(product, 0);
        Node node = product.getNodes().get(0);
        verify(node).defaultSystemProperty(eq(SSH_PORT), any());
    }

    @Test
    public void testGetSystemPropertiesSingleNode_doesNotDefaultHazelcast() throws Exception {
        setUpProductMockForGetSysProps();
        when(product.isMultiNode()).thenReturn(false);
        doReturn(mockNodes(1)).when(product).getNodes();

        bitbucketProductHandler.getSystemProperties(product, 0);
        Node node = product.getNodes().get(0);
        verify(node, never()).defaultSystemProperty(eq(HAZELCAST_PORT), any());
        verify(product, never()).defaultSystemProperty(eq(HAZELCAST_GROUP), any());
    }

    @Test
    public void testGetSystemPropertiesMultiNode_defaultsHazelcast() throws Exception {
        setUpProductMockForGetSysProps();
        when(product.isMultiNode()).thenReturn(true);
        doReturn(mockNodes(2)).when(product).getNodes();

        bitbucketProductHandler.getSystemProperties(product, 0);
        Node node = product.getNodes().get(0);
        verify(node).defaultSystemProperty(eq(HAZELCAST_PORT), any());
        verify(product).defaultSystemProperty(eq(HAZELCAST_GROUP), any());
    }

    private List<Node> mockNodes(final int nodeCount) {
        return range(0, nodeCount)
                .mapToObj(i -> mockNode())
                .collect(toList());
    }

    private Node mockNode() {
        Node node = mock(Node.class);
        Map<String, String> nodeSystemProperties = new HashMap<>();
        nodeSystemProperties.put(EMBEDDED_ELASTICSEARCH_HTTP_PORT, "123");
        nodeSystemProperties.put(EMBEDDED_ELASTICSEARCH_TCP_PORT, "124");
        when(node.getSystemProperties()).thenReturn(nodeSystemProperties);
        return node;
    }

    private void setUpProductMockForGetSysProps() {
        when(product.getInstanceId()).thenReturn("theInstanceId");
        when(product.getBaseUrlForNode(anyInt())).thenReturn("http://localhost:123");
    }

    @Test
    public void testFixJvmArgs() {
        assumeTrue(isJavaVersionAtLeast(JAVA_9));
        bitbucketProductHandler.fixJvmArgs(product);

        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        verify(product).setJvmArgs(captor.capture());

        assertThat(captor.getValue(), allOf(
                containsString("-Xmx1g"),
                containsString("--add-opens")));
    }
}
