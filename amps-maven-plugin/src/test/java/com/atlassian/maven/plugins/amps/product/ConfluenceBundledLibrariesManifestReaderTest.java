package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.atlassian.maven.plugins.amps.product.ConfluenceBundledLibrariesManifestReader.BUNDLED_LIBS_METADATA_FILE_PATH;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.newOutputStream;
import static java.util.Collections.emptySet;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.joining;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ConfluenceBundledLibrariesManifestReaderTest {
    @Rule
    public final TemporaryFolder tempDir = new TemporaryFolder();

    private File warFile;

    private final ConfluenceBundledLibrariesManifestReader manifestReader = new ConfluenceBundledLibrariesManifestReader();

    @Before
    public void setUp() throws Exception {
        warFile = tempDir.newFile();
    }

    @Test
    public void testWarContainsNoManifest() throws Exception {
        createEmptyWar();

        final Optional<Set<ProductArtifact>> result = manifestReader.readBundledLibrariesManifest(warFile);

        assertThat(result, is(empty()));
    }

    @Test
    public void testWarContainEmptyManifest() throws Exception {
        createWarWithBundledLibrariesManifest();

        final Optional<Set<ProductArtifact>> result = manifestReader.readBundledLibrariesManifest(warFile);

        assertThat(result, is(Optional.of(emptySet())));
    }

    @Test
    public void testWarContainPopulatedManifest() throws Exception {
        createWarWithBundledLibrariesManifest(
                new ProductArtifact("groupX", "artifactX", "versionX"),
                new ProductArtifact("groupY", "artifactY", "versionY"));

        final Optional<Set<ProductArtifact>> result = manifestReader.readBundledLibrariesManifest(warFile);

        assertThat(result, is(Optional.of(ImmutableSet.of(
                new ProductArtifact("groupX", "artifactX", "versionX"),
                new ProductArtifact("groupY", "artifactY", "versionY")))));
    }

    private void createWarWithBundledLibrariesManifest(ProductArtifact... bundledLibraries) throws Exception {
        final String metadataFileContents = Arrays.stream(bundledLibraries)
                .map(lib -> String.format("%s:%s:%s", lib.getGroupId(), lib.getArtifactId(), lib.getVersion()))
                .collect(joining("\n"));

        try (ZipOutputStream zos = new ZipOutputStream(newOutputStream(warFile.toPath()))) {
            zos.putNextEntry(new ZipEntry(BUNDLED_LIBS_METADATA_FILE_PATH));
            zos.write(metadataFileContents.getBytes(UTF_8));
        }
    }

    private void createEmptyWar() throws Exception {
        new ZipOutputStream(newOutputStream(warFile.toPath())).close();
    }
}
