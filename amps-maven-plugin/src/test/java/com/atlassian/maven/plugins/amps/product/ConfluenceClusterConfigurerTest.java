package com.atlassian.maven.plugins.amps.product;

import org.junit.Test;
import org.xmlunit.diff.Diff;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import static java.lang.String.format;
import static java.nio.file.Files.createTempFile;
import static org.apache.commons.io.FileUtils.copyFile;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.xmlunit.builder.DiffBuilder.compare;

/**
 * Tests the {@link ConfluenceClusterConfigurer} with real files, because that's the only way to test that class.
 */
public class ConfluenceClusterConfigurerTest {

    private static File getTestFile(final String name) {
        final URL fileUrl = ConfluenceClusterConfigurerTest.class.getResource(name);
        assertNotNull(format("Could not find '%s' in this test's package", name), fileUrl);
        try {
            return new File(fileUrl.toURI());
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    private static void assertConfigurationChanges(final String fileIn, final String expectedFileOut) throws Exception {
        // Arrange
        final File fileToConfigure = getTestFile(fileIn);
        final File tempFile = createTempFile(null, null).toFile(); // don't modify the original
        copyFile(fileToConfigure, tempFile);
        final File sharedHome = mock(File.class);
        when(sharedHome.getAbsolutePath()).thenReturn("theSharedHome");

        // Act
        new ConfluenceClusterConfigurer().configure(tempFile, sharedHome, "lo0");

        // Assert
        final Diff xmlDiff = compare(getTestFile(expectedFileOut))
                .withTest(tempFile)
                .ignoreComments()
                .ignoreWhitespace()
                .build();
        assertFalse(xmlDiff.getDifferences().toString(), xmlDiff.hasDifferences());
    }

    @Test
    public void configure_whenNoClusterPropertiesExist_shouldAddThem() throws Exception {
        assertConfigurationChanges("empty-in.xml", "empty-out.xml");
    }

    @Test
    public void configure_whenMulticastDiscoveryConfigured_shouldChangeToTcpIp() throws Exception {
        assertConfigurationChanges("multicast-in.xml", "multicast-out.xml");
    }

    @Test
    public void configure_whenTcpIpDiscoveryConfigured_shouldOnlyAddSharedHome() throws Exception {
        assertConfigurationChanges("tcp_ip-in.xml", "tcp_ip-out.xml");
    }
}
