package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.product.ConfluenceEmbeddedDatabaseResolver.WarFileBundledLibrariesManifestReader;
import com.google.common.collect.ImmutableSet;
import org.apache.maven.plugin.logging.Log;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.File;
import java.util.Optional;

import static com.atlassian.maven.plugins.amps.product.ConfluenceEmbeddedDatabaseResolver.H2;
import static com.atlassian.maven.plugins.amps.product.ConfluenceEmbeddedDatabaseResolver.HSQLDB;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.mockito.Mockito.when;

public class ConfluenceEmbeddedDatabaseResolverTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder tempDir = new TemporaryFolder();

    @Mock
    private Log log;

    @Mock
    private Product product;

    @Mock
    private WarFileBundledLibrariesManifestReader bundledLibrariesManifestReader;

    private ConfluenceEmbeddedDatabaseResolver embeddedDatabaseResolver;

    private File warFile;

    @Before
    public void setup() throws Exception {
        embeddedDatabaseResolver = new ConfluenceEmbeddedDatabaseResolver(log, bundledLibrariesManifestReader);
        warFile = tempDir.newFile("warFile");
    }

    /**
     * Tests the case where the Confluence WAR contains no bundled-libs manifest file, in which case we assume both
     * H2 and HSQL are bundled in the WAR. This is the case for any version of Confluence older than 7.20.
     */
    @Test
    public void testNoBundledLibrariesManifestPresentInWarFile() throws Exception {
        when(bundledLibrariesManifestReader.readBundledLibrariesManifest(warFile))
                .thenReturn(Optional.empty());

        assertThat(embeddedDatabaseResolver.getExtraEmbeddedDatabaseDependencies(product, warFile),
                empty());
    }

    /**
     * Tests the case where the Confluence WAR does contain a bundled-libs manifest file, but the manifest
     * does not contain entries for H2 or HSQL, in which case we need to add extra container dependencies for them.
     * This is the case for Confluence 8.0 or older.
     */
    @Test
    public void testBundledLibrariesManifestIsPresentButContainsNoH2OrHsql() throws Exception {
        setupBundledLibrariesManifest(dependency("some", "other", "library"));

        assertThat(embeddedDatabaseResolver.getExtraEmbeddedDatabaseDependencies(product, warFile),
                containsInAnyOrder(H2, HSQLDB));
    }

    /**
     * Tests the case where the Confluence WAR does contain a bundled-libs manifest file, and that manifest
     * contains entries for H2 and HSQL, in which case we know H2 and HSQL are bundled in the WAR.
     * This is the case for Confluence 7.20 specifially.
     */
    @Test
    public void testBundledLibrariesManifestIsPresentAndContainsH2AndHsql() throws Exception {
        setupBundledLibrariesManifest(H2, HSQLDB);

        assertThat(embeddedDatabaseResolver.getExtraEmbeddedDatabaseDependencies(product, warFile),
                empty());
    }

    /**
     * Tests the case where the Confluence WAR does contain a bundled-libs manifest file, and that manifest
     * contains entries for H2 and HSQL, but the versions of those are different to the versions that we would otherwise
     * be adding as container dependencies. This tests the condition where AMPS would be adding a different (i.e. newer)
     * version of H2/AMPS than is bundled in the WAR.
     */
    @Test
    public void testBundledLibrariesManifestIsPresentAndContainsH2AndHsqlWithDifferentVersions() throws Exception {
        setupBundledLibrariesManifest(
                withVersion(H2, "some other h2"),
                withVersion(HSQLDB, "some other hsqql"));

        assertThat(embeddedDatabaseResolver.getExtraEmbeddedDatabaseDependencies(product, warFile),
                empty());
    }

    /**
     * Tests the case where the Confluence WAR does contain a bundled-libs manifest file, the manifest
     * does not contain entries for H2 or HSQL, but the AMPS config contains liArtifacts for both of them, in which
     * case we don't need to add extra container dependencies. This would happen if the plugin developer wanted to test
     * against a different H2/HSQL version that AMPS would otherwise automatically add.
     */
    @Test
    public void testNoExtraDependenciesWhenConfigContainsDifferentVersionsOfH2AndHsqlLibArtifacts() throws Exception {
        setupEmptyBundledLibrariesManifest();

        when(product.getLibArtifacts()).thenReturn(asList(
                withVersion(H2, "some other h2"),
                withVersion(HSQLDB, "some other hsqql")));

        assertThat(embeddedDatabaseResolver.getExtraEmbeddedDatabaseDependencies(product, warFile),
                empty());
    }

    /**
     * Tests the case where the Confluence WAR does contain a bundled-libs manifest file, the manifest
     * does not contain entries for H2 or HSQL, but the AMPS config contains liArtifacts for only H2, in which
     * case we need to add an extra container dependency for HSQL. This would happen if the plugin developer wanted to test
     * against a different H2 version that AMPS would otherwise automatically add.
     */
    @Test
    public void testExtraHsqlDependencyWhenConfigContainsH2LibArtifacts() throws Exception {
        setupEmptyBundledLibrariesManifest();

        when(product.getLibArtifacts()).thenReturn(singletonList(
                withVersion(H2, "some other h2")));

        assertThat(embeddedDatabaseResolver.getExtraEmbeddedDatabaseDependencies(product, warFile),
                contains(HSQLDB));
    }

    private void setupBundledLibrariesManifest(final ProductArtifact... bundledLibraries) throws Exception {
        when(bundledLibrariesManifestReader.readBundledLibrariesManifest(warFile))
                .thenReturn(Optional.of(ImmutableSet.copyOf(bundledLibraries)));
    }

    private void setupEmptyBundledLibrariesManifest() throws Exception {
        when(bundledLibrariesManifestReader.readBundledLibrariesManifest(warFile))
                .thenReturn(Optional.of(emptySet()));
    }

    private static ProductArtifact withVersion(ProductArtifact artifact, String version) {
        final ProductArtifact lib = new ProductArtifact();
        lib.setGroupId(artifact.getGroupId());
        lib.setArtifactId(artifact.getArtifactId());
        lib.setVersion(version);
        return lib;
    }

    private static ProductArtifact dependency(String group, String artifact, String version) {
        return new ProductArtifact(group, artifact, version);
    }
}
