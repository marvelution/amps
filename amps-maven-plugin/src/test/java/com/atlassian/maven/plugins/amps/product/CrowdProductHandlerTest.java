package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import org.apache.commons.lang3.SystemUtils;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.maven.plugins.amps.product.CrowdProductHandler.withLocalhostAsHostname;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("HttpUrlsUsage")
public class CrowdProductHandlerTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private RepositorySystem repositorySystem;
    @Mock
    private ArtifactResolver artifactResolver;
    @Mock
    private Build build;
    @Mock
    private Log log;
    @Mock
    private MavenContext mavenContext;
    @Mock
    private MavenGoals mavenGoals;
    @Mock
    private MavenProject project;
    @Mock
    private Product product;
    @Mock
    private WebAppManager webAppManager;

    // Can't use @InjectMocks because of method calls on constructor args
    private CrowdProductHandler crowdProductHandler;

    private File crowdHome;

    @Before
    public void setUp() throws Exception {
        when(mavenContext.getLog()).thenReturn(log);
        when(mavenContext.getProject()).thenReturn(project);
        crowdHome = temporaryFolder.newFolder("crowdhome");
        crowdProductHandler =
                new CrowdProductHandler(mavenContext, mavenGoals, repositorySystem, artifactResolver, webAppManager);
    }

    private int setUpProcessHomeDirectory() throws Exception {
        final File buildDirectory = temporaryFolder.newFolder();
        when(build.getDirectory()).thenReturn(buildDirectory.getAbsolutePath());
        final int nodeIndex = 0;
        when(product.getBaseUrlForNode(nodeIndex)).thenReturn("theBaseUrl");
        when(product.getDataHome()).thenReturn(crowdHome.getCanonicalPath());
        when(product.getInstanceId()).thenReturn("coolInstanceId");
        when(product.getServer()).thenReturn("localhost");
        when(project.getBuild()).thenReturn(build);
        final Node node = mock(Node.class);
        when(product.getNodes()).thenReturn(singletonList(node));
        return nodeIndex;
    }

    @Test
    public void crowdServerUriConvertedToUseLocalhost() throws URISyntaxException {
        String uri = "http://example.test:8080/prefix/crowd?query#fragment";
        assertEquals("http://localhost:8080/prefix/crowd?query#fragment", withLocalhostAsHostname(uri));
    }

    @Test
    public void crowdServerUriConvertedToUseLocalhostWithHttps() throws URISyntaxException {
        String uri = "https://example.test:8080/prefix/crowd?query#fragment";
        assertEquals("https://localhost:8080/prefix/crowd?query#fragment", withLocalhostAsHostname(uri));
    }

    @Test
    public void shouldReplaceDatabaseUriInSharedHomeConfig() throws Exception {
        final int nodeIndex = setUpProcessHomeDirectory();
        final File sharedHome = initializeSharedHome();
        copyInputStreamToFile(getCrowdConfigFile(), new File(sharedHome, "crowd.cfg.xml"));

        crowdProductHandler.processHomeDirectory(product, nodeIndex, crowdHome);

        assertThat(readFileToString(new File(sharedHome, "crowd.cfg.xml"), UTF_8),
                containsString(databasePathContains(crowdHome.getCanonicalPath())));
    }

    @Test
    public void shouldReplaceDatabaseUriInClassicHomeConfig() throws Exception {
        final int nodeIndex = setUpProcessHomeDirectory();
        copyInputStreamToFile(getCrowdConfigFile(), new File(crowdHome, "crowd.cfg.xml"));

        crowdProductHandler.processHomeDirectory(product, nodeIndex, crowdHome);

        assertThat(readFileToString(new File(crowdHome, "crowd.cfg.xml"), UTF_8),
                containsString(databasePathContains(crowdHome.getCanonicalPath())));
    }

    @Test
    public void shouldReplaceDatabaseUriInBothCrowdConfigs() throws Exception {
        // Arrange
        final int nodeIndex = setUpProcessHomeDirectory();
        final File sharedHome = initializeSharedHome();
        copyInputStreamToFile(getCrowdConfigFile(), new File(sharedHome, "crowd.cfg.xml"));
        copyInputStreamToFile(getCrowdConfigFile(), new File(crowdHome, "crowd.cfg.xml"));

        // Act
        crowdProductHandler.processHomeDirectory(product, nodeIndex, crowdHome);

        // Assert
        assertThat(readFileToString(new File(crowdHome, "crowd.cfg.xml"), UTF_8),
                containsString(databasePathContains(crowdHome.getCanonicalPath())));
        assertThat(readFileToString(new File(sharedHome, "crowd.cfg.xml"), UTF_8),
                containsString(databasePathContains(crowdHome.getCanonicalPath())));
    }

    @Nonnull
    private static InputStream getCrowdConfigFile() {
        return requireNonNull(CrowdProductHandlerTest.class.getResourceAsStream("crowd.cfg.xml"));
    }

    private File initializeSharedHome() {
        final File sharedHome = new File(crowdHome, "shared");
        final boolean sharedHomeSuccessfullyCreated = sharedHome.mkdir();
        if (!sharedHomeSuccessfullyCreated) {
            throw new RuntimeException("Could not create shared home at " + sharedHome.getAbsolutePath());
        }
        return sharedHome;
    }

    private String databasePathContains(String path) {
        if (SystemUtils.IS_OS_WINDOWS) {
            path = path.replace('\\', '/');
        }

        return "<property name=\"hibernate.connection.url\">" +
                "jdbc:hsqldb:" +
                path +
                "/database/defaultdb" +
                "</property";
    }

    @Test
    public void getExtraJarsToSkipWhenScanningForTldsAndWebFragments_shouldContainActivationJar() {
        // Invoke
        final Collection<String> extraJarsToSkip =
                crowdProductHandler.getExtraJarsToSkipWhenScanningForTldsAndWebFragments();

        // Check
        assertTrue(extraJarsToSkip.contains("mail-*.jar"));
    }

    @Test
    public void getProductSpecificSystemProperties_whenTwoNodes_shouldDefineSameSharedHome() {
        // Arrange
        final Node node0 = mock(Node.class);
        final Node node1 = mock(Node.class);
        when(product.getInstanceId()).thenReturn("theInstanceId");
        when(product.getNodes()).thenReturn(asList(node0, node1));
        when(project.getBuild()).thenReturn(build);
        when(build.getDirectory()).thenReturn("theBuildDirectory");

        // Act
        final Map<String, String> node0Properties =
                crowdProductHandler.getProductSpecificSystemProperties(product, 0);
        final Map<String, String> node1Properties =
                crowdProductHandler.getProductSpecificSystemProperties(product, 1);

        // Assert
        final String homeProperty = "crowd.home";
        final String home0 = node0Properties.get(homeProperty);
        final String home1 = node1Properties.get(homeProperty);
        assertThat(home0, is(not(blankOrNullString())));
        assertThat(home1, is(not(blankOrNullString())));
        assertThat(home1, is(not(home0)));

        final String sharedHomeProperty = "crowd.shared.home";
        final String sharedHome0 = node0Properties.get(sharedHomeProperty);
        final String sharedHome1 = node1Properties.get(sharedHomeProperty);
        assertThat(sharedHome1, is(not(blankOrNullString())));
        assertThat(sharedHome1, is(sharedHome0));
    }
}
