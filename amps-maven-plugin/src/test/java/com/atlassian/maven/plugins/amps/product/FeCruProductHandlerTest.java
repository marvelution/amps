package com.atlassian.maven.plugins.amps.product;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.junit.Test;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ParametersAreNonnullByDefault
public class FeCruProductHandlerTest {

    private static final String VALID_GROUP_ID = "com.atlassian.fecru";

    private static final String VALID_ARTIFACT_ID = "amps-fecru";

    private static ArtifactVersion mockVersion(@Nullable final String qualifier) {
        final ArtifactVersion version = mock(ArtifactVersion.class);
        when(version.getQualifier()).thenReturn(qualifier);
        return version;
    }

    private static Artifact mockArtifact(final String groupId, final String artifactId) {
        final Artifact artifact = mock(Artifact.class);
        when(artifact.getGroupId()).thenReturn(groupId);
        when(artifact.getArtifactId()).thenReturn(artifactId);
        return artifact;
    }

    private static void assertFecruRelease(
            final String groupId,
            final String artifactId,
            @Nullable final String qualifier,
            final boolean expectedResult) {
        // Set up
        final Artifact artifact = mockArtifact(groupId, artifactId);
        final ArtifactVersion version = mockVersion(qualifier);

        // Invoke
        final boolean isFecruRelease = FeCruProductHandler.isFecruRelease(version, artifact);

        // Check
        assertThat(isFecruRelease, is(expectedResult));
    }

    @Test
    public void isFecruRelease_whenGroupIdIsWrong_shouldReturnFalse() {
        assertFecruRelease("com.example", VALID_ARTIFACT_ID, null, false);
    }

    @Test
    public void isFecruRelease_whenArtifactIdIsWrong_shouldReturnFalse() {
        assertFecruRelease(VALID_GROUP_ID, "not-fecru", null, false);
    }

    @Test
    public void isFecruRelease_whenQualifierIsNotTimestamp_shouldReturnFalse() {
        assertFecruRelease(VALID_GROUP_ID, VALID_ARTIFACT_ID, "not-a-timestamp", false);
    }

    @Test
    public void isFecruRelease_whenQualifierIsTimestamp_shouldReturnTrue() {
        assertFecruRelease(VALID_GROUP_ID, VALID_ARTIFACT_ID, "20200709182129", true);
    }
}
