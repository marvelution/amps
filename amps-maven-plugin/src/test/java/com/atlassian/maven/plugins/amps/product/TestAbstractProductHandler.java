package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.util.ArtifactRetriever;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.apache.maven.artifact.Artifact.SCOPE_RUNTIME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestAbstractProductHandler {

    private final static String GROUP_ID = "com.atlassian.refapp";
    private final static String ARTIFACT_ID = "atlassian-refapp";

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private RepositorySystem mockRepositorySystem;
    @Mock
    private ArtifactResolver mockArtifactResolver;
    @Mock
    private ArtifactRetriever mockArtifactRetriever;
    @Mock
    private Log mockLog;
    @Mock
    private MavenContext mockMavenContext;
    @Mock
    private MavenGoals mockMavenGals;
    @Mock
    private MavenProject mockProject;
    @Mock
    private PluginProvider mockPluginProvider;
    @Mock
    private Product mockProduct;

    private AbstractProductHandler handlerUnderTest;

    @Before
    public void setup() throws Exception {
        when(mockMavenContext.getLog()).thenReturn(mockLog);
        when(mockMavenContext.getProject()).thenReturn(mockProject);
        when(mockProduct.getArtifactRetriever()).thenReturn(mockArtifactRetriever);
        when(mockArtifactRetriever.getLatestStableVersion(any(Artifact.class))).thenReturn("3.0.3");

        handlerUnderTest = new AbstractProductHandler(
                mockMavenContext, mockMavenGals, mockPluginProvider, mockRepositorySystem, mockArtifactResolver) {
            @Override
            @Nonnull
            protected File extractApplication(final Product product) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            @Nonnull
            protected List<Node> startProduct(
                    final Product product, final File productFile, final List<Map<String, String>> systemProperties) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            protected boolean supportsStaticPlugins() {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            @Nonnull
            protected File getBundledPluginPath(final Product product, final File productDir) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            @Nonnull
            protected Optional<File> getUserInstalledPluginsDirectory(
                    final Product product, final File webappDir, final File homeDir) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Nonnull
            @Override
            protected Map<String, String> getSystemProperties(final Product product, final int nodeIndex) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Nonnull
            @Override
            public ProductArtifact getArtifact() {
                return new ProductArtifact(GROUP_ID, ARTIFACT_ID);
            }

            @Nonnull
            @Override
            public Optional<ProductArtifact> getTestResourcesArtifact() {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            @Nonnull
            public String getId() {
                return "refapp";
            }

            @Override
            public void stop(@Nonnull final Product product) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public int getDefaultHttpPort() {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public int getDefaultHttpsPort() {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    @Test
    public void testNullVersions() throws Exception {
        assertVersion(null);
    }

    @Test
    public void testLatestVersions() throws Exception {
        assertVersion("LATEST");
    }

    @Test
    public void testReleaseVersions() throws Exception {
        assertVersion("RELEASE");
    }

    private void assertVersion(String version) throws MojoExecutionException {
        final Artifact artifact = createTestArtifact(version);
        when(mockRepositorySystem.createProjectArtifact(eq(GROUP_ID), eq(ARTIFACT_ID), anyString())).thenReturn(artifact);
        String containerId = handlerUnderTest.getDefaultContainerId(mockProduct);
        verify(mockArtifactRetriever).getLatestStableVersion(artifact);
        assertThat(containerId, is("tomcat8x"));
    }

    private Artifact createTestArtifact(final String version) {
        ArtifactHandler handler = new DefaultArtifactHandler();
        VersionRange versionRange;
        if (version == null) {
            versionRange = VersionRange.createFromVersion(Artifact.RELEASE_VERSION);
        } else {
            versionRange = VersionRange.createFromVersion(version);
        }
        return new DefaultArtifact(GROUP_ID, ARTIFACT_ID, versionRange, SCOPE_RUNTIME, "pom", null,
                handler, false);
    }
}
