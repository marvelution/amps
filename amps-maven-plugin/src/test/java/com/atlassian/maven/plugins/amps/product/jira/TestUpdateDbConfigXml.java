package com.atlassian.maven.plugins.amps.product.jira;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.database.DatabaseType;
import com.atlassian.maven.plugins.amps.product.JiraProductHandler;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.w3c.dom.Document;

import java.io.File;
import java.io.IOException;

import static com.atlassian.maven.plugins.amps.product.jira.utils.DocumentUtils.copySampleDbConfigTo;
import static com.atlassian.maven.plugins.amps.product.jira.utils.DocumentUtils.getDocumentFrom;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasXPath;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUpdateDbConfigXml {

    @ClassRule
    public static TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    private static File getSubdirectoryFromTempDir(final String subPath) {
        return new File(temporaryFolder.getRoot(), subPath);
    }

    private static File getConfigFile() {
        return getSubdirectoryFromTempDir("test/dbconfig.xml");
    }

    @Mock
    private RepositorySystem repositorySystem;

    @Mock
    private ArtifactResolver artifactResolver;

    @Mock
    private Log log;

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MavenGoals mavenGoals;

    @Mock
    private MavenProject mavenProject;

    @Mock
    private WebAppManager webAppManager;

    // Can't use @InjectMocks because of method calls on constructor args
    private JiraProductHandler jiraProductHandler;

    @Before
    public void setUp() throws IOException {
        when(mavenContext.getLog()).thenReturn(log);
        when(mavenContext.getProject()).thenReturn(mavenProject);
        jiraProductHandler = new JiraProductHandler(mavenContext, mavenGoals, repositorySystem, artifactResolver, webAppManager);
        deleteDirectory(getSubdirectoryFromTempDir("test"));
        getSubdirectoryFromTempDir("test").mkdir();
    }

    @AfterClass
    public static void tearDown() throws IOException {
        deleteDirectory(getSubdirectoryFromTempDir("test"));
    }

    @Test
    public void shouldModifyDatabaseTypeWhenTypeIsDifferent() throws Exception {
        copySampleDbConfigTo(getConfigFile());
        final String newDbType = "theNewDbType";
        final DatabaseType databaseType = mockDatabaseType(newDbType, false);

        JiraProductHandler.updateDbConfigXml(getSubdirectoryFromTempDir("test"), databaseType, "PUBLIC");

        final Document doc = getDocumentFrom(getConfigFile());
        assertThat(doc, hasXPath(
                "//jira-database-config/database-type",
                containsString(newDbType)
        ));
    }

    @Test
    public void shouldCreateSchemaWhenSchemaIsMissingAndDatabaseSupportsSchema() throws Exception {
        copySampleDbConfigTo(getConfigFile());
        final DatabaseType databaseType = mockDatabaseType("mssql", true);

        JiraProductHandler.updateDbConfigXml(getSubdirectoryFromTempDir("test"), databaseType, "PUBLIC");

        final Document doc = getDocumentFrom(getConfigFile());
        assertThat(doc, hasXPath(
                "//jira-database-config/schema-name",
                containsString("PUBLIC")
        ));
    }

    private DatabaseType mockDatabaseType(final String ofBizName, final boolean hasSchema) {
        final DatabaseType databaseType = mock(DatabaseType.class);
        when(databaseType.getOfBizName()).thenReturn(ofBizName);
        when(databaseType.hasSchema()).thenReturn(hasSchema);
        return databaseType;
    }
}
