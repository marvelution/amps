package com.atlassian.maven.plugins.amps.product.jira.config;

import com.atlassian.maven.plugins.amps.database.DatabaseType;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DatabaseTypeUpdaterTransformerTest {

    private Document document;

    @Before
    public void setUp() {
        DocumentFactory documentFactory = DocumentFactory.getInstance();
        document = documentFactory.createDocument(documentFactory.createElement("jira-database-config"));
    }

    @Test
    public void shouldNotModifyTypeWhenSameTypeIsPresent() {
        final String dbType = "postgres72";
        final DatabaseType databaseType = mockDatabaseType(dbType);
        document.getRootElement().addElement("database-type").setText(dbType);
        DatabaseTypeUpdaterTransformer databaseTypeUpdaterTransformer = new DatabaseTypeUpdaterTransformer(databaseType);

        final boolean transformed = databaseTypeUpdaterTransformer.transform(document);

        assertThat(transformed, is(false));
        assertThat(document.selectSingleNode("/jira-database-config/database-type"), is(not(nullValue())));
        assertThat(document.selectSingleNode("/jira-database-config/database-type").getText(), is(dbType));
    }

    private static DatabaseType mockDatabaseType(final String ofBizName) {
        final DatabaseType databaseType = mock(DatabaseType.class);
        when(databaseType.getOfBizName()).thenReturn(ofBizName);
        return databaseType;
    }

    @Test
    public void shouldModifyTypeWhenDifferentTypeIsPresent() {
        // Arrange
        final String oldDbType = "theDbType";
        final String newDbType = oldDbType + "x";
        final DatabaseType databaseType = mockDatabaseType(newDbType);
        document.getRootElement().addElement("database-type").setText(oldDbType);
        final DatabaseTypeUpdaterTransformer databaseTypeUpdaterTransformer = new DatabaseTypeUpdaterTransformer(databaseType);

        // Act
        final boolean transformed = databaseTypeUpdaterTransformer.transform(document);

        // Assert
        assertThat(transformed, is(true));
        assertThat(document.selectSingleNode("/jira-database-config/database-type"), is(not(nullValue())));
        assertThat(document.selectSingleNode("/jira-database-config/database-type").getText(), is(newDbType));
    }

    @Test
    public void shouldNotCreateTypeWhenMissing() {
        // Arrange
        final DatabaseType databaseType = mockDatabaseType("postgres72");
        final DatabaseTypeUpdaterTransformer databaseTypeUpdaterTransformer = new DatabaseTypeUpdaterTransformer(databaseType);

        // Act
        final boolean transformed = databaseTypeUpdaterTransformer.transform(document);

        // Assert
        assertThat(transformed, is(false));
        assertThat(document.selectSingleNode("/jira-database-config/database-type"), is(nullValue()));
    }
}
