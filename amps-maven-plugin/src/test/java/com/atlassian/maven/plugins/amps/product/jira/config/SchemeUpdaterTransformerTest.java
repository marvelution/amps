package com.atlassian.maven.plugins.amps.product.jira.config;

import com.atlassian.maven.plugins.amps.database.DatabaseType;
import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SchemeUpdaterTransformerTest {

    private final String PUBLIC_SCHEMA = "PUBLIC";

    private Document document;

    @Before
    public void setUp() {
        DocumentFactory documentFactory = DocumentFactory.getInstance();
        document = documentFactory.createDocument(documentFactory.createElement("jira-database-config"));
    }

    @Test
    public void shouldRemoveSchemaWhenPresentSchemaAndDatabaseDoesNotSupportSchema() throws MojoExecutionException {
        document.getRootElement().addElement("schema-name").setText("PUBLIC");
        final DatabaseType databaseType = mockDatabaseType(false);
        SchemeUpdaterTransformer schemeUpdaterTransformer = new SchemeUpdaterTransformer(databaseType, null);

        boolean transformed = schemeUpdaterTransformer.transform(document);

        assertThat(transformed, is(true));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name"), is(nullValue()));
    }

    private static DatabaseType mockDatabaseType(final boolean hasSchema) {
        final DatabaseType databaseType = mock(DatabaseType.class);
        when(databaseType.hasSchema()).thenReturn(hasSchema);
        return databaseType;
    }

    @Test
    public void shouldNotCreateSchemaWhenMissingSchemaAndDatabaseDoesNotSupportSchema() throws MojoExecutionException {
        final DatabaseType databaseType = mockDatabaseType(false);

        final SchemeUpdaterTransformer schemeUpdaterTransformer = new SchemeUpdaterTransformer(databaseType, null);

        boolean transformed = schemeUpdaterTransformer.transform(document);

        assertThat(transformed, is(false));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name"), is(nullValue()));
    }

    @Test
    public void shouldCreateSchemaWhenMissingAndDatabaseSupportsSchema() throws MojoExecutionException {
        // Arrange
        final DatabaseType databaseType = mockDatabaseType(true);
        final SchemeUpdaterTransformer schemeUpdaterTransformer = new SchemeUpdaterTransformer(databaseType, PUBLIC_SCHEMA);

        // Act
        final boolean transformed = schemeUpdaterTransformer.transform(document);

        // Assert
        assertThat(transformed, is(true));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name").getText(), is(equalTo(PUBLIC_SCHEMA)));
    }

    @Test
    public void shouldChangeSchemaWhenDifferentSchemaAndDatabaseSupportsSchema() throws MojoExecutionException {
        // Arrange
        final DatabaseType databaseType = mockDatabaseType(true);
        document.getRootElement().addElement("schema-name").setText("PRIVATE");
        SchemeUpdaterTransformer schemeUpdaterTransformer = new SchemeUpdaterTransformer(databaseType, PUBLIC_SCHEMA);

        // Act
        final boolean transformed = schemeUpdaterTransformer.transform(document);

        // Assert
        assertThat(transformed, is(true));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name"), is(not(nullValue())));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name").getText(), is(equalTo(PUBLIC_SCHEMA)));
    }

    @Test
    public void shouldNotChangeSchemaWhenSameSchemaAndDatabaseSupportsSchema() throws MojoExecutionException {
        // Arrange
        final DatabaseType databaseType = mockDatabaseType(true);
        document.getRootElement().addElement("schema-name").setText(PUBLIC_SCHEMA);
        SchemeUpdaterTransformer schemeUpdaterTransformer = new SchemeUpdaterTransformer(databaseType, PUBLIC_SCHEMA);

        // Act
        final boolean transformed = schemeUpdaterTransformer.transform(document);

        // Assert
        assertThat(transformed, is(false));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name"), is(not(nullValue())));
        assertThat(document.selectSingleNode("/jira-database-config/schema-name").getText(), is(equalTo(PUBLIC_SCHEMA)));
    }
}
