package com.atlassian.maven.plugins.amps.product.manager;

import com.atlassian.maven.plugins.amps.Product;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class ContainerConfigTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Container container;

    @Mock
    private Product product;

    @Test
    public void getLogFile_whenNodeIsZeroAndProductOutputIsNull_shouldReturnNull() {
        assertLogFile(null, 0, null);
    }

    @Test
    public void getLogFile_whenNodeNotZeroAndProductOutputIsNull_shouldReturnNull() {
        assertLogFile(null, 1, null);
    }

    @Test
    public void getLogFile_whenNodeIsZeroAndProductOutputNotNull_shouldReturnProductOutput() {
        final String productOutput = "theProductOutput";
        assertLogFile(productOutput, 0, productOutput);
    }

    @Test
    public void getLogFile_whenNodeNotZeroAndProductOutputNotNull_shouldReturnSuffixedProductOutput() {
        assertLogFile("theProductOutput", 1, "theProductOutput-1");
    }

    private void assertLogFile(final String productOutput, final int nodeIndex, final String expectedLogFile) {
        // Arrange
        when(product.getOutput()).thenReturn(productOutput);
        final ContainerConfig containerConfig = new ContainerConfig(container, product, nodeIndex);

        // Act
        final String logFile = containerConfig.getLogFile();

        // Assert
        assertThat(logFile, is(expectedLogFile));
    }
}
