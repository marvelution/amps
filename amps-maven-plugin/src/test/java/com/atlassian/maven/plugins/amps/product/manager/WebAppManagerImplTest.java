package com.atlassian.maven.plugins.amps.product.manager;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.util.MojoExecutorWrapper;
import org.apache.maven.model.Plugin;
import org.codehaus.plexus.logging.Logger;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.maven.plugins.amps.product.manager.WebAppManagerImpl.AJP_PORT_PROPERTY;
import static com.atlassian.maven.plugins.amps.util.ProductHandlerUtil.pickFreePort;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WebAppManagerImplTest {

    private static final int AJP_PORT = 12345;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Logger logger;

    @Mock
    private MojoExecutor.ExecutionEnvironment executionEnvironment;

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MojoExecutorWrapper mojoExecutorWrapper;

    @Mock
    private Node node;

    @Mock
    private Product product;

    @InjectMocks
    private WebAppManagerImpl webAppManager;

    @Before
    public void setUpLogger() {
        webAppManager.enableLogging(logger);
    }

    private void setUpCargoPlugin() {
        setUpMavenPlugin("org.codehaus.cargo", "cargo-maven2-plugin");
    }

    private Node setUpSingleNode() {
        final Node node = mock(Node.class);
        when(product.getNodes()).thenReturn(singletonList(node));
        return node;
    }

    @Test
    public void startWebapp_whenAllPortsAreAvailable_shouldStartWebapp() throws Exception {
        final File war = mock(File.class);
        when(war.getPath()).thenReturn("/");

        when(mavenContext.getExecutionEnvironment()).thenReturn(executionEnvironment);
        when(product.getContainerId()).thenReturn("tomcat8x");
        when(product.getServer()).thenReturn("server");
        when(product.getContextPath()).thenReturn("/context");
        when(product.getProtocol()).thenReturn("http");

        final int freeHttpPort = pickFreePort(0);
        setUpDependencyPlugin();
        setUpCargoPlugin();
        final Node node = setUpSingleNode();
        when(node.getWebPort()).thenReturn(freeHttpPort);

        final List<Node> nodes = webAppManager.startWebapp(
                war, emptySystemProperties(), new ArrayList<>(), new ArrayList<>(), product, mavenContext);
        assertThat(nodes, hasSize(1));
    }

    private List<Map<String, String>> emptySystemProperties() {
        return singletonList(emptyMap());
    }

    private void setUpDependencyPlugin() {
        setUpMavenPlugin("org.apache.maven.plugins", "maven-dependency-plugin");
    }

    @Test
    public void startWebapp_shouldUseHttpPortsFromNode() throws Exception {
        final File war = mock(File.class);
        when(war.getPath()).thenReturn("/");

        when(mavenContext.getExecutionEnvironment()).thenReturn(executionEnvironment);
        when(product.getContainerId()).thenReturn("tomcat8x");
        when(product.getServer()).thenReturn("server");
        when(product.getContextPath()).thenReturn("/context");
        when(product.getProtocol()).thenReturn("http");
        setUpDependencyPlugin();
        setUpCargoPlugin();
        final Node node = setUpSingleNode();

        webAppManager.startWebapp(
                war, emptySystemProperties(), new ArrayList<>(), new ArrayList<>(), product, mavenContext);

        verify(node, atLeastOnce()).getAjpPort();
        verify(node, atLeastOnce()).getRmiPort();
        verify(node, atLeastOnce()).getWebPort();
    }

    @Test
    public void startWebapp_shouldUseHttpsPortsFromNode() throws Exception {
        final File war = mock(File.class);
        when(war.getPath()).thenReturn("/");

        when(mavenContext.getExecutionEnvironment()).thenReturn(executionEnvironment);
        when(product.getContainerId()).thenReturn("tomcat8x");
        when(product.getServer()).thenReturn("server");
        when(product.getContextPath()).thenReturn("/context");
        when(product.getProtocol()).thenReturn("https");
        setUpDependencyPlugin();
        setUpCargoPlugin();
        final Node node = setUpSingleNode();

        webAppManager.startWebapp(
                war, emptySystemProperties(), new ArrayList<>(), new ArrayList<>(), product, mavenContext);

        verify(node, atLeastOnce()).getAjpPort();
        verify(node, atLeastOnce()).getRmiPort();
        verify(node, atLeastOnce()).getWebPort();
    }

    private void setUpMavenPlugin(final String groupId, final String artifactId) {
        final Plugin plugin = mock(Plugin.class);
        when(mavenContext.getPlugin(groupId, artifactId)).thenReturn(plugin);
    }

    private MojoExecutor.Element[] getConfigurationProperties() {
        // Set up
        final Map<String, String> systemProperties = emptyMap();
        final Product mockProduct = mock(Product.class);
        final ContainerConfig containerConfig = mock(ContainerConfig.class);
        when(containerConfig.getProduct()).thenReturn(mockProduct);
        when(containerConfig.getNode()).thenReturn(node);
        when(node.getAjpPort()).thenReturn(AJP_PORT);

        // Invoke
        final MojoExecutor.Element[] configurationProperties =
                webAppManager.getConfigurationProperties(systemProperties, containerConfig);

        // Check
        assertNotNull(configurationProperties);
        return configurationProperties;
    }

    @Test
    public void getConfigurationProperties_shouldIncludeAjpPortInConfigurationIfSet() {
        // Invoke
        final MojoExecutor.Element[] configurationProperties = getConfigurationProperties();

        // Check
        for (final MojoExecutor.Element element : configurationProperties) {
            final Xpp3Dom elementDom = element.toDom();
            if (elementDom.getName().equals(AJP_PORT_PROPERTY)) {
                assertEquals(String.valueOf(AJP_PORT), elementDom.getValue());
                return;
            }
        }
        fail("No element called " + AJP_PORT_PROPERTY);
    }
}
