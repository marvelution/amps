package com.atlassian.maven.plugins.amps.util;

import org.apache.maven.artifact.Artifact;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ArtifactRetrieverTest {

    // A representative sample of Fecru versions found in the Atlassian Maven repository
    private static final String[] FECRU_VERSIONS = {
            "2.0.7-dev-test-tom1",
            "2.0.7-dev46121",
            "2.1.0-441",
            "2.1.0.M5-dev46036",
            "2.1.0.M5-438",
            "2.1.0.RC1-440",
            "2.1.1-443",
            "2.2.0.M1-dev48930",
            "2.2.0.M1r50062-449",
            "2.2.0.M1-2.2.0.M1-dev48877",
            "2.2.0.M1-446",
            "2.2.0.M2-dev50951",
            "2.2.0.M2-452",
            "2.2.0.M3-dev54169",
            "2.2.0.RC1-dev54987",
            "2.2.0.RC1-r54848",
            "2.2.0.RC1-463",
            "2.2.1-467",
            "2.2.1.1-468",
            "2.2.3-473",
            "2.2.3.1-475",
            "2.2.4-488",
            "2.2.4-20101022032418",
            "2.3.0-480",
            "2.3.0.M1-470",
            "2.3.0.RC3-479",
            "2.3.3-dev-62433",
            "2.3.3-489",
            "2.3.4-dev-63923",
            "2.3.4-20100712032131",
            "2.4.0-M1-dev62893",
            "2.4.0-M1-dev-63606",
            "2.4.0-M3-dev64340",
            "2.4.0-M4-r65904",
            "2.4.0-M4-r106405",
            "2.4.0-20101020013901",
            "2.5.4.studio-4-20110509203220",
            "2.5.4-20110408064401",
            "2.5.7.studio-1-20110725064125",
            "2.5.7-20110621031531",
            "2.6.0-M2-20110329050339",
            "2.6.0-20110606054705",
            "2.6.1.STRM-M1-20110713191724",
            "2.6.1-20110621071349",
            "2.6.5.1-20110830071256",
            "2.6.6-20110902004728",
            "2.7.0-M1-20110621052038",
            "2.7.0-M7.1-20110831041110",
            "2.7.0-M8-20110905235620",
            "2.7.0-EAP-1-20110809075104",
            "2.7.0-EAP-2.1-20110824230957",
            "2.7.0-20110906065646",
            "2.7.3.1",
            "2.7.4-20110928032505",
            "2.7.10-20120119115142",
            "2.7.10.1-20120122215659",
            "2.7.10.2-20120208062757",
            "2.7.11-20120227023831",
            "2.7.13.1-20120530000837",
            "2.7.13.9-20120924053137",
            "2.7.13.10-20120927051533",
            "2.7.14-20120612060728",
            "2.8.0-m1-20120619011455",
            "2.8.0-20120810022244",
            "2.9.0-m3-20121003045332",
            "2.9.0-rc1-20121029024609",
            "2.9.0-20121111231108",
            "2.10.0-20130115015957",
            "2.10.5-b1-20130422062105",
            "2.10.6-m1",
            "2.10.6-m1-20130617081820",
            "2.10.6-20130617233602",
            "3.0.0-M1-20111004030302",
            "3.0.0-20130529072315",
            "3.5.5-20141215151512",
            "3.6.0-m1-20141015040349",
            "3.6.0-20141028105444",
            "3.9.0-m01",
            "3.9.0-nps-plugin-2.0.9",
            "3.9.2-20151023105615",
            "3.10.0-m01",
            "3.10.0-20151028083004",
            "3.11.0-m01",
            "3.11.0-rc02",
            "4.0.0-rc03",
            "4.0.0-20160314140114",
            "4.8.3-20200703142631",
            "4.9.0-SNAPSHOT",
            "4.9.0-20200709182129"
    };

    private static void assertLatestRelease(@Nullable final String expectedVersion, final String... versions) {
        // Set up
        final List<String> versionList = asList(versions);
        final Artifact artifact = mock(Artifact.class);
        // These values copied from FeCruProductHandler
        when(artifact.getGroupId()).thenReturn("com.atlassian.fecru");
        when(artifact.getArtifactId()).thenReturn("amps-fecru");

        // Invoke
        final String version = ArtifactRetriever.getLatestOfficialRelease(versionList, artifact);

        // Check
        assertThat(version, is(expectedVersion));
    }

    @Test
    public void getLatestOfficialRelease_whenNoVersionsExist_shouldReturnNull() {
        assertLatestRelease(null);
    }

    @Test
    public void getLatestOfficialRelease_whenOneUnqualifiedVersionExists_shouldReturnIt() {
        final String version = "1.2.3";
        assertLatestRelease(version, version);
    }

    @Test
    public void getLatestOfficialRelease_whenOneQualifiedVersionExists_shouldReturnNull() {
        final String version = "1.2.3-rc1";
        assertLatestRelease(null, version);
    }

    @Test
    public void getLatestOfficialRelease_whenThreeUnqualifiedVersionsExist_shouldReturnTheLatest() {
        final String version1 = "1.1.1";
        final String version2 = "1.1.3";
        final String version3 = "1.1.4-rc1";
        final String version4 = "1.1.2";
        assertLatestRelease(version2, version1, version2, version3, version4);
    }

    @Test
    public void getLatestOfficialRelease_whenPassedFecruVersions_shouldReturnTheLatest() {
        assertLatestRelease("4.9.0-20200709182129", FECRU_VERSIONS);
    }
}
