package com.atlassian.maven.plugins.amps.util;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;

public class MapUtilsTest {

    @Test
    public void emptyMapShouldJoinIntoAnEmptyString() {
        assertThat(MapUtils.join(emptyMap(), "foo", "bar"), is(""));
    }

    @Test
    public void singletonMapShouldJoinIntoKeyPlusEqualsPlusValue() {
        assertThat(MapUtils.join(singletonMap(1, 2L), ";", ":"), is("1;2"));
    }

    @Test
    public void keyValueSeparatorShouldBeAbleToBeEmpty() {
        assertThat(MapUtils.join(singletonMap(3, true), "", ":"), is("3true"));
    }

    @Test
    public void shouldBeAbleToJoinMultipleEntryMap() {
        final Map<?, ?> map = ImmutableMap.of("foo", 41, "bar", 42);
        assertThat(MapUtils.join(map, "#", "@"), is("foo#41@bar#42"));
    }

    @Test
    public void merge_whenBothMapsAreEmpty_shouldReturnEmptyMap() {
        // Act
        final Map<String, Object> result = MapUtils.merge(emptyMap(), emptyMap());

        // Assert
        assertThat(result, is(emptyMap()));
    }

    @Test
    public void merge_whenKeysClash_shouldFavourSecondMap() {
        // Arrange
        final String key = "answer";
        final Map<String, Integer> losers = singletonMap(key, 43);
        final int winningValue = 42;
        final Map<String, Integer> winners = singletonMap(key, winningValue);

        // Act
        final Map<String, Integer> result = MapUtils.merge(losers, winners);

        // Assert
        assertThat(result.size(), is(1));
        assertThat(result, hasEntry(key, winningValue));
    }
}
