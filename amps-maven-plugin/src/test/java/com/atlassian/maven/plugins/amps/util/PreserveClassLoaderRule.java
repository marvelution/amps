package com.atlassian.maven.plugins.amps.util;

import org.junit.rules.ExternalResource;

import static java.lang.Thread.currentThread;

/**
 * A JUnit rule that ensures that the current thread's context
 * {@link ClassLoader} is reinstated after the test(s) complete.
 */
public class PreserveClassLoaderRule extends ExternalResource {

    private ClassLoader previousClassLoader;

    @Override
    protected void before() {
        previousClassLoader = currentThread().getContextClassLoader();
    }

    @Override
    protected void after() {
        if (previousClassLoader != null) {
            currentThread().setContextClassLoader(previousClassLoader);
        }
    }
}
