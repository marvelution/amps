// invalid because I18n is not attached to a namespace
I18n.getText('plugin.i18n.invalid.must-have-one-parent-namespace');
// invalid because namespaces must be bare objects and not the result of function calls
(() => WRM)()['I18n'].getText('plugin.i18n.invalid.namespace-must-be-bare-object');
// invalid because namespace must be statically analysable
var namespace = 'WRM';
window[namespace]['I18n'].getText('plugin.i18n.invalid.must-be-statically-analysable');
// invalid because getText must be a function call, not a property lookup
WRM.I18n['getText']('plugin.i18n.invalid.gettext-must-be-bare-function-call');
// invalid because first argument must be a plain string
WRM.I18n.getText(`plugin.i18n.invalid.gettext-argument-must-be-plain-string`);
var key = 'plugin.i18n.invalid.gettext-argument-must-be-plain-string';
WRM.I18n.getText(key);
// invalid because whitespace is not a valid character in a translation key, no matter where it occurs
WRM.I18n.getText(' plugin.i18n.invalid.no-whitespace-in-key.prefix');
WRM.I18n.getText('plugin.i18n.invalid.no-whitespace-in-key.suffix ');
WRM.I18n.getText('plugin.i18n.invalid.no-whitespace-in-key.  infix');
WRM.I18n.getText('plugin.i18n.invalid.no-whitespace-in-key\n\t\r.special-characters');
