const translations = {
    first: AJS.I18n.getText("plugin.i18n.string"),
    second: WRM.I18n.getText("plugin.i18n.string"),
};

const lol = window.WRM;
translations.third = lol.I18n.getText("plugin.i18n.string");
translations.copied = translations.first;

console.log('These will all be translated at runtime:', translations);
