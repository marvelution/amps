package com.atlassian.maven.plugins.bamboo;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="post-process-resources")
public class BambooPostProcessResourcesMojo extends PostProcessResourcesMojo {
}
