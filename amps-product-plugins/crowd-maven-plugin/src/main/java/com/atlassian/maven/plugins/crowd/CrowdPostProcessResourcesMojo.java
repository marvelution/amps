package com.atlassian.maven.plugins.crowd;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="post-process-resources")
public class CrowdPostProcessResourcesMojo extends PostProcessResourcesMojo {
}
