package com.atlassian.maven.plugins.fecru;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="post-process-resources")
public class FecruPostProcessResourcesMojo extends PostProcessResourcesMojo {
}
