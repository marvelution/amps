package com.atlassian.maven.plugins.jira;

import com.atlassian.maven.plugins.amps.PluginInformation;
import org.apache.maven.plugins.annotations.Mojo;

import javax.annotation.Nonnull;

@Mojo(name = "create_v4", requiresProject = false)
public class JiraCreateV4Mojo extends JiraCreateMojo {

    private static final String JIRA4 = "jira4";

    @Override
    protected String getDefaultProductId() {
        return JIRA4;
    }

    @Override
    @Nonnull
    protected PluginInformation getPluginInformation() {
        final PluginInformation pluginInformation = super.getPluginInformation();
        return new PluginInformation(pluginInformation.getGroupId(),
                "maven-jira4-plugin", pluginInformation.getVersion(), JIRA4);
    }
}
