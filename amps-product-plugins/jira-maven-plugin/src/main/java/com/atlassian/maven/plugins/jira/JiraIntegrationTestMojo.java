package com.atlassian.maven.plugins.jira;

import com.atlassian.maven.plugins.amps.IntegrationTestMojo;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.JIRA;
import static org.apache.maven.plugins.annotations.ResolutionScope.TEST;

/**
 * Runs the project's integration tests against a Jira instance.
 */
@Mojo(name = "integration-test", requiresDependencyResolution = TEST)
public class JiraIntegrationTestMojo extends IntegrationTestMojo {

    @Override
    protected String getDefaultProductId() {
        return JIRA;
    }

    @Override
    protected Map<String, String> getProductFunctionalTestProperties(final Product product) {
        final Map<String, String> props = new HashMap<>();

        // set up properties for Jira functional test library
        props.put("jira.protocol", product.getProtocol());
        props.put("jira.host", product.getServer());
        props.put("jira.xml.data.location", getMavenContext().getProject().getBasedir() + "/src/test/xml");
        props.put("jira.context", product.getContextPath());
        props.put("jira.port", String.valueOf(product.getWebPortForNode(0)));
        for (int i = 0; i < product.getNodes().size(); i++) {
            props.put("jira.port." + i, String.valueOf(product.getWebPortForNode(i)));
        }
        getLog().info("jira props: " + props);

        return props;
    }
}
