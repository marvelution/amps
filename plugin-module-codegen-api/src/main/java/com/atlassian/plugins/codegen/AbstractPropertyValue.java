package com.atlassian.plugins.codegen;

import static java.util.Objects.requireNonNull;

/**
 * Base class for any change item that is a key-value pair.
 */
public abstract class AbstractPropertyValue {
    private final String name;
    private final String value;

    protected AbstractPropertyValue(String name, String value) {
        this.name = requireNonNull(name, "name");
        this.value = requireNonNull(value, "value");
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name + "=" + value;
    }

    @Override
    public boolean equals(final Object other) {
        if (other != null && other.getClass() == this.getClass()) {
            final AbstractPropertyValue otherValue = (AbstractPropertyValue) other;
            return name.equals(otherValue.name) && value.equals(otherValue.value);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
