package com.atlassian.plugins.codegen;

import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;

/**
 * Describes a &lt;component&gt; element that should be added to the plugin XML file.
 * This is provided in addition to {@link ModuleDescriptor} because some other types of
 * modules may also need to create component declarations.
 * <p>
 * Unlike other classes in this package, this class uses a builder pattern due to the
 * large number of optional properties.
 */
public final class ComponentDeclaration implements PluginProjectChange {

    public enum Visibility {
        PUBLIC,
        PRIVATE
    }

    private final ClassId classId;
    private final String key;
    private final Visibility visibility;
    private final Optional<String> name;
    private final Optional<String> nameI18nKey;
    private final Optional<String> description;
    private final Optional<String> descriptionI18nKey;
    private final Optional<ClassId> interfaceId;
    private final Optional<String> alias;
    private final Optional<String> application;
    private final ImmutableMap<String, String> serviceProperties;

    public static Builder builder(ClassId classId, String key) {
        return new Builder(classId, key);
    }

    public static ComponentDeclaration componentDeclaration(ClassId classId, String key) {
        return builder(classId, key).build();
    }

    private ComponentDeclaration(Builder builder) {
        this.classId = builder.classId;
        this.visibility = builder.visibility;
        this.key = builder.key;
        this.name = builder.name;
        this.nameI18nKey = builder.nameI18nKey;
        this.description = builder.description;
        this.descriptionI18nKey = builder.descriptionI18nKey;
        this.interfaceId = builder.interfaceId;
        this.alias = builder.alias;
        this.application = builder.application;
        this.serviceProperties = ImmutableMap.copyOf(builder.serviceProperties);
    }

    public ClassId getClassId() {
        return classId;
    }

    public String getKey() {
        return key;
    }

    public Optional<ClassId> getInterfaceId() {
        return interfaceId;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getNameI18nKey() {
        return nameI18nKey;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<String> getDescriptionI18nKey() {
        return descriptionI18nKey;
    }

    public Optional<String> getAlias() {
        return alias;
    }

    public Optional<String> getApplication() {
        return application;
    }

    public ImmutableMap<String, String> getServiceProperties() {
        return serviceProperties;
    }

    @Override
    public String toString() {
        return "[component: " + classId + "]";
    }

    public static class Builder {

        private final ClassId classId;
        private final String key;
        private Visibility visibility = Visibility.PRIVATE;
        private Optional<String> name = empty();
        private Optional<String> nameI18nKey = empty();
        private Optional<String> description = empty();
        private Optional<String> descriptionI18nKey = empty();
        private Optional<ClassId> interfaceId = empty();
        private Optional<String> alias = empty();
        private Optional<String> application = empty();
        private Map<String, String> serviceProperties = new HashMap<>();

        public Builder(ClassId classId, String key) {
            this.classId = checkNotNull(classId, "classId");
            this.key = checkNotNull(key, "key");
        }

        public ComponentDeclaration build() {
            return new ComponentDeclaration(this);
        }

        public Builder interfaceId(Optional<ClassId> interfaceId) {
            this.interfaceId = checkNotNull(interfaceId, "interfaceId");
            return this;
        }

        public Builder visibility(Visibility visibility) {
            this.visibility = checkNotNull(visibility, "visibility");
            return this;
        }

        public Builder name(Optional<String> name) {
            this.name = checkNotNull(name, "name");
            return this;
        }

        public Builder nameI18nKey(Optional<String> nameI18nKey) {
            this.nameI18nKey = checkNotNull(nameI18nKey, "nameI18nKey");
            return this;
        }

        public Builder description(Optional<String> description) {
            this.description = checkNotNull(description, "description");
            return this;
        }

        public Builder descriptionI18nKey(Optional<String> descriptionI18nKey) {
            this.descriptionI18nKey = checkNotNull(descriptionI18nKey, "descriptionI18nKey");
            return this;
        }

        public Builder alias(Optional<String> alias) {
            this.alias = checkNotNull(alias, "alias");
            return this;
        }

        public Builder application(Optional<String> application) {
            this.application = checkNotNull(application, "application");
            return this;
        }

        public Builder serviceProperties(Map<String, String> serviceProperties) {
            this.serviceProperties.putAll(checkNotNull(serviceProperties, "serviceProperties"));
            return this;
        }
    }
}
