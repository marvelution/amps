package com.atlassian.plugins.codegen;

import com.google.common.collect.ImmutableList;

import java.util.Optional;

import static com.atlassian.plugins.codegen.ClassId.fullyQualified;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;

/**
 * Describes a &lt;component-import&gt; element that should be added to the plugin XML file.
 */
public final class ComponentImport implements PluginProjectChange {

    private final ClassId interfaceClass;
    private final Optional<String> key;
    private final Optional<String> filter;
    private final ImmutableList<ClassId> alternateInterfaces;

    public static ComponentImport componentImport(ClassId interfaceClass) {
        return new ComponentImport(interfaceClass, empty(), empty(), ImmutableList.<ClassId>of());
    }

    public static ComponentImport componentImport(String fullyQualifiedInterfaceName) {
        return new ComponentImport(fullyQualified(fullyQualifiedInterfaceName), empty(), empty(), ImmutableList.<ClassId>of());
    }

    private ComponentImport(ClassId interfaceClass, Optional<String> key, Optional<String> filter, ImmutableList<ClassId> alternateInterfaces) {
        this.interfaceClass = checkNotNull(interfaceClass, "interfaceClass");
        this.key = checkNotNull(key, "key");
        this.filter = checkNotNull(filter, "filter");
        this.alternateInterfaces = checkNotNull(alternateInterfaces, "alternateInterfaces");
    }

    public ComponentImport key(Optional<String> key) {
        return new ComponentImport(this.interfaceClass, key, this.filter, this.alternateInterfaces);
    }

    public ComponentImport filter(Optional<String> filter) {
        return new ComponentImport(this.interfaceClass, this.key, filter, this.alternateInterfaces);
    }

    public ComponentImport alternateInterfaces(ClassId... interfaceClasses) {
        return new ComponentImport(this.interfaceClass, this.key, this.filter, ImmutableList.copyOf(interfaceClasses));
    }

    public ClassId getInterfaceClass() {
        return interfaceClass;
    }

    public Optional<String> getKey() {
        return key;
    }

    public Optional<String> getFilter() {
        return filter;
    }

    public Iterable<ClassId> getAlternateInterfaces() {
        return alternateInterfaces;
    }

    @Override
    public String toString() {
        return "[component-import: " + interfaceClass + "]";
    }
}
