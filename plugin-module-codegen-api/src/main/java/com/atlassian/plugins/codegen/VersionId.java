package com.atlassian.plugins.codegen;

import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;

/**
 * Simple wrapper for an artifact version string. This can also be a Maven version property,
 * in which case the version string is used as the default value for that property.
 */
public final class VersionId {

    /**
     * Creates an instance with no property name.
     *
     * @param version the version
     * @return a new instance
     */
    public static VersionId version(String version) {
        return new VersionId(Optional.of(version), empty());
    }

    /**
     * Creates an instance with a property name.
     *
     * @param propertyName   the property name
     * @param defaultVersion the default version
     * @return a new instance
     */
    public static VersionId versionProperty(String propertyName, String defaultVersion) {
        return new VersionId(Optional.of(defaultVersion), Optional.of(propertyName));
    }

    /**
     * Creates an instance for no version at all.
     *
     * @return a new instance
     */
    public static VersionId noVersion() {
        return new VersionId(empty(), empty());
    }

    private final Optional<String> version;
    private final Optional<String> propertyName;

    private VersionId(final Optional<String> version, final Optional<String> propertyName) {
        this.version = requireNonNull(version, "version");
        this.propertyName = requireNonNull(propertyName, "propertyName");
    }

    public Optional<String> getVersion() {
        return version;
    }

    public Optional<String> getPropertyName() {
        return propertyName;
    }

    public boolean isDefined() {
        return version.isPresent() || propertyName.isPresent();
    }

    /**
     * Returns the version string, unless a property name was specified, in which case it returns
     * "${propertyName}". Returns none() if neither was specified.
     */
    public Optional<String> getVersionOrPropertyPlaceholder() {
        return propertyPlaceholder().isPresent() ? propertyPlaceholder() : version;
    }

    @Override
    public String toString() {
        return propertyPlaceholder()
                .map(placeholder -> placeholder + defaultValueSuffix())
                .orElse(version.orElse("?"));
    }

    private String defaultValueSuffix() {
        return version
                .map(v -> " (" + v + ")")
                .orElse("");
    }

    private Optional<String> propertyPlaceholder() {
        return propertyName.map(property -> "${" + property + "}");
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof VersionId) {
            VersionId v = (VersionId) other;
            return version.equals(v.version) && propertyName.equals(v.propertyName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
