package com.atlassian.plugins.codegen;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ArtifactIdTest {

    @Test
    public void getCombinedId_whenGroupIdAbsent_shouldReturnOnlyTheArtifactId() {
        // Arrange
        final String artifactIdStr = "foo";
        final ArtifactId artifactId = ArtifactId.artifactId(artifactIdStr);

        // Act
        final String combinedId = artifactId.getCombinedId();

        // Assert
        assertThat(combinedId, is(artifactIdStr));
    }

    @Test
    public void getCombinedId_whenGroupIdPresent_shouldReturnBothPartsColonDelimited() {
        // Arrange
        final String groupIdStr = "bar";
        final String artifactIdStr = "foo";
        final ArtifactId artifactId = ArtifactId.artifactId(groupIdStr, artifactIdStr);

        // Act
        final String combinedId = artifactId.getCombinedId();

        // Assert
        assertThat(combinedId, is(groupIdStr + ":" + artifactIdStr));
    }
}
