package com.atlassian.plugins.codegen;

import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

public class AsmUtilTest {

    @Test
    public void asmOpCode_shouldBeGreaterThanZero() {
        assertThat(AsmUtil.asmOpCode(), greaterThan(0));
    }
}
