package com.atlassian.plugins.codegen.modules.common.servlet;

import com.atlassian.plugins.codegen.modules.BasicClassModuleProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

/**
 * The properties of a <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/servlet-filter-plugin-module/">
 * {@code servlet-filter}</a> module.
 */
public class ServletFilterProperties extends BasicClassModuleProperties {
    public static final String LOCATION = "LOCATION";
    public static final String WEIGHT = "WEIGHT";
    public static final String URL_PATTERN = "URL_PATTERN";
    public static final String DISPATCHERS = "DISPATCHERS";
    public static final String INIT_PARAMS = "INIT_PARAMS";

    private static final List<String> ALLOWED_LOCATIONS = asList(
            "after-encoding",
            "before-login",
            "before-decoration",
            "before-dispatch");

    private static final List<String> ALLOWED_DISPATCHERS = asList(
            "REQUEST",
            "INCLUDE",
            "FORWARD",
            "ERROR"
    );

    public ServletFilterProperties() {
        this("MyServletFilter");
    }

    public ServletFilterProperties(String fqClassName) {
        super(fqClassName);
        put(DISPATCHERS, new ArrayList<String>());
        put(INIT_PARAMS, new HashMap<String, String>());

        //sane defaults
        setUrlPattern("/*");
        setLocation(ALLOWED_LOCATIONS.get(3));
        setWeight(100);
    }

    public void setLocation(String location) {
        setProperty(LOCATION, location);
    }

    public void setWeight(int weight) {
        setProperty(WEIGHT, Integer.toString(weight));
    }

    public void setUrlPattern(String pattern) {
        setProperty(URL_PATTERN, pattern);
    }

    public void setDispatchers(List<String> dispatchers) {
        put(DISPATCHERS, dispatchers);
    }

    @SuppressWarnings(value = "unchecked")
    public void addDispatcher(String dispatcher) {
        List<String> dispatchers = (List<String>) get(DISPATCHERS);
        if (dispatchers == null) {
            dispatchers = new ArrayList<>();
            setDispatchers(dispatchers);
        }

        dispatchers.add(dispatcher);
    }

    public void setInitParams(Map<String, String> params) {
        put(INIT_PARAMS, params);
    }

    @SuppressWarnings(value = "unchecked")
    public void addInitParam(String name, String value) {
        Map<String, String> params = (Map<String, String>) get(INIT_PARAMS);
        if (params == null) {
            params = new HashMap<>();
            setInitParams(params);
        }

        params.put(name, value);
    }

    public List<String> allowedLocations() {
        return unmodifiableList(ALLOWED_LOCATIONS);
    }

    public List<String> allowedDispatchers() {
        return unmodifiableList(ALLOWED_DISPATCHERS);
    }
}
