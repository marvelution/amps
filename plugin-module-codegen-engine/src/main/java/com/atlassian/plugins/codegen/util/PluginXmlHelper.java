package com.atlassian.plugins.codegen.util;

import com.atlassian.plugins.codegen.modules.PluginModuleLocation;
import com.google.common.annotations.VisibleForTesting;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.atlassian.plugins.codegen.util.Dom4jUtil.newSaxReader;
import static java.lang.String.format;

/**
 * Provides read-only operations on atlassian-plugin.xml.
 */
public class PluginXmlHelper {

    /**
     * Returns the first child element of the given parent that both:
     * <ul>
     *     <li>has the given element type, and</li>
     *     <li>has the given attribute set to the given value</li>
     * </ul>
     *
     * @param parent the parent element
     * @param type the type of child element to find
     * @param attributeName the child attribute to inspect
     * @param attributeValue the expected value of that attribute
     * @return none if there's no such child element
     */
    public static Optional<Element> findElementByTypeAndAttribute(
            final Element parent, final String type, final String attributeName, final String attributeValue) {
        final List<?> children = parent.elements(type);
        return children.stream()
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .filter(e -> attributeValue.equals(e.attributeValue(attributeName)))
                .findFirst();
    }

    private static Document read(final File xmlFile) throws DocumentException, FileNotFoundException {
        final SAXReader reader = newSaxReader();
        reader.setMergeAdjacentText(true);
        reader.setStripWhitespaceText(false);
        return reader.read(new FileInputStream(xmlFile));
    }

    private final Document document;
    private final File xmlFile;
    private final PluginModuleLocation location;

    public PluginXmlHelper(final PluginModuleLocation location)
            throws DocumentException, IOException {
        this(location, "atlassian-plugin.xml");
    }

    @VisibleForTesting
    PluginXmlHelper(final PluginModuleLocation location, final String descriptorName)
            throws DocumentException, IOException {
        this.location = location;
        this.xmlFile = new File(location.getResourcesDir(), descriptorName);
        this.document = read(xmlFile);
    }

    public PluginXmlHelper(final File pluginXmlFile) throws IOException, DocumentException {
        this.location = null;
        this.xmlFile = pluginXmlFile;
        this.document = read(pluginXmlFile);
    }

    public Document getDocument() {
        return document;
    }

    public File getXmlFile() {
        return xmlFile;
    }

    private String getPluginKey() {
        final String key = document.getRootElement().attributeValue("key");
        if (key == null) {
            throw new IllegalStateException("atlassian-plugin element does not have required attribute: key");
        }
        if (location == null) {
            if (key.contains("${")) {
                final String message = format(
                        "cannot substitute placeholders in plugin key '%s' when plugin details are unknown", key);
                throw new UnsupportedOperationException(message);
            }
            return key;
        }
        return key.replace("${project.groupId}", location.getGroupId())
                .replace("${project.artifactId}", location.getArtifactId())
                .replace("${atlassian.plugin.key}", location.getDefaultPluginKey());
    }

    /**
     * Returns the default {@code name} attribute for new i18n resources.
     *
     * @return a non-blank name
     */
    public String getDefaultI18nName() {
        return "i18n";
    }

    /**
     * Returns the default location for i18n resources in this plugin.
     *
     * @return see description
     */
    @Nonnull
    public String getDefaultI18nLocation() {
        final List<?> i18nNodes = document.selectNodes("//resource[@type='i18n']");
        return i18nNodes.stream()
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .findFirst()
                .map(element -> element.attributeValue("location"))
                .orElseGet(this::getPluginKey);
    }
}
