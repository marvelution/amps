package com.atlassian.plugins.codegen.util;

import com.atlassian.plugins.codegen.modules.PluginModuleLocation;
import org.dom4j.Element;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.Optional;

import static java.util.Optional.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PluginXmlHelperTest {

    private static final String ATTRIBUTE_NAME = "theAttributeName";
    private static final String ATTRIBUTE_VALUE = "theAttributeValue";
    private static final String GROUP_ID = "theGroupId";
    private static final String ARTIFACT_ID = "theArtifactId";
    private static final String DEFAULT_PLUGIN_KEY = "theDefaultPluginKey";

    private static File loadFileFromThisPackage(final String name) throws Exception {
        final URL fileUrl = PluginXmlHelperTest.class.getResource(name);
        assertNotNull("Can't load " + name, fileUrl);
        return new File(fileUrl.toURI());
    }

    private static void assertDefaultI18nLocation(
            final PluginXmlHelper pluginXmlHelper, final String expectedLocation) {
        // Act
        final String defaultI18nLocation = pluginXmlHelper.getDefaultI18nLocation();

        // Assert
        assertThat(defaultI18nLocation, is(expectedLocation));
    }

    @Nonnull
    private static PluginXmlHelper getPluginXmlHelperFromFile(final String pluginXmlFilename) throws Exception {
        final File pluginXml = loadFileFromThisPackage(pluginXmlFilename);
        return new PluginXmlHelper(pluginXml);
    }

    @Nonnull
    private static PluginXmlHelper getPluginXmlHelperFromLocation(final String pluginXmlFilename) throws Exception {
        final File pluginXml = loadFileFromThisPackage(pluginXmlFilename);
        final PluginModuleLocation location = mock(PluginModuleLocation.class);
        when(location.getGroupId()).thenReturn(GROUP_ID);
        when(location.getArtifactId()).thenReturn(ARTIFACT_ID);
        when(location.getDefaultPluginKey()).thenReturn(DEFAULT_PLUGIN_KEY);
        when(location.getResourcesDir()).thenReturn(pluginXml.getParentFile());
        return new PluginXmlHelper(location, pluginXmlFilename);
    }

    @Test
    public void getDefaultI18nLocation_whenNoI18nResourcesExist_shouldReturnPluginKey() throws Exception {
        final PluginXmlHelper helper = getPluginXmlHelperFromFile("minimal-atlassian-plugin.xml");
        assertDefaultI18nLocation(helper, "thePluginKey");
    }

    @Test
    public void getDefaultI18nLocation_whenMultipleI18nResourcesExist_shouldReturnLocationOfFirst() throws Exception {
        final PluginXmlHelper helper = getPluginXmlHelperFromFile("multiple-i18n-atlassian-plugin.xml");
        assertDefaultI18nLocation(helper, "correctLocation");
    }

    @Test
    public void getDefaultI18nLocation_whenPluginKeyIsGroupIdPlusArtifactId_shouldReturnPluginKey() throws Exception {
        final PluginXmlHelper helper =
                getPluginXmlHelperFromLocation("groupId-artifactId-atlassian-plugin.xml");
        assertDefaultI18nLocation(helper, GROUP_ID + "." + ARTIFACT_ID);
    }

    @Test
    public void getDefaultI18nLocation_whenPluginKeyIsPluginKeyProperty_shouldReturnPluginKey() throws Exception {
        final PluginXmlHelper helper =
                getPluginXmlHelperFromLocation("atlassian-plugin-key-atlassian-plugin.xml");
        assertDefaultI18nLocation(helper, DEFAULT_PLUGIN_KEY);
    }

    private static void assertElementByTypeAndAttribute(final Optional<Object> expectedResult, final Element... children) {
        // Arrange
        final Element parent = mock(Element.class);
        final String type = "theType";
        when(parent.elements(type)).thenReturn(Arrays.asList(children));

        // Act
        final Optional<Element> result =
                PluginXmlHelper.findElementByTypeAndAttribute(parent, type, ATTRIBUTE_NAME, ATTRIBUTE_VALUE);

        // Assert
        assertThat(result, is(expectedResult));
    }

    @Test
    public void findElementByTypeAndAttribute_whenNoChildrenExist_shouldReturnNone() {
        assertElementByTypeAndAttribute(empty());
    }

    private static Element mockElement(final String attributeValue) {
        final Element element = mock(Element.class);
        when(element.attributeValue(ATTRIBUTE_NAME)).thenReturn(attributeValue);
        return element;
    }

    @Test
    public void findElementByTypeAndAttribute_whenNoChildrenHaveTheAttribute_shouldReturnNone() {
        final Element child = mockElement(null);
        assertElementByTypeAndAttribute(empty(), child);
    }

    @Test
    public void findElementByTypeAndAttribute_whenOnlyOneChildHasTheAttribute_shouldReturnThatChild() {
        final Element child1 = mockElement("not " + ATTRIBUTE_VALUE);
        final Element child2 = mockElement(ATTRIBUTE_VALUE);
        assertElementByTypeAndAttribute(Optional.of(child2), child1, child2);
    }

    @Test
    public void findElementByTypeAndAttribute_whenMultipleChildrenHaveTheAttribute_shouldReturnTheFirstOne() {
        final Element child1 = mockElement(ATTRIBUTE_VALUE);
        final Element child2 = mockElement(ATTRIBUTE_VALUE);
        final Element child3 = mockElement(ATTRIBUTE_VALUE);
        assertElementByTypeAndAttribute(Optional.of(child1), child1, child2, child3);
    }
}