package com.atlassian.maven.plugins.amps.codegen;

import com.atlassian.plugins.codegen.modules.PluginModuleCreator;
import org.codehaus.plexus.components.interactivity.PrompterException;

import java.util.Map;

/**
 * @since 3.6
 */
public interface PluginModuleSelectionQueryer {
    PluginModuleCreator selectModule(Map<Class, PluginModuleCreator> map) throws PrompterException;

    boolean addAnotherModule() throws PrompterException;
}
