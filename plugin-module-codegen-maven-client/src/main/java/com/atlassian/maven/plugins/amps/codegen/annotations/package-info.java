/**
 * Provides annotations used in the Maven client.
 * <p>
 * Particularly, the {@link com.atlassian.maven.plugins.amps.codegen.annotations.ModuleCreatorClass} annotation that
 * links Prompters with their respective ModuleCreators.
 *
 * @since 3.6
 */
package com.atlassian.maven.plugins.amps.codegen.annotations;
