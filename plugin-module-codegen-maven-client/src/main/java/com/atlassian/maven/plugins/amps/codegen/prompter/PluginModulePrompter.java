package com.atlassian.maven.plugins.amps.codegen.prompter;

import com.atlassian.plugins.codegen.modules.PluginModuleLocation;
import com.atlassian.plugins.codegen.modules.PluginModuleProperties;
import org.codehaus.plexus.components.interactivity.PrompterException;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

public interface PluginModulePrompter<T extends PluginModuleProperties> {
    List<String> YN_ANSWERS = unmodifiableList(asList("Y", "y", "N", "n"));

    List<String> ANDOR_ANSWERS = unmodifiableList(asList("AND", "and", "OR", "or"));

    T getModulePropertiesFromInput(PluginModuleLocation moduleLocation) throws PrompterException;

    T promptForBasicProperties(PluginModuleLocation moduleLocation) throws PrompterException;

    void promptForAdvancedProperties(T props, PluginModuleLocation moduleLocation) throws PrompterException;

    void setDefaultBasePackage(String basePackage);

    String getDefaultBasePackage();

    void setPluginKey(String pluginKey);

    String getPluginKey();
}
