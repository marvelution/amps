/**
 * Provides Maven Prompters to collect information from the user and create the proper Properties class to be passed
 * to the ModuleCreator.
 * <p>
 * These essentially make up the client UI for plugin module generation in Maven.
 *
 * @see com.atlassian.maven.plugins.amps.codegen.prompter.AbstractModulePrompter
 * @since 3.6
 */
package com.atlassian.maven.plugins.amps.codegen.prompter;
